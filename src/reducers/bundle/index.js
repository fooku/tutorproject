import { BUNDLE_SESSION, BUNDLE_ONE_SESSION } from '../../actions/bundle'

export const initialState = {
  isFetching: false,
  error: null,
  data: [],
  dataOne: {}
}

export default (state = initialState, { type, payload, error }) => {
  switch (type) {
    case BUNDLE_SESSION.PENDING:
      return {
        ...state,
        isFetching: true
      }
    case BUNDLE_SESSION.SUCCESS:
      return {
        ...state,
        isFetching: false,
        data: payload
      }
    case BUNDLE_ONE_SESSION.SUCCESS:
      return {
        ...state,
        isFetching: false,
        dataOne: payload
      }
    case BUNDLE_SESSION.FAILED:
      return {
        ...state,
        isFetching: false,
        error,
        data: [],
        dataOne: {}
      }
    default:
      return state
  }
}
