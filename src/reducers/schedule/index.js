import { SCHEDULE_SESSION } from '../../actions/schedule'

export const initialState = {
  isFetching: false,
  error: null,
  data: {}
}

export default (state = initialState, { type, payload, error }) => {
  switch (type) {
    case SCHEDULE_SESSION.PENDING:
      return {
        ...state,
        isFetching: true
      }
    case SCHEDULE_SESSION.SUCCESS:
      return {
        ...state,
        isFetching: false,
        data: payload
      }
    case SCHEDULE_SESSION.FAILED:
      return {
        ...state,
        isFetching: false,
        error,
        data: {}
      }
    default:
      return state
  }
}
