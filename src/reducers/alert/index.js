import { ALERT_SESSION } from '../../actions/auth'

export const initialState = {
  isFetching: false,
  error: null,
  alert: {}
}

export default (state = initialState, { type, payload, error }) => {
  switch (type) {
    case ALERT_SESSION.PENDING:
      return {
        ...state,
        isFetching: true
      }
    case ALERT_SESSION.SUCCESS:
      return {
        ...state,
        isFetching: false,
        alert: payload
      }
    case ALERT_SESSION.FAILED:
      return {
        ...state,
        isFetching: false,
        error,
        alert: {}
      }
    default:
      return state
  }
}
