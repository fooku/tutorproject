import { IMG_SESSION } from '../../actions/img'

export const initialState = {
  isFetching: false,
  error: null,
  img: {}
}

export default (state = initialState, { type, payload, error }) => {
  switch (type) {
    case IMG_SESSION.PENDING:
      return {
        ...state,
        isFetching: true
      }
    case IMG_SESSION.SUCCESS:
      return {
        ...state,
        isFetching: false,
        img: payload
      }
    case IMG_SESSION.FAILED:
      return {
        ...state,
        isFetching: false,
        error,
        img: {}
      }
    default:
      return state
  }
}
