import { ORDER_SESSION, ORDERS_SESSION } from '../../actions/order'

export const initialState = {
  isFetching: false,
  error: null,
  orders: [],
  order: {}
}

export default (state = initialState, { type, payload, error }) => {
  switch (type) {
    case ORDER_SESSION.PENDING:
    case ORDERS_SESSION.PENDING:
      return {
        ...state,
        isFetching: true
      }

    case ORDERS_SESSION.SUCCESS:
      return {
        ...state,
        isFetching: false,
        orders: payload
      }
    case ORDER_SESSION.SUCCESS:
      return {
        ...state,
        isFetching: false,
        order: payload
      }
    case ORDERS_SESSION.FAILED:
    case ORDER_SESSION.FAILED:
      return {
        ...state,
        isFetching: false,
        error,
        orders: [],
        order: {}
      }
    default:
      return state
  }
}
