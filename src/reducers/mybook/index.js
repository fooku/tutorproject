import { LIST_MY_BOOKS } from '../../actions/book'

export const initialState = {
  isFetching: false,
  error: null,
  data: [],
  dataOne: {},
  traceBook: []
}

export default (state = initialState, { type, payload, error }) => {
  switch (type) {
    case LIST_MY_BOOKS.PENDING:
      return {
        ...state,
        isFetching: true
      }
    case LIST_MY_BOOKS.SUCCESS:
      return {
        ...state,
        isFetching: false,
        data: payload
      }
    case LIST_MY_BOOKS.FAILED:
      return {
        ...state,
        isFetching: false,
        error
      }
    default:
      return state
  }
}
