import { combineReducers } from 'redux'
import { connectRouter } from 'connected-react-router'
import { reducer as form } from 'redux-form'
import auth from './auth'
import users from './users'
import course from './course'
import bundle from './bundle'
import img from './img'
import cart from './cart'
import order from './order'
import mycourse from './mycourse'
import status from './status'
import alert from './alert'
import schedule from './schedule'
import book from './book'
import mybook from './mybook'

export default history =>
  combineReducers({
    router: connectRouter(history),
    form,
    auth,
    users,
    course,
    bundle,
    img,
    cart,
    order,
    mycourse,
    status,
    alert,
    schedule,
    book,
    mybook
  })
