import { STATUS_SESSION } from '../../actions/status'

export const initialState = {
  isFetching: false,
  error: null,
  status: []
}

export default (state = initialState, { type, payload, error }) => {
  switch (type) {
    case STATUS_SESSION.PENDING:
      return {
        ...state,
        isFetching: true
      }
    case STATUS_SESSION.SUCCESS:
      return {
        ...state,
        isFetching: false,
        status: payload
      }
    case STATUS_SESSION.FAILED:
      return {
        ...state,
        isFetching: false,
        error,
        status: []
      }
    default:
      return state
  }
}
