import { LIST_MY_COURSES, GET_MY_COURSES } from '../../actions/course'

export const initialState = {
  isFetching: false,
  error: null,
  courses: [],
  course: {}
}

export default (state = initialState, { type, payload, error }) => {
  switch (type) {
    case LIST_MY_COURSES.PENDING:
    case GET_MY_COURSES.PENDING:
      return {
        ...state,
        isFetching: true
      }
    case LIST_MY_COURSES.SUCCESS:
      return {
        ...state,
        isFetching: false,
        courses: payload
      }
    case GET_MY_COURSES.SUCCESS:
      return {
        ...state,
        isFetching: false,
        course: payload
      }
    case LIST_MY_COURSES.FAILED:
    case GET_MY_COURSES.FAILED:
      return {
        ...state,
        isFetching: false,
        error,
        courses: [],
        course: {}
      }
    default:
      return state
  }
}
