import { LIST_SESSION, DELETE_MEMBER, UPDATE_MEMBER } from '../../actions/users'

export const initialState = {
  isFetching: false,
  error: null,
  listUsers: {}
}

export default (state = initialState, { type, payload, error }) => {
  switch (type) {
    case LIST_SESSION.PENDING:
    case DELETE_MEMBER.PENDING:
    case UPDATE_MEMBER.PENDING:
      return {
        ...state,
        isFetching: true
      }
    case LIST_SESSION.SUCCESS:
    case DELETE_MEMBER.SUCCESS:
    case UPDATE_MEMBER.SUCCESS:
      return {
        ...state,
        isFetching: false,
        listUsers: payload
      }
    case LIST_SESSION.FAILED:
    case DELETE_MEMBER.FAILED:
    case UPDATE_MEMBER.FAILED:
      return {
        ...state,
        isFetching: false,
        error,
        listUsers: {}
      }
    default:
      return state
  }
}
