import { LIST_COURSES, GET_COURSE } from '../../actions/course'

export const initialState = {
  isFetching: false,
  error: null,
  courses: [],
  course: {}
}

export default (state = initialState, { type, payload, error }) => {
  switch (type) {
    case LIST_COURSES.PENDING:
    case GET_COURSE.PENDING:
      return {
        ...state,
        isFetching: true
      }
    case LIST_COURSES.SUCCESS:
      return {
        ...state,
        isFetching: false,
        courses: payload
      }
    case GET_COURSE.SUCCESS:
      return {
        ...state,
        isFetching: false,
        course: payload
      }
    case LIST_COURSES.FAILED:
    case GET_COURSE.FAILED:
      return {
        ...state,
        isFetching: false,
        error,
        courses: [],
        course: {}
      }
    default:
      return state
  }
}
