import { BOOK_ADD, BOOK_DETAIL, BOOK_TRACING } from '../../actions/book'

export const initialState = {
  isFetching: false,
  error: null,
  data: [],
  dataOne: {},
  traceBook: []
}

export default (state = initialState, { type, payload, error }) => {
  switch (type) {
    case BOOK_ADD.PENDING:
      return {
        ...state,
        isFetching: true
      }
    case BOOK_ADD.SUCCESS:
      return {
        ...state,
        isFetching: false,
        data: payload
      }
    case BOOK_DETAIL.SUCCESS:
      return {
        ...state,
        isFetching: false,
        dataOne: payload
      }
    case BOOK_TRACING.SUCCESS:
      return {
        ...state,
        isFetching: false,
        traceBook: payload
      }
    case BOOK_ADD.FAILED:
      return {
        ...state,
        isFetching: false,
        error
      }
    default:
      return state
  }
}
