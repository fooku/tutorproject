import { CART_SESSION } from '../../actions/cart'

export const initialState = {
  isFetching: false,
  error: null,
  cart: []
}

export default (state = initialState, { type, payload, error }) => {
  switch (type) {
    case CART_SESSION.PENDING:
      return {
        ...state,
        isFetching: true
      }
    case CART_SESSION.SUCCESS:
      return {
        ...state,
        isFetching: false,
        cart: payload
      }
    case CART_SESSION.FAILED:
      return {
        ...state,
        isFetching: false,
        error,
        cart: []
      }
    default:
      return state
  }
}
