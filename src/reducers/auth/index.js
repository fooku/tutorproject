import {
  USER_SESSION,
  USER_LOGOUT,
  USER_AUTH,
  USER_REGISTER
} from '../../actions/auth'

export const initialState = {
  isFetching: false,
  isFetchingAuth: false,
  error: null,
  currentUser: {},
  isAuthenticated: false
}

export default (state = initialState, { type, payload, error }) => {
  switch (type) {
    case USER_SESSION.PENDING:
    case USER_LOGOUT.PENDING:
    case USER_REGISTER.PENDING:
      return {
        ...state,
        isFetchingAuth: false,
        isFetching: true
      }
    case USER_AUTH.PENDING:
      return {
        ...state,
        isFetchingAuth: true,
        isFetching: false
      }
    case USER_SESSION.SUCCESS:
    case USER_AUTH.SUCCESS:
    case USER_REGISTER.SUCCESS:
      return {
        ...state,
        isFetching: false,
        isFetchingAuth: false,
        currentUser: payload,
        isAuthenticated: true
      }
    case USER_SESSION.FAILED:
    case USER_AUTH.FAILED:
    case USER_LOGOUT.SUCCESS:
    case USER_REGISTER.FAILED:
      return {
        ...state,
        isFetching: false,
        isFetchingAuth: false,
        error,
        currentUser: {},
        isAuthenticated: false
      }
    default:
      return state
  }
}
