import React, { Component } from 'react'
import { connect } from 'react-redux'

class BookCard extends Component {
  render() {
    let { name, img, link } = this.props

    return (
      <React.Fragment>
        <div className="column is-4">
          <div className="card is-fixed-height">
            <div className="card-image">
              <figure className="image is-5by6 book-crop">
                <img src={img} alt="Placeholder" />
              </figure>
            </div>
            <div className="card-content">
              <div className="content">
                <h1 className="has-text-centered">{name}</h1>
              </div>
            </div>
            <footer className="card-footer">
              <a
                href={link}
                target="_blank"
                rel="noopener noreferrer"
                className="card-footer-item active-info has-text-info is-size-5"
              >
                <b>Download</b>
              </a>
            </footer>
          </div>
        </div>
      </React.Fragment>
    )
  }
}

export default connect()(BookCard)
