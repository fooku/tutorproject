import React, { Component } from 'react'
import Appfooter from '../../../component/Appfooter'

import { ThemeProvider } from 'styled-components'
import StyledWbnPlayer from '../../../components/styles/StyledWbnPlayer'
import Video from '../../../components/Video'
import Playlist from '../../../components/containers/Playlist'

import { Link } from 'react-router-dom'
import { getMyCourse } from '../../../actions/course'
import { connect } from 'react-redux'
import LoadingIn from '../../../component/LoadingIn'

const theme = {
  bgcolor: '#353535',
  bgcolorItem: '#414141',
  bgcolorItemActive: '#405c63',
  bgcolorPlayed: '#526d4e',
  border: 'none',
  borderPlayed: 'none',
  color: '#fff'
}

class MyCourseDetailPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      activeVideoIn: {},
      playlistId: '',
      autoplay: false,
      two: false
    }
  }
  componentDidMount() {
    window.scrollTo(0, 0)
    const { match, dispatch } = this.props
    const { id } = match.params

    dispatch(getMyCourse(id))
  }

  componentWillReceiveProps(nextProps) {
    console.log('componentWillReceiveProps')
    const { match, course } = nextProps
    const { activeVideo, id } = match.params
    const { playlistId } = this.state

    console.log('this,', activeVideo)
    if (activeVideo === undefined) {
      if (
        course.section !== undefined &&
        course.section &&
        course.section[0] !== undefined &&
        course.section[0].lectures !== null
      ) {
        this.props.history.push({
          pathname: `/mycourse/${id}/${course.section[0].lectures[0].id}`,
          autoplay: false
        })
        this.setState({
          activeVideoIn: course.section[0].lectures[0],
          playlistId: course.section[0].lectures[0].id
        })
      } else {
        this.setState({
          activeVideoIn: {},
          playlistId: ''
        })
      }
    } else {
      if (activeVideo !== playlistId) {
        if (course.name !== undefined) {
          if (
            course.section !== undefined &&
            course.section &&
            course.section[0] !== undefined &&
            course.section[0].lectures !== null
          ) {
            course.section.forEach((sec, index) => {
              console.log('this>> ', sec)
              if (sec.lectures) {
                const newActiveVideo =
                  sec.lectures &&
                  sec.lectures.findIndex(video => video.id === activeVideo)
                if (newActiveVideo !== -1) {
                  this.setState({
                    activeVideoIn:
                      course.section[index].lectures[newActiveVideo],
                    playlistId:
                      course.section[index].lectures[newActiveVideo].id
                  })
                }
              }
            })
          } else {
            this.setState({
              activeVideoIn: {},
              playlistId: ''
            })
          }
        }
      }
    }
    // if (activeVideo !== undefined) {
    //   console.log(activeVideo, playlistId)

    //   if (activeVideo !== playlistId) {
    //     if (course.name !== undefined) {
    //       course.section.forEach((sec, index) => {
    //         const newActiveVideo = sec.lectures.findIndex(
    //           video => video.id === activeVideo
    //         )
    //         if (newActiveVideo !== -1) {
    //           this.setState({
    //             videos: course.section,
    //             activeVideoIn: course.section[index].lectures[newActiveVideo],
    //             playlistId: course.section[index].lectures[newActiveVideo].id
    //           })
    //           console.log(activeVideo)
    //           console.log(course.section[index].lectures[newActiveVideo])
    //           console.log(this.state.activeVideoIn)
    //         }
    //       })
    //     }
    //   } else {
    //     this.setState({
    //       two: true
    //     })
    //   }
    // } else if (course.name !== undefined) {
    //   if (
    //     course.section[0] !== undefined &&
    //     course.section[0].lectures !== null
    //   ) {
    //     this.props.history.push({
    //       pathname: `/mycourse/${id}/${course.section[0].lectures[0].id}`,
    //       autoplay: false
    //     })
    //     this.setState({
    //       videos: course.section,
    //       activeVideoIn: course.section[0].lectures[0],
    //       playlistId: course.section[0].lectures[0].id
    //     })
    //   }
    // }
  }

  render() {
    const { course, match, currentUser } = this.props
    const { id } = match.params

    const { activeVideoIn, autoplay } = this.state
    if (currentUser.ID === undefined) {
      return (
        <>
          <div className="hero is-light">
            <div className="hero-body">
              <div className="container has-text-centered	mt-5">
                <h1 className="animated fadeInLeft faster is-size-1	">
                  คอร์สเรียน
                </h1>
              </div>
            </div>
          </div>{' '}
          <div className="container my-5">
            <div className="columns is-vcentered">
              <div className="column is-12 has-text-centered">
                <p>
                  <i className="fas fa-user-times fa-5x mb-4" />
                </p>
                <p>กรุณาเข้าสู่ระบบ!</p>
                <Link to="/login" className="button is-primary mt-4">
                  เข้าสู่ระบบ
                </Link>
              </div>
            </div>
          </div>
        </>
      )
    } else {
      if (course === undefined || course.name === undefined) {
        return (
          <>
            <div className="hero is-light">
              <div className="hero-body">
                <div className="container has-text-centered	mt-5">
                  <h1 className="animated fadeInLeft faster is-size-1	">
                    {course && course.name}
                  </h1>
                </div>
              </div>
            </div>{' '}
            <div className="container my-5">
              {' '}
              <LoadingIn />
            </div>
          </>
        )
      } else {
        if (course.section) {
          return (
            <React.Fragment>
              <div className="hero is-light">
                <div className="hero-body">
                  <div className="container has-text-centered	mt-5">
                    <h1 className="animated fadeInLeft faster is-size-1	">
                      {course && course.name}
                    </h1>
                  </div>
                </div>
              </div>
              <nav
                className="level"
                style={{
                  backgroundColor: 'hsl(0, 0%, 96%)',
                  marginBottom: '0px'
                }}
              >
                <div className="level-left">
                  {' '}
                  <Link to="/mycourse" className="button is-outlined ml-3 my-3">
                    <div className="icon">
                      <i className="fas fa-chevron-left" />
                    </div>
                    <span>My Courses</span>
                  </Link>
                </div>

                <div className="level-right">
                  {/* <span className="icon has-text-success">
                <i className="fas fa-play-circle mr-3 fa-lg" />
              </span> */}
                  {/* <span className="is-size-4 	m-4">{activeVideoIn.name}</span> */}
                  {/* <span className="icon has-text-success">
                <i className="fas fa-play-circle mr-4 fa-lg" />
              </span> */}
                  {/* <button className="button m-4">report problems</button> */}
                </div>
              </nav>
              <div
                style={{
                  backgroundColor: '#353535',
                  paddingTop: '10px',
                  paddingBottom: '10px'
                }}
              >
                <ThemeProvider theme={theme}>
                  <StyledWbnPlayer>
                    <Video
                      active={activeVideoIn}
                      autoplay={autoplay}
                      img={course.thumbnail}
                    />
                    {/* <Playlist videos={videos} active={activeVideoIn} idC={id} /> */}
                    <Playlist
                      videos={course.section}
                      active={activeVideoIn}
                      doc={course.doc}
                      idC={id}
                    />
                  </StyledWbnPlayer>
                </ThemeProvider>
              </div>

              <Appfooter />
            </React.Fragment>
          )
        } else {
          return (
            <>
              <div className="hero is-light">
                <div className="hero-body">
                  <div className="container has-text-centered	mt-5">
                    <h1 className="animated fadeInLeft faster is-size-1	">
                      {course && course.name}
                    </h1>
                  </div>
                </div>
              </div>
              <div className="container my-5">
                <div className="columns is-vcentered">
                  <div className="column is-12 has-text-centered">
                    <p>
                      <i className="fas fa-history fa-5x mb-4" />
                    </p>
                    <p>คอร์สเรียนของคุณหมดเวลาใช้งานแล้ว!</p>
                    <Link to="/mycourse" className="button is-primary mt-4">
                      กลับสู่หน้าคอร์สของฉัน
                    </Link>
                  </div>
                </div>
              </div>
              <Appfooter />
            </>
          )
        }
      }
    }
  }
}

const mapStateToProps = state => ({
  course: state.mycourse.course,
  isFetching: state.mycourse.isFetching,
  currentUser: state.auth.currentUser
})

export default connect(mapStateToProps)(MyCourseDetailPage)
