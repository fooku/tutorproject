import React, { Component } from 'react'
import Appfooter from '../../component/Appfooter'
import { connect } from 'react-redux'
import { listMyCourse } from '../../actions/course'
import { listMyBook } from '../../actions/book'
import Card from '../../component/Card/cardMyCourse'
import BookCard from './components/bookcard'
import { Link } from 'react-router-dom'
import LoadingIn from '../../component/LoadingIn'
class myCoursePage extends Component {
  componentDidMount() {
    const { dispatch, currentUser } = this.props

    window.scrollTo(0, 0)
    if (currentUser.ID !== undefined) {
      dispatch(listMyCourse(currentUser.ID))
      dispatch(listMyBook(currentUser.ID))
    } else {
      dispatch(listMyCourse('no'))
      dispatch(listMyBook('no'))
    }
  }

  renderCard(list) {
    return (
      list &&
      list.map((mycourse, index) => {
        console.log(',', mycourse, index)
        return (
          <Card
            key={index}
            id={mycourse.course.id}
            idc={mycourse.id}
            name={mycourse.course.name}
            detail={mycourse.course.detail}
            img={mycourse.course.thumbnail}
            timeleft={mycourse.timeleft}
          />
        )
      })
    )
  }

  renderBookCard(list) {
    return (
      list &&
      list.map((mybook, index) => {
        console.log(',', mybook, index)
        return (
          <BookCard
            key={index}
            name={mybook.book.name}
            detail={mybook.book.detail}
            img={mybook.book.thumbnail}
            link={mybook.link}
          />
        )
      })
    )
  }

  render() {
    const { books, courses, isFetching } = this.props
    console.log(courses)
    console.log(books)

    if (isFetching) {
      return (
        <>
          <div className="hero is-warning is-medium">
            <div className="hero-body">
              <div className="container  has-text-centered">
                <h1 className="animated flipInX is-size-1	">My Corner</h1>
                <h2 className="animated flipInX "> มุมของฉัน</h2>
              </div>
            </div>
          </div>
          <div className="container my-5">
            <LoadingIn />
          </div>
        </>
      )
    } else {
      return (
        <React.Fragment>
          <div className="hero is-warning is-medium">
            <div className="hero-body">
              <div className="container has-text-centered">
                <h1 className="animated flipInX is-size-1	">My Corner</h1>
                <h2 className="animated flipInX ">มุมของฉัน</h2>
              </div>
            </div>
          </div>
          <div className="container my-5">
            {courses && courses !== undefined && courses.length > 0 ? (
              <>
                <p className="is-size-2	my-4">My Courses</p>
                <div className="columns is-multiline">
                  {courses.length > 0 && this.renderCard(courses)}
                </div>
              </>
            ) : books && books !== undefined && books.length > 0 ? null : (
              <>
                <p className="is-size-2	my-4">My Courses</p>
                <div className="columns is-vcentered">
                  <div className="column is-12 has-text-centered">
                    <p>
                      <i className="fas fa-desktop fa-5x mb-4" />
                    </p>
                    <p>Your course is empty. Keep shopping to find a course!</p>
                    <Link to="/course" className="button is-primary mt-4">
                      Keep Shopping
                    </Link>
                  </div>
                </div>
              </>
            )}
          </div>

          <div className="container my-5">
            {books && books !== undefined && books.length > 0 ? (
              <>
                <p className="is-size-2	my-4">My eBooks</p>
                <div className="columns is-multiline">
                  {books.length > 0 && this.renderBookCard(books)}
                </div>
              </>
            ) : courses &&
              courses !== undefined &&
              courses.length > 0 ? null : (
              <>
                <hr />
                <p className="is-size-2	my-4">My eBooks</p>
                <div className="columns is-vcentered">
                  <div className="column is-12 has-text-centered">
                    <p>
                      <i className="fas fa-book fa-5x mb-4" />
                    </p>
                    <p>Your eBook is empty. Keep shopping to find a eBook!</p>
                    <Link to="/book" className="button is-primary mt-4">
                      Keep Shopping
                    </Link>
                  </div>
                </div>
              </>
            )}
          </div>
          <Appfooter />
        </React.Fragment>
      )
    }
  }
}
const mapStateToProps = state => ({
  books: state.mybook.data,
  courses: state.mycourse.courses,
  isFetching: state.mycourse.isFetching,
  currentUser: state.auth.currentUser
})

export default connect(mapStateToProps)(myCoursePage)
