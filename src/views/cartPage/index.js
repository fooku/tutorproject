import React, { Component } from 'react'
import Appfooter from '../../component/Appfooter'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { deleteItem, createOrder } from '../../actions/cart'
import Swal from 'sweetalert2'
import promptpay from '../../img/QR_Promptpay.jpg'
import HowToOrder from '../../img/step3-2.jpg'

class CartPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      openCheckout: false,
      openQR: false,
      name: '',
      tel: '',
      address: '',
      district: '',
      province: '',
      postcode: ''
    }
  }
  componentDidMount() {
    window.scrollTo(0, 0)
  }

  componentWillReceiveProps() {
    // this.setState({
    //   openCheckout: false
    // })
  }

  renderBox(list) {
    const { dispatch, currentUser } = this.props

    return (
      list &&
      list.map((cart, index) => {
        return (
          <div className="box my-2" key={index}>
            <article className="media">
              <div className="media-left">
                <figure className="image alligator-turtle">
                  <img src={cart.img} alt="cart item" />
                </figure>
              </div>
              <div className="media-content">
                <div className="content">
                  <div className="columns">
                    <div className="column is-half">
                      <p>
                        {' '}
                        <strong> {cart.name}</strong>
                      </p>
                      <Link
                        to="/cart"
                        onClick={() =>
                          dispatch(deleteItem(currentUser.ID, index))
                        }
                      >
                        <i className="fas fa-trash-alt mr-1" />
                        <span>remove</span>
                      </Link>
                    </div>
                    <div className="column has-text-right	has-text-danger	has-text-weight-bold is-size-6">
                      <span>{'฿' + cart.price}</span>
                      <i className="fas fa-tag ml-2" />
                    </div>
                  </div>
                </div>
              </div>
            </article>
          </div>
        )
      })
    )
  }

  onHandleCheckout() {
    const { dispatch, currentUser } = this.props
    const { name, tel, address, province, district, postcode } = this.state

    Swal.fire({
      title: 'Are you sure?',
      text: 'คุณต้องการยืนยันคำสั่งซื้อใช่หรือไม่',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then(result => {
      if (result.value) {
        const add = {
          name,
          tel,
          address,
          province,
          district,
          postcode
        }
        dispatch(createOrder(currentUser.ID, add))
        this.setState({
          openCheckout: !this.state.openCheckout
        })
      }
    })
  }

  renderItem(list) {
    return (
      list &&
      list.map((item, index) => {
        return (
          <nav className="level" key={index}>
            <div className="level-left">{item.name}</div>
            <div className="level-right">{'฿' + item.price}</div>
          </nav>
        )
      })
    )
  }

  onChangeInput(name, even) {
    this.setState({
      [name]: even.target.value
    })
  }

  render() {
    const { cart } = this.props
    const {
      openCheckout,
      name,
      tel,
      address,
      province,
      district,
      postcode
    } = this.state

    let total = 0
    cart &&
      cart.forEach(element => {
        total += element.price
      })
    return (
      <React.Fragment>
        <div className="hero is-warning is-medium">
          <div className="hero-body">
            <div className="container has-text-centered">
              <h1 className="animated flipInX is-size-1	">Shopping Cart</h1>
              <h2 className="animated flipInX ">ตะกร้าสินค้า</h2>
            </div>
          </div>
        </div>
        <div className="container my-5">
          {cart.length > 0 ? (
            <div className="columns">
              <div className="column is-two-thirds">
                <div className="is-size-4">
                  {cart.length === 1
                    ? cart.length + ' Item in Cart'
                    : cart.length + ' Items in Cart'}
                </div>
                {cart.length > 0 && this.renderBox(cart)}
              </div>
              <div className="column mx-3">
                <p className="is-size-4">ที่อยู่จัดส่งสินค้า :</p>
                <p className="has-text-danger mb-3">
                  ** สำหรับคอร์สที่มีการจัดส่งหนังสือ
                </p>
                <div className="field">
                  <p className="control has-icons-left has-icons-right">
                    <input
                      className="input"
                      type="text"
                      placeholder="ชื่อ นามสกุล"
                      value={name}
                      onChange={e => this.onChangeInput('name', e)}
                    />
                    <span className="icon is-small is-left">
                      <i className="fas fa-file-signature" />
                    </span>
                  </p>
                </div>
                <div className="field">
                  <p className="control has-icons-left has-icons-right">
                    <input
                      className="input"
                      type="text"
                      placeholder="เบอร์โทรศัพท์"
                      value={tel}
                      onChange={e => this.onChangeInput('tel', e)}
                    />
                    <span className="icon is-small is-left">
                      <i className="fas fa-mobile-alt" />
                    </span>
                  </p>
                </div>
                <div className="field">
                  <p className="control has-icons-left has-icons-right">
                    <input
                      className="input"
                      type="text"
                      placeholder="ที่อยู่ - อาคาร, ถนน และ อื่นๆ"
                      value={address}
                      onChange={e => this.onChangeInput('address', e)}
                    />
                    <span className="icon is-small is-left">
                      <i className="fas fa-map-pin" />
                    </span>
                  </p>
                </div>
                <div className="field">
                  <p className="control has-icons-left has-icons-right">
                    <input
                      className="input"
                      type="text"
                      placeholder="เขต/อำเภอ"
                      value={district}
                      onChange={e => this.onChangeInput('district', e)}
                    />
                    <span className="icon is-small is-left">
                      <i className="fas fa-map-pin" />
                    </span>
                  </p>
                </div>

                <div className="field">
                  <p className="control has-icons-left has-icons-right">
                    <input
                      className="input"
                      type="text"
                      placeholder="จังหวัด"
                      value={province}
                      onChange={e => this.onChangeInput('province', e)}
                    />
                    <span className="icon is-small is-left">
                      <i className="fas fa-map-pin" />
                    </span>
                  </p>
                </div>
                <div className="field">
                  <p className="control has-icons-left has-icons-right">
                    <input
                      className="input"
                      type="text"
                      placeholder="รหัสไปรษณีย์"
                      value={postcode}
                      onChange={e => this.onChangeInput('postcode', e)}
                    />
                    <span className="icon is-small is-left">
                      <i className="fas fa-address-card" />
                    </span>
                  </p>
                </div>

                <hr />
                <p className="is-size-4">Total :</p>
                <p className="is-size-1">{'฿' + total}</p>
                <button
                  className="button is-success is-fullwidth mt-2 is-medium"
                  onClick={() => this.onHandleCheckout()}
                >
                  ยืนยันคำสั่งซื้อ
                </button>
              </div>
            </div>
          ) : (
            <div className="columns is-vcentered">
              <div className="column is-12 has-text-centered">
                <p>
                  <i className="fas fa-shopping-cart fa-5x mb-4" />
                </p>
                <p>Your cart is empty. Keep shopping to find a course!</p>
                <Link to="/course" className="button is-primary mt-4">
                  Keep Shopping
                </Link>
              </div>
            </div>
          )}
        </div>
        <div className={openCheckout ? 'modal is-active' : 'modal'}>
          <div className="modal-background" />
          <div className="modal-card is-large">
            <header className="modal-card-head">
              <p className="modal-card-title has-text-centered is-size-3">
                เราได้รับคำสั่งซื้อของคุณแล้ว
              </p>
            </header>
            <section className="modal-card-body has-text-centered">
              {/* <span className="is-size-3"> เราได้รับคำสั่งซื้อของคุณแล้ว</span>
              <hr /> */}
              <p>
                คุณสามารถติดตามสถานะคำสั่งซื้อได้ตลอดเวลาโดยไปที่ Order
                ที่อยู่ด้านบนขวา
              </p>
              <hr />
              <p className="is-size-4 mb-4">ขั้นตอนการโอนเงิน</p>

              <p className="is-size-5">1. โอนเงินผ่านบัญชีธนาคาร</p>
              <div className="columns my-3">
                <div className="column">
                  {this.state.openQR ? (
                    <>
                      <button
                        className="button is-text"
                        onClick={() =>
                          this.setState({
                            openQR: !this.state.openQR
                          })
                        }
                      >
                        Close QR code promptpay
                      </button>
                      <img src={promptpay} alt="promptpay" />
                    </>
                  ) : (
                    <button
                      className="button is-text"
                      onClick={() =>
                        this.setState({
                          openQR: !this.state.openQR
                        })
                      }
                    >
                      Show QR code promptpay
                    </button>
                  )}
                </div>
                <div className="column">
                  <div className="columns is-multiline">
                    {/* <div className="column is-full">
                      <article className="media">
                        <figure className="media-left">
                          <p className="image is-64x64">
                            <img
                              src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxEHBhUTExAWFRMXFxUaGRgXGBgdFRcSGBUaFxYYHx4YIikiGBslGxUXITEhJSkrLjAuGCAzODMtNygtLiwBCgoKDg0OGxAQGy8mHyUwLS0tLy0tLS0rLi8tLS0tNS0tLy0tKy0tLS0tLS0tLS0tLS0tLS0tLS0tKy0tLS0tLf/AABEIAOEA4QMBEQACEQEDEQH/xAAcAAEAAgMBAQEAAAAAAAAAAAAABgcDBAUCAQj/xABFEAACAQICBwUEBA0BCQAAAAAAAQIDEQQFBhIhMUFRYQcTInGBFDKRoXKCsdEVIzVCUlNic5KissHwMxYXQ1SDk8LS8f/EABsBAQACAwEBAAAAAAAAAAAAAAAEBQECAwYH/8QANxEBAAIBAgQDBQcEAAcAAAAAAAECAwQRBRIhMUFRYQYTIjJxFJGhscHR4UJSgfAWIzM0YnKC/9oADAMBAAIRAxEAPwCDF+7AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD1TpurUUYptt2SW9s1vetKza07RDMRMztDJjMJUwOIcKkXGStsfJ7n1XU54NRiz0jJitvDNqzWdpYTs1AAAAAAAAAAAAAAAAAAAAAAAAAAAAAMuFw08XXUIR1pPh/d8l1OOfUY8GOcmSdohtSk2naqdZLkkMrp3fiqNbZcukeS+35HgeKcWyay3LHSkeHn6ytsGCMcb+KR51kVLPMAozVppeCa96Lt81zX/0ouG8XzcPzTanWsz1r4T+0+rnlxRfuqvN8qq5PjHTqxs+DXuyjzT4/wBj6poOIYdbhjJin6x4x6T+iuvSaTtLRJrQAAAAAAAAAAAAAAAAAAAAAAAAAAMtvLMuqZnidSmr83+bFc2RNbrcWkxzkyz9I8Z+jfHitkttVYOU5RTyqhqx2yfvSe+T/suh874hxLLrcnNeekdo8lxhwxjjaGtnOdUssVn4qnCC3+b/AEUd+HcJzazrHSnnP6ebXNqK4485dDRrS6jmtqc/xVXck34Z/RfPo9vK5F4v7N6jR75Mfx089usfWP1Rseoi/fo7Ob5XSzfBunVjdb0170Zc0+D/AMZT8P4hm0WWMmKdp8Y848pdb0i8bKp0hyGrkWK1Z+KD9yaWyXTpLofVeFcXwcQx81Olo718Y/j1V2TFNJ9HJLVyAAAAAAAAAAAAAAAAAAAAAAAADpZJk9TN8RaOyC96b3Lp1fQreJcTxaHHvbrae0f74O2HDbLPTssXLsvp5bhVCnGy4vjJ82+LPnGs1uXV5PeZZ/x4R9Fxjx1xxtVwdJ9I/Y26VJ3qfnS4Q6LnL7C/4LwOc8Rmz/L4R5/X0RdRquX4ad0HlJzk2223tbe9vme5rWKxtWNoVkzM9ZfDLCdaIaYtSVDEyut0Kr3p8IzfH6Xx5nhuP+zUTE6jSR9a/rH7JeHUbdLJxjcJTx+FdOpFShLen9vR9UeI0+py6bLGXHO1o/3/AGEy1YtG0qt0p0YqZHU1ledBvZLjF8IytufXc+m4+o8F47i4hXkt0yR3jz9Y/wB6K/Lhmk+iPl+4AAAAAAAAAAAAAAAAAAAAAAHQyPLvwpmKg3aNm5Nb9Vcut2kV3FNd9j085Yjee0fWXbBj95fZZeBwsMJQUIRUYrcl/m19T5nqM+TPecmSd5ldVrFY2hyNLc7/AAZh+7g/xs1/BDdrefL48C64Dwn7Xk97k+Sv4z5fujarP7uu0d5V29rPoURERtCpDLAAAsPQHSN4iKw1WXiS/Fye+UUvcfVLd0XTb899qeCRjmdXhjpPzR5T5puny7/DKZ1qUa1JxlFSi0001dNPg0954zHktjtFqTtMeMJcxExtKqdM8ijkmPj3d+7qJuKe1xaa1o34rarcdvS59W9nuLX1+CfefPXbf19Vdnx8k9EfL9wAAAAAAAAAAAAAAAAAAAAATDQHDXjUqdVFei1pfbE8b7V5+uPFHrM/osdBXvZLsXiY4HByqT92Kbf9l5t7PU8np9PfUZq4qd7Tsn3vFKzaVU4/Fzx+MlUm/FJ38uSXRKyPq+l09NPiripHSP8Ad/8APdRXvN7TaWud2gAAAe6VWVGqpRbjKLTTW9STun8TW+OuSs0vG8T02ZidusLjyHNlnGUwqrY3skuVRe8vLiujR8d4rw6dDqrYp7d49Y8P5WmK/PXdwu0XD9/kqnxpzi/qy8L+bj8C79kdR7vWTjntaJ++Ov5buWqrvTfyVsfSVeAAAAAAAAAAAAAAAAAAAADKydEsL7PkdPnJOT+s7r5WPmnHtR73XX8o6R/hc6WnLihzO0HG6lGnRT97xy8lsivjd/VRa+yukib31E+HSPrPf9nDX5OkUQk9srAAAAAAJb2eZh3GYTot+GotZfTh98b/AMKPI+1ujjJp654jrWdp+k/tP5yl6S+1pqmWc4f23K6lP9KEkvpW8PzseM4bm9xqseTymPuTsleaswqBbj7CpgAAAAAAAAAAAAAAAAAAAPdCk8RXjBb5SUV5ydl9pplyRjpa8+ETLasbzELfw9JUqaS3JJLyWxHyHJeb2m0+PVfxG0bK20wxHtGkNTlHVivqrb/M5H0ngGH3Wgp6/FP+f4U2qtzZZcYuEcAAAAADcyfE+yZtSnynG/0W7S+TZD4jh99pcmPzrP7/AJw3x25bxK2Zysz5FFfBcqozvDeyZvVhwU219GXij8mj63wzP7/SY8nnEfh0U+WvLeYaJOcwAAAAAAAAAAAAAAAAAAdrQ/De05/DlBSm/RWX8zRS+0Gf3Whvt3t0+/8AhJ0lObLCzIo+aSulR5rPvM0qvnUqf1s+uaGvLpscf+NfyUGSd7z9ZapJcwAAAAAPj3Dbfoyt2FTXpp80n8UfHr12vMes/mu69oQfTnD6mZRnwnG31ovb8nE937L5+bTWx/2z+E9f0lX6yu1t/NGz0yGAAAAAAAAAAAAAAAAAACZ9nmG8NWp1jBenil9sfgeM9rM/XHhj1t+kfqstBXvZNDxqxVNntB4bOa0X+sk/ST1l8mj6xw3LGXSY7R/bH4dFDmry5Jj1aJNcgAAAAAPVOm61RRW+TSXm3ZGt7xjrNp8OrMRvOy10tWKXJWPkVp5rTZeR2R/TXD97lSlxhJP6svC/m4novZrPyaqaf3R+MdfyRdZXem/kgx71WAAAAAAAAAAAAAAAAAAAsTQOcZZHZb1OWt5vavlb4Hzz2npeNbzW7TEbf79VvoZj3eyRnnUxHNKdG/wq1UptKqlZp7prht4Ncz0XBeNxo4nFljek/fE/t6eCHqdN7z4q90PqaPYunKzw8/SzXxTsewrxnQ2jeMsfkrp0+SPBs4TRLF4iW2Cguc5L7I3ZGz+0Whxx8NuafSP16Q6U0mW3ht9WfPtGo5NlKn3jnNzjHdaKTUnsW++xbbkfhnHLa7VTjiu1YiZ9fBtm00YqbzPVh0ZyKGdUautOUJQ1NVqzXi1r3T37lxR241xbJw++PlrExbfff027OeDDGSJ3fcZojicO/Co1F+y7P4St8mzXT+02iyx8e9Z9e33wzbS5I7dWisixTlb2efwVvjuJ08Y0O2/vY+9pGDJ5JFo7o5LCV1Vq21l7sVts+bfPojzfGOPVz0nBp/lnvPn9PRL0+mms81kmPLJrm6RzjDI6utxjZfSb8PzsWnBqWtrsfL4T1+ni46iY93O6uD6WpwAAAAAAAAAAAAAAAAAAbmV5nVyrEa9OVm96e2Mlya/xkPW6DDrKcmaN/Ke0w6Y8tsc71TLLNNKVeyrRdOX6S2w++Pz8zx2t9l82P4sE80eXaf5WOPW1t0t0SajWjXpKUZKUXuaaafqjzeTFfHblvExPr0TK2i0bxL2c2wN4Ea7QPyJH97H+mR6X2W/7y3/rP5wha7/px9XP7PHZV/8Apf8AmTPa6OuL/wCv0R9F2lLZSPHRCexNnSIZeZSUI3bslxe43rWbTtHX6Ezt1lwsx0qo4a6herLpsh/Fx9Ll/o/Z3U5trZfgj17/AHIuTV0r0jqiea5vVzSa12lFborZFPn1fVnr9BwzBoon3cdZ7zPf6ekIGTNbJ3aBYOQAAAAAAAAAAAAAAAAAAAADYwWOq4CprUqkoPpufmnsfqR9TpMOpry5axP1/fu3pktSd6ykFPTjERgk6dNvnaSv8GUFvZXSzO9bWiEuNdkjwev9ua/6mn/P95r/AMKaf++34H2+/lDQzrSSpnGEVOdOEUpKV4617pNcX1J/D+CYtFlnJS0zO23Vyzam2Wu0wwZLnc8nU9SEZa+rfWvs1b23P9o68S4Tj18155mNt+3q0xZpx9nRemdZ/wDCp/zfeVn/AAtp/wC+34O322/k+PTGtb/Tp/zfebR7L6ffre34H22/k42PzKtmEvxlRyXLdFei2F3pdDp9LG2KsR6+P390e+W1+8tQluYAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAGfBYOpj8SqdKnKpN7oxTb8+i67jFrRWN7TsJbhuy7Mq9JNxpU+k6niX8CkvmRp1mKPP7mOaGHMOzfM8FTv3Maq491NNr0lqt+iZmurxT47HNCKVaUqFVxlFxknZxkmpJ8mntTJMTExvDLxuDKS5RoHmOa01KOHcIPdKq1BP0fi9dWxwvqcVekz9zWbRDpVeyvMqcLruJdI1JX/mil8znGtxev3Mc0Ixm+R4rJZ2xFCdO+5tXi3yUo3i30TJFMtL/LLbeGXR7R7EaRV5QoRi5QSk9aSjsbtxMZMtccb2JnZ3v92GZ/q6f/cRx+2YmOaGHEdm+Z0Kd1h4z6RqQv8ANq5mNXinxOaEXxOHnhK7hUhKE4uzjJNST6pkiJiY3hliMgAAAAAAAAAAAAAAAAAAyvTsoyWGX6LwrWXe17ylLjqazUI+SSvbm2VOryTbJMeEOVu6K6QdqeLpZrUhQp0o04TlFOcZSnJxk4t7JJLc9lvUkY9FWaxNpbRSH3Ke12tCdsTh4Tj+lSvGS+rNtS+KF9DH9MscivMfi5ZhjqlWb8VScpvzk27eSvb0JtaxWsRDeFp9leh0IYSONrwUpy20YvdCHCpbjJ71yVueyu1eeZnkjw7tLS6ul3aPRyHFOjSp99Wj7yvaEHycrNuXRL1Rzw6W2SN5naGIruitLtexUaviwtFx5Jzi/i7/AGEidDXws25HrTHtFp57ow6NKnOnUqSiqilZpU14nqyW+7SW5O1xh0s0ybzPQiu0nYh+WcR+6j/WNd8sfVi/ZJe0fTLE6L4yjGjClJVIzb7yMm7xaStqyjzOGmwVyxO8sVru52iHabVzXOadDEUYR7x6sZ09ZJTtdJxk3sdrXT3tem+bSRSs2iSatztjyenX0f8AabJVaUoLW4ypzkouL5pOSa5WfNmujvMX5fNmndS5aNwAAAAAAAAAAAAAAAAAAXD2S6U062WRwdSSjVhfu7v/AFKbblZftRu1blZ87VmrwzFueO0tLR4t7S7s3o55iJVqM+5rS2y2Xpzlza3xb5r1TZrh1dqRtPWGItsqvP8ARPGaPu9aj4P1kHrU/jvj9ZIn489L9pdImJcejS7+tGF7OTUb8m3a/wAzrM7RuP0rmuIWS6PVZxWyjRm4rpTg9VfJFJSOe8R5y5R1l+aJSc5tyd5Nttve5N3bfVsvNtujs+BgAsjsR/LOI/dR/rIOu+Wv1aX7JV2g6F1tKsXSlTqwgqcZp6ylt1mnst5EfT54xRMTHdittmjoj2Z/gbNo4itXVR09sIQi1HXtZSbbu7X3WW2xvm1fPXliNibbw4faxpTPGTWEjRqU4KWtKVSLi6rj7uqnvgm734tLlt66TDEfFvu2rHircnNn1K7G7L49jDAAAAAAAAAAAAAAAAAcQJbkHaHj8ntF1O/pr82rdyS6T95et10I2TS479uksTWJW1onpRQ0uwEtWGrKNlUpTs7KSdukouz29NxXZcNsU9XOY2VJ2hZLDRjSldzG1OSjVhH9F6zTgumtG66SS4Fjpsk5cfXv2dK9YXZmFGOfaOzhGXhr0ZKL4WqQ8L+aKyszS8T5S5x0l+aatKVGq4zTjOLcZJ71KLtJPqmmi8iYmN4dXkABZHYj+WcR+6j/AFkHXfLX6tL9na7VNJsXkGOoRw9bu1OE3JakJXalFL307b3uOWkw0yRPNBWIlEsB2oZjhsQnUnCtDjFwjFtcbOCVn12+RJto8cx06M8sLWz/ACmjpbo3Zx9+CnSk14oTcdaEum9Jrim0V2O84r7tInaX5zp+NLZvts47S7no6plozgKSxeGr2dGdGrSdZVdZRlBTTdROW62+3Tybos2r1GHUTiyfFS/Ssx4T5SkRirlp/wAv5o8HnSTL6XtGIrWlWnXq1ZUlT1nGEHNtTbjvunuf3tMGr1GfURjx/Djp0tM97THhG/5k4q4qfH80+Hl9UQasy9RwAAAAAAAAAAAAAAC2NFOz7CZxofTnUk++nefeU5bY63uwa2xdkldNXTctxXZdVeuSduzSbTEufieyHExq/i8VSlHnKM4u3ktb7TpGur4wzzppoJodHRLD1JSqqdSpq6ztqwjCN2krv9ptt/IiZ885Zjp0hrNt1XdpWewz3SZypPWpU4qnGS3SablKS6Xk0nx1b8Sw0uOaU6+LesbQkvZdptDDUFg8TNRin+JqSfhs3/pyb3bfde7bbZZXj6rTzM89f8tbV8Uo0s7PsNpFiHVUnRrPfOKTjPZZOUdl31TTOGHU3xxt3hiLbIvS7HZ954sdHV42ou7XrOy+ZInXeVW3O9aZ9n2FyXROVSlJ97TlGTnUkr1I7pQS2RW/WSSu9W20xh1V7ZNrdiLby1exH8s4j91H+s313y1Yv2S3T3QmeleLpTjXjS7uM42cHK+s077GrbiNp9RGKJiY3YrbZH8B2PqOJTrYzWprfGFPVlLprOT1V6X8jtbXdPhjqzN020qz2lovkLleKko6tKHGU7WikuS2XfBEXFjnJfZrEby/O+DrSwdaE4ScZwcXGXFSi7p7eqLqY36S7LCwOeVs4oUaGMxbre1VKUVTgqce7g5rxScIp63R8Vu3nn7WzX1Uxgry0x/NM/1T5R6esfek45rp495HzeH8vmPz2vlOGq0cHi3R9lnUg6c1TlrwU2teLnFu75Li926+Mc5qamK5681L/LMf0+k7fnLGTlzV95/V4/wr/EVpYnESnN605ylKT4uUm3J+rbPQRERG0IzGZAAAAAAAAAAAAAAG3luZ18qra1CtOk+OpJpPzW6Xrc1tStvmjc2hIqXaRmlOFvaFLq6dO/yijhOkxeTHLDl5xpTjs6p6tfEzlDjFWjB+cYJKXrc6UwY6doIiHGOrIB2cp0qx2T01Gjipxit0XacEuSU01FeVjlfBjv3gmIl06vaPmlSFvaVHqqdO/wA4s0jSYo8GOWEdzDMa2Z1tetWnVlwc5N28r7IrorHatK1jaIZZsnzvE5HVlLD1XTlJJNpRd0ne3iTMXx1vtFoJjd1f9v8ANP8AnZfwUf8A0Of2XF/axyw+T09zScbPGz9IUk/io3H2bF/acsODi8XUx1dzq1J1Jv8AOnJylbld8Oh2rEVjaOjLCZGXC4ieExMakJas4SUotb1JO6fxRi0RaNpHzE15YrESnOTlOUnKTe9yk7t/FmYjljaBjAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA//Z"
                              alt="kasikorn logo"
                            />
                          </p>
                        </figure>
                        <div className="media-content">
                          <div className="content">
                            <p>
                              บัญชีธนาคารไทยพาณิชย์เลขที่ 813-285713-5 <br />
                              ชื่อบัญชี
                            </p>
                          </div>
                        </div>
                      </article>
                    </div> */}
                    <div className="column is-full">
                      <article className="media">
                        <figure className="media-left">
                          <p className="image is-64x64">
                            <img
                              src="https://i.pinimg.com/originals/b4/ed/2f/b4ed2f55bd95c9689eec4d7aa6c14788.jpg"
                              alt="scb logo"
                            />
                          </p>
                        </figure>
                        <div className="media-content">
                          <div className="content">
                            <p>
                              บัญชีธนาคารกสิกรไทยเลขที่ 037-2-91740-7 <br />
                              ชื่อบัญชี อภิชญา กัฬหะสุต
                            </p>
                          </div>
                        </div>
                      </article>
                    </div>
                  </div>
                </div>
              </div>
              <i className="fas fa-chevron-down" />
              <p className="is-size-5">
                2. แจ้งการโอนเงินโดยเข้าไปที่{' '}
                <span style={{ textDecoration: 'underline' }}>Order</span>{' '}
                จากนั้นคลิกที่{' '}
                <span style={{ textDecoration: 'underline' }}>
                  แจ้งจำระเงิน
                </span>
              </p>
              <img
                className="mt-3"
                src={HowToOrder}
                alt="HowToOrder"
                width="80%"
              />
              <i className="fas fa-chevron-down" />
              <p className="is-size-5">
                3. รอผู้ดูแลตรวจสอบเพื่อ เริ่มเรียน/ดาวน์โหลดหนังสือ
              </p>
            </section>
            <footer className="modal-card-foot">
              <button
                className="button is-fullwidth"
                onClick={() =>
                  this.setState({
                    openCheckout: !this.state.openCheckout
                  })
                }
              >
                ปิดหน้าต่าง
              </button>
            </footer>
          </div>
        </div>
        <Appfooter />
      </React.Fragment>
    )
  }
}

const mapStateToProps = state => ({
  cart: state.cart.cart,
  isFetching: state.cart.isFetching,
  currentUser: state.auth.currentUser
})

export default connect(mapStateToProps)(CartPage)
