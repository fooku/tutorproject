import React, { Component } from 'react'
import styled from 'styled-components'
import Appfooter from '../../component/Appfooter'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import Homeimg from './../../img/AugustHome.jpg'

import Left01 from './../../img/Left01.png'
import Left02 from './../../img/Left02.png'
import Middle01 from './../../img/Middle01.png'
import Middle02 from './../../img/Middle02.png'
import Right01 from './../../img/Right01.png'
import Right02 from './../../img/Right02.png'

import One from './../../img/1.png'
import Two from './../../img/2.png'
import Three from './../../img/3.png'
import Profile from './../../img/profile.jpg'
import Home from './../../img/home.jpg'

// const imgurl = 'https://sv1.picz.in.th/images/2019/01/15/9vAwP2.png'
class HomePage extends Component {
  constructor(props) {
    super(props)

    let Img = styled.abbr`
      background: url(${Homeimg}) center;
      background-size: cover;
      height: 100vh;
      display: flex;
    `

    this.state = {
      Img,
      hover: false,
      hover2: false,
      hover3: false
    }
  }
  componentDidMount() {
    window.scrollTo(0, 0)
  }

  toggleHover = id => {
    if (id === 1) {
      this.setState({ hover: !this.state.hover })
    } else if (id === 2) {
      this.setState({ hover2: !this.state.hover2 })
    } else if (id === 3) {
      this.setState({ hover3: !this.state.hover3 })
    }
  }

  render() {
    const { Img, hover, hover2, hover3 } = this.state
    const screenSize = window.screen.width < 1275

    return (
      <React.Fragment>
        {screenSize ? (
          <div
            className="hero is-light is-medium mb-4"
            style={{
              backgroundImage: `url(${Home})`,
              backgroundPosition: 'center',
              backgroundColor: '#fcf2d6'
            }}
          >
            <div className="hero-body">
              <div className="container has-text-centered	">
                <h1 className="animated flipInX is-size-1">My English Tutor</h1>
                {/* <h2 className="animated flipInX ">คอร์สเรียนออฟไลน์</h2> */}
              </div>
            </div>
          </div>
        ) : (
          <Img className="mb">
            <div className="home-title has-text-centered">
              <h1 className="is-size-1 animated fadeInUp ff">
                My English Tutor
              </h1>
              <h1 className="animated fadeInUp">
                เรียนภาษาอังกฤษและแกรมม่า <i className="fa fa-heart color-5" />
              </h1>
            </div>
          </Img>
        )}

        <div className="container has-text-centered ">
          <div className="pb-4 color-6">
            <h1
              className={
                screenSize
                  ? 'is-size-2 has-text-weight-bold'
                  : 'is-size-1 has-text-weight-bold'
              }
            >
              Feel Confident
            </h1>
            <p>คอร์สเรียนเพื่อไขข้อสงสัยและเพิ่มความมั่นใจในการใช้ภาษาอังกฤษ</p>
          </div>
          <div className="columns is-multiline is-centered mb-4">
            <div className="column has-text-centered p-4 is-6-tablet is-4-desktop">
              <div className="card" style={{ boxShadow: 'none' }}>
                <div className="card-content">
                  <figure
                    className="image my-2"
                    style={{ margin: 'auto' }}
                    onMouseEnter={() => this.toggleHover(1)}
                    onMouseLeave={() => this.toggleHover(1)}
                  >
                    <img src={hover ? Left02 : Left01} alt="gammar" />
                  </figure>
                  {/* <i className="fas fa-image fa-8x color-5" /> */}{' '}
                  <p className="title">Grammar</p>
                  <p className="subtitle mt-4">
                    เริ่มต้นเข้าใจวิธีการแต่งประโยค
                  </p>
                </div>
              </div>
            </div>
            <div className="column has-text-centered p-4 is-6-tablet is-4-desktop">
              <div className="card" style={{ boxShadow: 'none' }}>
                <div className="card-content">
                  <figure
                    className="image my-2"
                    style={{ margin: 'auto' }}
                    onMouseEnter={() => this.toggleHover(2)}
                    onMouseLeave={() => this.toggleHover(2)}
                  >
                    <img src={hover2 ? Middle02 : Middle01} alt="gammar" />
                  </figure>
                  {/* <i className="fas fa-image fa-8x color-5" /> */}
                  <p className="title">Test Preparation</p>
                  <p className="subtitle mt-4">
                    เตรียมสอบพร้อมแบบฝึกหัดและคำอธิบายอย่างละเอียด
                  </p>
                </div>
              </div>
            </div>
            <div className="column has-text-centered p-4 is-6-tablet is-4-desktop">
              <div className="card" style={{ boxShadow: 'none' }}>
                <div className="card-content">
                  <figure
                    className="image my-2"
                    style={{ margin: 'auto' }}
                    onMouseEnter={() => this.toggleHover(3)}
                    onMouseLeave={() => this.toggleHover(3)}
                  >
                    <img src={hover3 ? Right02 : Right01} alt="gammar" />
                  </figure>
                  <p className="title">Speaking Practice</p>
                  {/* <i className="fas fa-image fa-8x color-5" /> */}
                  <p className="subtitle mt-4">
                    ทฤษฎีการออกเสียงและแบบฝึกหัดปรับสำเนียง
                  </p>
                </div>
              </div>
            </div>
          </div>
          <Link className="button  is-large is-warning m-3" to="/course">
            <span className="icon">
              {' '}
              <i className="fas fa-globe-asia" />
            </span>

            <span>เรียนออนไลน์</span>
          </Link>
          <Link className="button is-large is-primary m-3" to="/offlinecourse">
            <span className="icon">
              {' '}
              <i className="fas fa-chalkboard-teacher" />
            </span>

            <span>เรียนตัวต่อตัว</span>
          </Link>
        </div>

        <div className="container has-text-centered">
          <hr className="color-6" />
          <div className="columns is-variable is-2-mobile is-0-tablet is-3-desktop is-8-widescreen is-1-fullhd mt mb">
            <div className="column ">
              <figure className="image" style={{ margin: 'auto' }}>
                <img src={One} alt="gammar" />
              </figure>
              <h1 className="is-size-3 mt-4">เลือกเรียนได้ตามต้องการ</h1>
              <div className="is-size-6 mt-2">
                เราคัดสรรเนื้อหาที่มีคุณภาพและมีความหลากลายเพื่อคุณสามารถนำไปประยุกต์ใช้ให้ตรงกับความต้องการได้อย่างถูกต้องและมีความมั่นใจมากขึ้น
              </div>
            </div>
            <div className="column">
              <figure className="image" style={{ margin: 'auto' }}>
                <img src={Two} alt="gammar" />
              </figure>
              <h1 className="is-size-3 mt-4">เรียนได้ทุกที่ทุกเวลา</h1>
              <div className="is-size-6 mt-2">
                คอร์สเรียนบนเว็ปไซต์นี้สามารถใช้งานได้กับทุกระบบปฏิบัติการ
                คุณสามารถเข้าถึงบทเรียนได้ทุกที่ทุกเวลา
                เพียงแค่เชื่อมต่ออินเทอร์เน็ตเข้ากับอุปกรณ์
              </div>
            </div>
            <div className="column">
              <figure className="image" style={{ margin: 'auto' }}>
                <img src={Three} alt="gammar" />
              </figure>
              <h1 className="is-size-3 mt-4">ถามข้อสงสัยแบบตัวต่อตัว</h1>
              <div className="is-size-6 mt-2">
                เราเข้าใจว่าผู้เรียนอาจมีคำถามระหว่างเรียนและเรายินดีที่จะตอบข้อสงสัยผ่านทางข้อความหรือวีดีโอคอลด้วยความเต็มใจ
                เป็นบริการพิเศษสำหรับผู้ซื้อคอร์สเรียนของเรา
              </div>
            </div>
          </div>
          <hr className="color-6" />
        </div>

        <div
          style={{
            paddingTop: '80px',
            paddingBottom: '80px'
          }}
        >
          <div className="container">
            <div className="columns is-1">
              <div className="column is-one-quarter">
                <div className="columns is-variable is-multiline is-centered is-1 has-text-centered">
                  <div className="column is-full">
                    <figure
                      className="image"
                      style={{
                        maxWidth: '240px',
                        height: 'auto',
                        marginLeft: 'auto',
                        marginRight: 'auto'
                      }}
                    >
                      <img className="is-rounded" src={Profile} alt="profile" />
                    </figure>
                  </div>
                  <div className="column is-full">
                    <p className="is-size-5">CELTA certificate, Oxford</p>
                    <p className="is-size-5">International TESOL, Thailand</p>
                  </div>
                </div>
              </div>
              <div className="column is-three-quarters">
                <div className="ml-lg-5">
                  <p className="is-size-2 mb-2 has-text-centered-mobile">
                    About Us
                  </p>
                  <p className="content is-large">
                    Well-Balanced English provides one-to-one tutoring service
                    run and taught by a tutor whose intentions are to prove that
                    fluency in English is attainable. She has been teaching
                    private lessons in her hometown, Korat, since 2017. With the
                    aim of offering lessons to those who prefer to study at
                    home, the tutor created this website.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>

        <Appfooter />
      </React.Fragment>
    )
  }
}

export default connect()(HomePage)
