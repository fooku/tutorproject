import React, { Component } from 'react'
import Appfooter from '../../../component/Appfooter'
import Modal from 'react-responsive-modal'
import CourseForm from './components/addCourseForm'
import { addCourse, listCourseAll } from '../../../actions/course'
import { connect } from 'react-redux'
import Card from '../../../component/Card/cardCourse'
import { addBundle, listBundle } from '../../../actions/bundle'
import CardBundle from '../../../component/Card/cardBundle'
import LoadingIn from '../../../component/LoadingIn'

class CourseManage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      open: false
    }
  }
  componentDidMount() {
    const { dispatch } = this.props
    window.scrollTo(0, 0)

    dispatch(listCourseAll())
    dispatch(listBundle())
    this.setState({ open: false })
  }

  handleSubmit = (values, dispatch) => {
    const { name, price, detail, img } = values

    this.setState({ open: false })
    console.log(name, price, detail, img)
    const course = {
      name: name,
      price: price,
      detail: detail,
      thumbnail: img
    }
    dispatch(addCourse(course))
  }

  onOpenModal = () => {
    this.setState({ open: true })
  }

  onCloseModal = () => {
    this.setState({ open: false })
  }

  renderCard(list) {
    return (
      list &&
      list.map((course, index) => {
        console.log(course, index)
        return (
          <Card
            key={index}
            id={course.id}
            name={course.name}
            detail={course.detail}
            img={course.thumbnail}
          />
        )
      })
    )
  }

  renderCardBundle(list) {
    return (
      list &&
      list.map((course, index) => {
        console.log(course, index)
        return (
          <CardBundle
            key={index}
            id={course.id}
            name={course.name}
            detail={course.detail}
            img={course.thumbnail}
          />
        )
      })
    )
  }

  render() {
    const { open } = this.state
    const { isFetching, list, bundle, dispatch } = this.props
    if (isFetching) {
      return (
        <>
          <div className="hero is-warning is-medium">
            <div className="hero-body">
              <div className="container has-text-centered">
                <h1 className="animated fadeInLeft faster is-size-1">
                  Course Manager
                </h1>
              </div>
            </div>
          </div>
          <div>
            <div className="container">
              <LoadingIn />
            </div>
          </div>
        </>
      )
    } else {
      return (
        <React.Fragment>
          <div className="hero is-warning is-medium">
            <div className="hero-body">
              <div className="container has-text-centered">
                <h1 className="animated fadeInLeft faster is-size-1">
                  Course Manager
                </h1>
              </div>
            </div>
          </div>
          <div>
            <div className="container">
              <div className="field has-addons has-addons-right mt-4">
                <button
                  className="button is-primary"
                  onClick={this.onOpenModal}
                >
                  Add Course
                </button>
              </div>
              <Modal
                open={open}
                onClose={this.onCloseModal}
                closeIconSize={52}
                center
              >
                <CourseForm onSubmit={this.handleSubmit} />
              </Modal>

              <div className="columns is-multiline my-5">
                {list && list.length > 0 && this.renderCard(list)}
              </div>

              <hr />
              <div className="field has-addons has-addons-right ">
                <button
                  className="button is-primary"
                  onClick={() => dispatch(addBundle())}
                >
                  Add Bundle
                </button>
              </div>
              <div className="columns is-multiline my-5">
                {bundle && bundle.length > 0 && this.renderCardBundle(bundle)}
              </div>
            </div>
          </div>
          <Appfooter />
        </React.Fragment>
      )
    }
  }
}

const mapStateToProps = state => ({
  list: state.course.courses,
  bundle: state.bundle.data,
  isFetching: state.course.isFetching
})

export default connect(mapStateToProps)(CourseManage)
