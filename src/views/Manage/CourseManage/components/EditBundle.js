import React, { Component } from 'react'
import { connect } from 'react-redux'
// import baseurl from '../../../../services/baseurl'
import { updateBundle } from '../../../../actions/bundle'

class EditBundle extends Component {
  constructor(props) {
    super(props)
    const {
      name,
      detail,
      thumbnail,
      price,
      hour,
      Timestamp,
      idcourse
    } = this.props.bundle

    this.state = {
      name,
      detail,
      thumbnail,
      price,
      hour,
      Timestamp,
      courses: idcourse || [],
      c: this.props.courses
    }
  }

  handleChangeName = event => {
    this.setState({ name: event.target.value })
  }
  handleChangeDetail = event => {
    this.setState({ detail: event.target.value })
    event.preventDefault()
  }
  handleChangePrice = event => {
    this.setState({ price: event.target.value })
    event.preventDefault()
  }
  handleChangeHour = event => {
    this.setState({ hour: event.target.value })
    event.preventDefault()
  }

  edit = () => {
    console.log(this.props.bundle.id)
    const { name, detail, thumbnail, price, hour, courses } = this.state
    let idcourse = []
    let priceold = 0
    for (const course of courses) {
      idcourse.push(course.id)
      priceold += parseInt(course.price, 10)
    }
    console.log(priceold)

    const bundle = {
      name: name,
      hour: hour,
      price: price,
      detail: detail,
      thumbnail: thumbnail,
      idcourse: idcourse,
      priceold: String(priceold)
    }
    this.props.dispatch(updateBundle(this.props.bundle.id, bundle))
  }

  onHandleImg(img) {
    this.setState({
      thumbnail: 'https://wellbalancedenglishapi.xyz' + img
    })
  }

  renderListImage(list) {
    console.log(list)
    return (
      list &&
      list.map((imgs, i) => {
        console.log(imgs)

        return (
          <option
            key={i}
            value={imgs.img}
            onClick={() => this.onHandleImg(imgs.img)}
          >
            {imgs.name}
          </option>
        )
      })
    )
  }

  renderSeleteCourse(list) {
    console.log(list)
    return (
      list &&
      list.map((c, i) => {
        console.log('22', c)

        return (
          <option
            key={i}
            value={c.id}
            onClick={() => {
              this.state.courses.splice(i, 1)
              this.setState({ course: this.state.courses })
            }}
          >
            {c.name}
          </option>
        )
      })
    )
  }

  renderListCourse(list) {
    console.log(list)
    return (
      list &&
      list.map((c, i) => {
        return (
          <option
            key={i}
            value={c.id}
            onClick={() =>
              this.setState({ course: this.state.courses.push(c) })
            }
          >
            {c.name}
          </option>
        )
      })
    )
  }

  render() {
    const { img } = this.props
    var { c, courses } = this.state
    c = c.filter(item => {
      return (
        courses.filter(t => {
          return t.id === item.id
        }).length === 0
      )
    })
    console.log('this', courses)
    return (
      <div className="columns">
        <div className="column has-text-centered">
          <figure className="image">
            <img src={this.state.thumbnail} alt="img course" />
          </figure>
          <br />
          <div className="select is-fullwidth is-multiple mt-3">
            <select multiple size={img.length > 4 ? '4' : img.length}>
              {this.renderListImage(img)}
            </select>
          </div>
        </div>
        <div className="column py-0">
          <input
            className="input is-large mb-3 has-text-centered"
            type="text"
            placeholder="Large input"
            value={this.state.name}
            onChange={this.handleChangeName}
          />
          <textarea
            className="textarea mt-3"
            value={this.state.detail}
            onChange={this.handleChangeDetail}
            rows="9"
          />
          <br />
          <div className="columns">
            <div className="column">
              <div className="select is-fullwidth is-multiple">
                <p>คอร์สบันเดิล</p>
                <select multiple size="5">
                  {this.renderSeleteCourse(courses)}
                </select>
              </div>
            </div>
            <div className="column">
              <div className="select is-fullwidth is-multiple">
                <p>คอร์สทั้งหมด</p>
                <select multiple size="5">
                  {this.renderListCourse(c)}
                </select>
              </div>
            </div>
          </div>
          <br />
          <nav className="level">
            <div className="level-item has-text-centered">
              <div>
                <p className="heading">Days</p>
                <input
                  className="input  has-text-centered"
                  min="0"
                  type="number"
                  value={this.state.hour}
                  onChange={this.handleChangeHour}
                />
              </div>
            </div>
            <div className="level-item has-text-centered">
              <div>
                <p className="heading">Price</p>
                <input
                  className="input  has-text-centered"
                  min="0"
                  type="number"
                  value={this.state.price}
                  onChange={this.handleChangePrice}
                />
              </div>
            </div>
          </nav>
          <button
            className="button is-success is-outlined is-fullwidth my-3"
            onClick={this.edit}
            type="submit"
          >
            Save
          </button>
        </div>
      </div>
    )
  }
}

export default connect()(EditBundle)
