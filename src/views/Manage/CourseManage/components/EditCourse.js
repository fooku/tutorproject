import React, { Component } from 'react'
import { connect } from 'react-redux'
import { updateCourse } from '../../../../actions/course'
import baseurl from '../../../../services/baseurl'

class EditCourse extends Component {
  constructor(props) {
    super(props)
    const {
      name,
      detail,
      thumbnail,
      price,
      hour,
      Timestamp
    } = this.props.course
    this.state = {
      name,
      detail,
      thumbnail,
      price,
      hour,
      Timestamp
    }
  }

  handleChangeName = event => {
    this.setState({ name: event.target.value })
  }
  handleChangeDetail = event => {
    this.setState({ detail: event.target.value })
    event.preventDefault()
  }
  handleChangePrice = event => {
    this.setState({ price: event.target.value })
    event.preventDefault()
  }
  handleChangeHour = event => {
    this.setState({ hour: event.target.value })
    event.preventDefault()
  }

  edit = () => {
    console.log(this.props.course.id)
    const { name, detail, thumbnail, price, hour } = this.state
    const course = {
      name: name,
      hour: hour,
      price: price,
      detail: detail,
      thumbnail: thumbnail
    }
    this.props.dispatch(updateCourse(this.props.course.id, course))
  }

  onHandleImg(img) {
    this.setState({
      thumbnail: baseurl + img
    })
  }
  renderListImage(list) {
    console.log(list)
    return (
      list &&
      list.map((imgs, i) => {
        console.log(imgs)

        return (
          <option
            key={i}
            value={imgs.img}
            onClick={() => this.onHandleImg(imgs.img)}
          >
            {imgs.name}
          </option>
        )
      })
    )
  }

  render() {
    const { img } = this.props
    console.log(img)
    return (
      <div className="columns">
        <div className="column has-text-centered">
          <figure className="image">
            <img src={this.state.thumbnail} alt="img course" />
          </figure>
          <br />
          <div className="select is-fullwidth is-multiple mt-3">
            <select multiple size={img.length > 4 ? '4' : img.length}>
              {this.renderListImage(img)}
            </select>
          </div>
        </div>
        <div className="column py-0">
          <input
            className="input is-large mb-3 has-text-centered"
            type="text"
            placeholder="Large input"
            value={this.state.name}
            onChange={this.handleChangeName}
          />
          <textarea
            className="textarea mt-3"
            value={this.state.detail}
            onChange={this.handleChangeDetail}
            rows="9"
          />
          <br />
          <nav className="level">
            <div className="level-item has-text-centered">
              <div>
                <p className="heading">section</p>
                <p className="title">4</p>
              </div>
            </div>
            <div className="level-item has-text-centered">
              <div>
                <p className="heading">lectures</p>
                <p className="title">12</p>
              </div>
            </div>
            <div className="level-item has-text-centered">
              <div>
                <p className="heading">Days</p>
                <input
                  className="input  has-text-centered"
                  min="0"
                  type="number"
                  value={this.state.hour}
                  onChange={this.handleChangeHour}
                />
              </div>
            </div>
            <div className="level-item has-text-centered">
              <div>
                <p className="heading">Price</p>
                <input
                  className="input  has-text-centered"
                  min="0"
                  type="number"
                  value={this.state.price}
                  onChange={this.handleChangePrice}
                />
              </div>
            </div>
          </nav>
          <button
            className="button is-success is-outlined is-fullwidth my-3"
            onClick={this.edit}
            type="submit"
          >
            Save
          </button>
        </div>
      </div>
    )
  }
}

export default connect()(EditCourse)
