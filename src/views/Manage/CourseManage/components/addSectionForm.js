import React from 'react'
import { Field, reduxForm } from 'redux-form'
import { connect } from 'react-redux'
import { compose } from 'recompose'

const addSectionForm = props => {
  const { handleSubmit } = props
  return (
    <div onSubmit={handleSubmit}>
      <span className="is-size-2">Add Section</span>
      <form className="modal-in mt-2">
        <div className="field">
          <div className="control has-icons-left">
            <Field
              component="input"
              name="name"
              className="input is-rounded"
              placeholder="name"
            />
            <span className="icon is-small is-left">
              <i className="fas fa-file-signature" />
            </span>
          </div>
        </div>
        <br />
        <div className="has-text-right mt-2">
          <button className="button is-info is-outlined" type="submit">
            <span>สร้างเซคชั่น</span>
            <span className="icon is-small">
              <i className="fas fa-plus-circle" />
            </span>
          </button>
        </div>
      </form>
    </div>
  )
}
const mapStateToProps = state => ({
  isFetching: state.course.isFetching
})

const WithReduxForm = reduxForm({
  form: 'login-form'
})

export default compose(
  WithReduxForm,
  connect(mapStateToProps)
)(addSectionForm)
