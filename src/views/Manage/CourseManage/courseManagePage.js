import React, { Component } from 'react'
import Appfooter from '../../../component/Appfooter'
import { connect } from 'react-redux'
import {
  getCourseOne,
  addSection,
  editPublish,
  editDoc
} from '../../../actions/course'
import { Link } from 'react-router-dom'
import SectionForm from './components/addSectionForm'
import Modal from 'react-responsive-modal'
import CardSection from '../../../component/Card/cardSection'
import EditCourse from './components/EditCourse'
import renderHTML from 'react-render-html'
import Switch from 'react-switch'
import { listImg } from '../../../actions/img'
import LoadingIn from '../../../component/LoadingIn'

class CourseManagePage extends Component {
  constructor(props) {
    super(props)
    this.handleChange = this.handleChange.bind(this)

    this.state = {
      open: false,
      openAddDoc: false,
      linkDoc: '',
      id: null,
      editCourse: false
    }
  }
  componentDidMount() {
    const { match, dispatch } = this.props
    const { course } = match.params

    this.setState({ id: course })
    dispatch(getCourseOne(course))
    dispatch(listImg())
  }
  componentWillReceiveProps() {
    this.setState({ editCourse: false })
  }

  handleSubmit = (values, dispatch) => {
    const { name } = values

    this.setState({ open: false })
    const section = {
      name: name
    }
    dispatch(addSection(section, this.state.id))
  }

  onOpenModal = () => {
    this.setState({ open: true })
  }

  onCloseModal = () => {
    this.setState({ open: false, openAddDoc: false, linkDoc: '' })
  }

  onEditCourse = () => {
    this.setState({ editCourse: !this.state.editCourse })
  }

  handleChange(checked) {
    const { course, dispatch } = this.props

    dispatch(editPublish(course.id, checked))
  }

  renderCard(list) {
    return (
      list &&
      list.map((course, index) => {
        return (
          <CardSection
            key={index}
            name={course.name}
            idsec={course.id}
            lectures={course.lectures}
            idcourse={this.state.id}
          />
        )
      })
    )
  }

  render() {
    const { isFetching, course, img, dispatch } = this.props
    const { linkDoc } = this.state
    if (isFetching || course.name === undefined) {
      return (
        <>
          <div className="hero is-primary is-medium">
            <div className="hero-body">
              <div className="container has-text-centered">
                <h1 className="animated fadeInLeft faster is-size-1">
                  Course Details
                </h1>
              </div>
            </div>
          </div>
          <div className="container my-5">
            <LoadingIn />
          </div>
        </>
      )
    } else {
      return (
        <React.Fragment>
          <div className="hero is-primary is-medium">
            <div className="hero-body">
              <div className="container has-text-centered">
                <h1 className="animated fadeInLeft faster is-size-1">
                  Course Details
                </h1>
              </div>
            </div>
          </div>
          <div className="container">
            <nav className="level">
              <div className="level-left">
                <Link to="/coursemanage" className="button is-text my-4">
                  <i className="fas fa-chevron-left" />
                  Back
                </Link>
              </div>

              <div className="level-right">
                {this.state.editCourse ? (
                  <button
                    className="button my-4 is-warning"
                    onClick={this.onEditCourse}
                  >
                    <span className="icon">
                      <i className="fas fa-times-circle" />
                    </span>
                    <span>Cancel Edit Course</span>
                  </button>
                ) : (
                  <button
                    className="button my-4 is-link"
                    onClick={this.onEditCourse}
                  >
                    <span className="icon">
                      <i className="fas fa-file-signature" />
                    </span>
                    <span> Edit Course</span>
                  </button>
                )}
              </div>
            </nav>

            {this.state.editCourse ? (
              <EditCourse course={course} img={img} />
            ) : (
              <div className="columns is-multiline">
                <div className="column has-text-centered is-half">
                  <figure className="image">
                    <img src={course.thumbnail} alt="img course" />
                  </figure>
                </div>
                <div className="column py-0 is-half">
                  <nav className="level">
                    <div className="level-left">
                      <div className="is-size-1 ml-4 mb-2">{course.name}</div>
                    </div>

                    <div className="level-right">
                      <Switch
                        onChange={this.handleChange}
                        checked={course.publish}
                        height={42}
                        width={77}
                      />
                    </div>
                  </nav>

                  {renderHTML(course.detail !== undefined ? course.detail : '')}
                </div>
                <div className="column is-half">
                  <br />
                  <nav className="level">
                    <div className="level-item has-text-centered">
                      <div>
                        <p className="heading">Section</p>
                        <p className="title">{course.sec}</p>
                      </div>
                    </div>
                    <div className="level-item has-text-centered">
                      <div>
                        <p className="heading">Lectures</p>
                        <p className="title">{course.lec}</p>
                      </div>
                    </div>
                    <div className="level-item has-text-centered">
                      <div>
                        <p className="heading">Days</p>
                        <p className="title">{course.hour}</p>
                      </div>
                    </div>
                    <div className="level-item has-text-centered">
                      <div>
                        <p className="heading">Price</p>
                        <p className="title">{course.price}฿</p>
                      </div>
                    </div>
                  </nav>
                </div>
                <div className="column is-half">
                  <button className="button mx-lg-3 mt-3 is-large is-success is-fullwidth is-outlined">
                    ตย. ปุ่ม
                  </button>
                </div>
              </div>
            )}
          </div>
          <div className="container my-5">
            <div className="box">
              <article className="media">
                <span className="p-3">
                  ลิ้งเอกสาร :{' '}
                  <a
                    target="_blank"
                    rel="noopener noreferrer"
                    href={course.doc}
                  >
                    {course.doc}
                  </a>
                </span>
              </article>
            </div>
            <nav className="level">
              <div className="level-left">
                {' '}
                <button
                  className="button is-link is-outlined mr-3"
                  onClick={() => this.setState({ openAddDoc: true })}
                >
                  <span className="icon">
                    <i className="fas fa-file-pdf" />
                  </span>
                  <span>เปลี่ยนที่อยู่เอกสาร</span>
                </button>
              </div>

              <div className="level-right">
                <button className="button is-dark" onClick={this.onOpenModal}>
                  <span className="icon">
                    <i className="fas fa-layer-group" />
                  </span>
                  <span>Add Section</span>
                </button>
              </div>
            </nav>

            <Modal
              open={this.state.open}
              onClose={this.onCloseModal}
              closeIconSize={52}
              center
            >
              <SectionForm onSubmit={this.handleSubmit} />
            </Modal>
            <Modal
              open={this.state.openAddDoc}
              onClose={this.onCloseModal}
              closeIconSize={42}
              center
            >
              <span className="is-size-3 mb-4">เพิ่มเอกสาร</span>
              <input
                className="input my-4"
                type="text"
                placeholder="link เอกสาร"
                value={linkDoc}
                onChange={e => this.setState({ linkDoc: e.target.value })}
              />
              <button
                className="button is-fullwidth"
                onClick={() => {
                  dispatch(editDoc(course.id, linkDoc))
                  this.onCloseModal()
                }}
              >
                บันทึก
              </button>
            </Modal>

            {course.section && this.renderCard(course.section)}
          </div>
          <Appfooter />
        </React.Fragment>
      )
    }
  }
}

const mapStateToProps = state => ({
  course: state.course.course,
  img: state.img.img,
  isFetching: state.course.isFetching
})

export default connect(mapStateToProps)(CourseManagePage)
