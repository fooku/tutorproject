import React, { Component } from 'react'
import Appfooter from '../../../component/Appfooter'
import { connect } from 'react-redux'
import { listCourseAll } from '../../../actions/course'
import { Link } from 'react-router-dom'
import EditBundle from './components/EditBundle'
import renderHTML from 'react-render-html'
import Switch from 'react-switch'
import { listImg } from '../../../actions/img'
import { getBundle, editPublishBundle } from '../../../actions/bundle'
import LoadingIn from '../../../component/LoadingIn'

class BundleManagePage extends Component {
  constructor(props) {
    super(props)
    this.handleChange = this.handleChange.bind(this)

    this.state = {
      open: false,
      id: null,
      editCourse: false,
      num: 100
    }
  }
  componentDidMount() {
    const { match, dispatch } = this.props
    const { bundle } = match.params

    this.setState({ id: bundle })
    console.log(bundle)

    dispatch(getBundle(bundle))
    dispatch(listCourseAll())
    dispatch(listImg())
  }
  componentWillReceiveProps() {
    this.setState({ editCourse: false })
  }

  onOpenModal = () => {
    this.setState({ open: true })
  }

  onCloseModal = () => {
    this.setState({ open: false })
  }

  onEditCourse = () => {
    this.setState({ editCourse: !this.state.editCourse })
  }

  handleChange(checked) {
    const { bundle, dispatch } = this.props

    dispatch(editPublishBundle(bundle.id, checked))
  }

  renderBox(list) {
    const { num } = this.state
    return (
      list &&
      list.map((cart, index) => {
        return (
          <div className="box my-2" key={index}>
            <article className="media">
              <div className="media-left">
                <figure className="image alligator-turtle">
                  <img src={cart.thumbnail} alt="cart item" />
                </figure>
              </div>
              <div className="media-content">
                <div className="content">
                  <div className="columns">
                    <div className="column is-half is-size-4">
                      {' '}
                      <p>
                        {' '}
                        <strong> {cart.name}</strong>
                      </p>
                    </div>
                    {/* <div className="column has-text-right	has-text-weight-bold is-size-4">
                      <span>{'฿' + cart.price}</span>
                    </div> */}
                    <div className="column has-text-right	has-text-weight-bold">
                      <span
                        style={{ cursor: 'pointer' }}
                        onClick={() =>
                          num === index
                            ? this.setState({ num: 100 })
                            : this.setState({ num: index })
                        }
                      >
                        อ่านลายละเอียดเพิ่มเติม
                      </span>
                    </div>
                  </div>
                  {num === index ? <p>{renderHTML(cart.detail)}</p> : null}
                </div>
              </div>
            </article>
          </div>
        )
      })
    )
  }

  render() {
    const { isFetching, bundle, img, courses } = this.props

    if (isFetching || bundle.name === undefined) {
      return (
        <>
          <div className="hero is-primary is-medium">
            <div className="hero-body">
              <div className="container has-text-centered">
                <h1 className="animated fadeInLeft faster is-size-1">
                  Course Details
                </h1>
              </div>
            </div>
          </div>
          <div className="container my-5">
            {' '}
            <LoadingIn />{' '}
          </div>
        </>
      )
    } else {
      return (
        <React.Fragment>
          <div className="hero is-primary is-medium">
            <div className="hero-body">
              <div className="container has-text-centered">
                <h1 className="animated fadeInLeft faster is-size-1">
                  Course Details
                </h1>
              </div>
            </div>
          </div>
          <div className="container">
            <nav className="level">
              <div className="level-left">
                <Link to="/coursemanage" className="button is-text my-4">
                  <i className="fas fa-chevron-left" />
                  Back
                </Link>
              </div>

              <div className="level-right">
                {this.state.editCourse ? (
                  <button
                    className="button my-4 is-warning"
                    onClick={this.onEditCourse}
                  >
                    <span className="icon">
                      <i className="fas fa-times-circle" />
                    </span>
                    <span>Cancel Edit Course</span>
                  </button>
                ) : (
                  <button
                    className="button my-4 is-link"
                    onClick={this.onEditCourse}
                  >
                    <span className="icon">
                      <i className="fas fa-file-signature" />
                    </span>
                    <span> Edit Course</span>
                  </button>
                )}
              </div>
            </nav>

            {this.state.editCourse ? (
              <EditBundle bundle={bundle} img={img} courses={courses} />
            ) : (
              <div className="columns mb-4 is-multiline">
                <div className="column has-text-centered is-half">
                  <figure className="image">
                    <img src={bundle.thumbnail} alt="img course" />
                  </figure>
                </div>
                <div className="column is-half">
                  <nav className="level">
                    <div className="level-left">
                      <div className="is-size-1 ml-4 mb-2">{bundle.name}</div>
                    </div>

                    <div className="level-right">
                      <Switch
                        onChange={this.handleChange}
                        checked={bundle.publish}
                        height={42}
                        width={77}
                      />
                    </div>
                  </nav>
                  <span style={{ display: 'block' }}>
                    {renderHTML(
                      bundle.detail !== undefined ? bundle.detail : ''
                    )}
                  </span>
                </div>
                <div className="column is-half">
                  <p className="is-size-3 has-text-centered my-4">
                    ภายในประกอบด้วย
                  </p>
                  {bundle.idcourse &&
                    bundle.idcourse.length > 0 &&
                    this.renderBox(bundle.idcourse)}
                </div>
                <div className="column is-half">
                  <nav className="level">
                    <div className="level-item has-text-centered">
                      <div>
                        <p className="heading">Price</p>
                        <p className="title">{bundle.price}฿</p>
                      </div>
                    </div>
                    <div className="level-item has-text-centered">
                      <div>
                        <p className="heading">Days</p>
                        <p className="title">{bundle.hour}</p>
                      </div>
                    </div>

                    <div className="level-item has-text-centered">
                      <button className="button mx-lg-3 mt-3 is-large is-success is-fullwidth is-outlined">
                        เพิ่มใส่รถเข็น
                      </button>
                    </div>
                  </nav>
                </div>
              </div>
            )}
          </div>

          <Appfooter />
        </React.Fragment>
      )
    }
  }
}

const mapStateToProps = state => ({
  courses: state.course.courses,
  bundle: state.bundle.dataOne,
  img: state.img.img,
  isFetching: state.course.isFetching
})

export default connect(mapStateToProps)(BundleManagePage)
