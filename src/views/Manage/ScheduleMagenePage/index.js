import React, { Component } from 'react'
import Appfooter from '../../../component/Appfooter'
import Table from './Table'
import { listSchedule } from '../../../actions/schedule'
import { connect } from 'react-redux'
import ModalAddCourse from './ModalAddCourse'
import SubjectTable from './SubjectTable'
import Modal from 'react-responsive-modal'

class ScheduleManagePage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      open: false
    }
  }

  componentDidMount() {
    window.scrollTo(0, 0)
    const { dispatch } = this.props

    dispatch(listSchedule())
  }

  onOpenModal = () => {
    this.setState({ open: true })
  }

  onCloseModal = () => {
    this.setState({ open: false })
  }

  render() {
    const { schedule, dispatch } = this.props
    console.log(schedule)

    return (
      <React.Fragment>
        <div className="hero is-light is-medium">
          <div className="hero-body">
            <div className="container has-text-centered	">
              <h1 className="animated flipInX is-size-1"> Schedule Manage</h1>
            </div>
          </div>
        </div>
        <div className="container is-fluid my-5">
          <div
            style={{
              justifyContent: 'space-between',
              alignItems: 'center'
            }}
            className="table-container"
          >
            {' '}
            <Table plan={schedule} />
          </div>
          <div className="field has-addons has-addons-right mt-4">
            <button className="button is-primary" onClick={this.onOpenModal}>
              เพิ่มวัน
            </button>
          </div>
          <Modal
            open={this.state.open}
            onClose={this.onCloseModal}
            closeIconSize={52}
            center
          >
            <p className="is-size-3">เพิ่มวัน</p>
            <hr />
            <ModalAddCourse
              plan={schedule}
              dispatch={dispatch}
              onCloseModal={this.onCloseModal}
            />
          </Modal>
          <SubjectTable plan={schedule} />
        </div>

        <Appfooter />
      </React.Fragment>
    )
  }
}

const mapStateToProps = state => ({
  schedule: state.schedule.data
})

export default connect(mapStateToProps)(ScheduleManagePage)
