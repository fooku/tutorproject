import React from 'react'
import RenderRow from './RenderRow'
const Table = React.forwardRef(({ plan }, ref) => {
  const x = '1.7ex'
  return (
    <table
      className="table is-bordered is-fullwidth has-text-centered center-table"
      style={{ fontSize: x }}
      ref={ref}
    >
      <thead>
        <tr>
          <th scope="col" />
          <th colSpan="2" scope="col">
            8:00-9:00
          </th>
          <th colSpan="2" scope="col">
            9:00-10:00
          </th>
          <th colSpan="2" scope="col">
            10:00-11:00
          </th>
          <th colSpan="2" scope="col">
            11:00-12:00
          </th>
          <th className="table-active" colSpan="2" scope="col">
            12:00-13:00
          </th>
          <th colSpan="2" scope="col">
            13:00-14:00
          </th>
          <th colSpan="2" scope="col">
            14:00-15:00
          </th>
          <th colSpan="2" scope="col">
            15:00-16:00
          </th>
          <th colSpan="2" scope="col">
            16:00-17:00
          </th>
          <th colSpan="2" scope="col">
            17:00-18:00
          </th>
          <th colSpan="2" scope="col">
            18:00-19:00
          </th>
          <th colSpan="2" scope="col">
            19:00-20:00
          </th>
          <th colSpan="2" scope="col">
            20:00-21:00
          </th>
          <th colSpan="2" scope="col">
            21:00-22:00
          </th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">Monday</th>
          {plan && plan.mon ? <RenderRow list={plan.mon} /> : null}
        </tr>
        <tr>
          <th scope="row">Tuesday</th>
          {plan && plan.tue ? <RenderRow list={plan.tue} /> : null}
        </tr>
        <tr>
          <th scope="row">Wednesday</th>
          {plan && plan.wed ? <RenderRow list={plan.wed} /> : null}
        </tr>
        <tr>
          <th scope="row">Thursday</th>
          {plan && plan.thu ? <RenderRow list={plan.thu} /> : null}
        </tr>
        <tr>
          <th scope="row">Friday</th>
          {plan && plan.fri ? <RenderRow list={plan.fri} /> : null}
        </tr>
        <tr>
          <th scope="row">Saturay</th>
          <RenderRow list={plan.sat} />
        </tr>
        <tr>
          <th scope="row">Sunday</th>
          <RenderRow list={plan.sun} />
        </tr>
        <tr>
          <td
            style={{
              textAlign: 'center',
              width: '50px',
              border: '0 solid'
            }}
          />
          <td
            style={{
              textAlign: 'center',
              width: '50px',
              border: '0 solid'
            }}
          />
          <td
            style={{
              textAlign: 'center',
              width: '50px',
              border: '0 solid'
            }}
          />
          <td
            style={{
              textAlign: 'center',
              width: '50px',
              border: '0 solid'
            }}
          />
          <td
            style={{
              textAlign: 'center',
              width: '50px',
              border: '0 solid'
            }}
          />
          <td
            style={{
              textAlign: 'center',
              width: '50px',
              border: '0 solid'
            }}
          />
          <td
            style={{
              textAlign: 'center',
              width: '50px',
              border: '0 solid'
            }}
          />
          <td
            style={{
              textAlign: 'center',
              width: '50px',
              border: '0 solid'
            }}
          />
          <td
            style={{
              textAlign: 'center',
              width: '50px',
              border: '0 solid'
            }}
          />
          <td
            style={{
              textAlign: 'center',
              width: '50px',
              border: '0 solid'
            }}
          />
          <td
            style={{
              textAlign: 'center',
              width: '50px',
              border: '0 solid'
            }}
          />
          <td
            style={{
              textAlign: 'center',
              width: '50px',
              border: '0 solid'
            }}
          />
          <td
            style={{
              textAlign: 'center',
              width: '50px',
              border: '0 solid'
            }}
          />
          <td
            style={{
              textAlign: 'center',
              width: '50px',
              border: '0 solid'
            }}
          />
          <td
            style={{
              textAlign: 'center',
              width: '50px',
              border: '0 solid'
            }}
          />
          <td
            style={{
              textAlign: 'center',
              width: '50px',
              border: '0 solid'
            }}
          />
          <td
            style={{
              textAlign: 'center',
              width: '50px',
              border: '0 solid'
            }}
          />
          <td
            style={{
              textAlign: 'center',
              width: '50px',
              border: '0 solid'
            }}
          />
          <td
            style={{
              textAlign: 'center',
              width: '50px',
              border: '0 solid'
            }}
          />
          <td
            style={{
              textAlign: 'center',
              width: '50px',
              border: '0 solid'
            }}
          />
          <td
            style={{
              textAlign: 'center',
              width: '50px',
              border: '0 solid'
            }}
          />
          <td
            style={{
              textAlign: 'center',
              width: '50px',
              border: '0 solid'
            }}
          />
          <td
            style={{
              textAlign: 'center',
              width: '50px',
              border: '0 solid'
            }}
          />
          <td
            style={{
              textAlign: 'center',
              width: '50px',
              border: '0 solid'
            }}
          />
          <td
            style={{
              textAlign: 'center',
              width: '50px',
              border: '0 solid'
            }}
          />
          <td
            style={{
              textAlign: 'center',
              width: '50px',
              border: '0 solid'
            }}
          />
          <td
            style={{
              textAlign: 'center',
              width: '50px',
              border: '0 solid'
            }}
          />
          <td
            style={{
              textAlign: 'center',
              width: '50px',
              border: '0 solid'
            }}
          />
          <td
            style={{
              textAlign: 'center',
              width: '50px',
              border: '0 solid'
            }}
          />
        </tr>
      </tbody>
    </table>
  )
})
export default Table
