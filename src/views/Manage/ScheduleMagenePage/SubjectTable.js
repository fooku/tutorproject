import React from 'react'
import Swal from 'sweetalert2'
import { connect } from 'react-redux'
import { UpdateSchedule } from '../../../actions/schedule'
// import { updatePlan } from '../../../actions/plans'
// import ModalEditSubject from './ModalEditSubject'

const SubjectTable = ({ plan, dispatch }) => {
  // const [subID, setSubID] = useState(null)

  const deleteSubject = index => {
    Swal.fire({
      title: 'Are you sure?',
      text: `คุณต้องการลบใช่ไหม`,
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#45B39D',
      cancelButtonColor: '#CD6155',
      confirmButtonText: 'Yes'
    }).then(result => {
      if (result.value) {
        if (
          plan.subject[index].positions &&
          plan.subject[index].positions.length > 0
        ) {
          plan.subject[index].positions.forEach(position => {
            plan[position.day] = plan[position.day].map((item, i) => {
              if (
                position.front % 2 === 0 &&
                item.sequene === position.front - 1
              ) {
                if (!item.end) {
                  item.half = false
                }
              }
              if (position.end % 2 === 1 && item.sequene === position.end + 1) {
                if (!item.show) {
                  item.half = false
                }
              }

              if (
                item.sequene >= position.front &&
                item.sequene <= position.end
              ) {
                item.half = false
                item.end = false
                item.name = ''
                item.color = ''
                item.show = false

                if (
                  i !== 27 &&
                  item.sequene === position.end &&
                  plan[position.day][i + 1].show
                ) {
                  item.half = true
                }

                if (
                  i !== 0 &&
                  plan[position.day][i - 1].show &&
                  item.sequene === position.front
                ) {
                  item.half = true
                }
                return item
              } else {
                return item
              }
            })
          })
        }

        plan.subject.splice(index, 1)
        // setSubID(null)

        dispatch(UpdateSchedule(plan, 'ลบเรียบร้อยแล้ว'))
      }
    })
  }
  return (
    <>
      {plan ? (
        <>
          <table
            className="table is-striped is-fullwidth has-text-centered center-table my-3"
            style={{ border: ' 2px solid #dee2e6' }}
          >
            <thead>
              <tr>
                <th scope="col">ชื่อ</th>
                <th scope="col">วัน-เวลา</th>
                <th scope="col" />
              </tr>
            </thead>
            <tbody>
              {plan.subject && plan.subject.length > 0 ? (
                plan.subject.map((subject, index) => {
                  return (
                    <tr key={`subject_${index}`}>
                      <td>{subject.name}</td>
                      <td>
                        {subject.daytime &&
                          subject.daytime.map((itam, index) => {
                            if (index + 1 === subject.daytime.length) {
                              return (
                                <span key={`${itam}-${index}`}>{itam}</span>
                              )
                            } else {
                              return <p key={`${itam}-${index}`}>{itam}</p>
                            }
                          })}
                      </td>

                      <td>
                        {/* <button
                            type="button"
                            className="button"
                            data-toggle="modal"
                            data-target="#ModalEditSubject"
                            // onClick={() => setSubID(index)}
                          >
                            แก้ไข
                          </button> */}
                        <button
                          type="button"
                          className="button is-danger is-light"
                          onClick={() => deleteSubject(index)}
                        >
                          ลบ
                        </button>
                      </td>
                    </tr>
                  )
                })
              ) : (
                <tr>
                  <td colSpan="8">ไม่มีวันที่ว่าง หรือ ไม่ว่าง 555</td>
                </tr>
              )}
            </tbody>
          </table>
          {/* <ModalEditSubject subID={subID} plan={plan} /> */}
        </>
      ) : null}
    </>
  )
}

export default connect()(SubjectTable)
