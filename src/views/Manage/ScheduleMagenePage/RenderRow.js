import React from 'react'

const RenderRow = ({ list }) => {
  var showEmpty = 0
  var showColor = 0
  return (
    <>
      {list &&
        list.map((item, index) => {
          if (item.show) {
            showColor++
          } else {
            showEmpty++
          }

          if (item.half) {
            // แสดง ครึ่ง
            showEmpty = 0
            return <td key={index} colSpan="1" />
          } else if (item.end) {
            // แสดง วิชาเรียน
            const colSpan = showColor
            showColor = 0
            return (
              <td
                className="text-center , font-weight-bold"
                key={index}
                colSpan={colSpan}
                style={{ backgroundColor: item.color }}
              >
                {item.name}
              </td>
            )
          } else if (showEmpty === 2) {
            // แสดง ตารางปกติ
            showEmpty = 0
            return <td key={index} colSpan="2" />
          } else {
            return null
          }
        })}
    </>
  )
}

export default RenderRow
