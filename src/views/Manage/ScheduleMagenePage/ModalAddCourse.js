import React, { useState } from 'react'
// import { updatePlan } from '../../../actions/plans'
import Swal from 'sweetalert2'
import { UpdateSchedule } from '../../../actions/schedule'

const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3500
})

const ColorTable = [
  '#FE8372',
  '#F89C5F',
  '#FFD777',
  '#4298C5',
  '#4FC5D2',
  '#83E2DF'
]

const ModalAddCourse = ({ plan, dispatch, onCloseModal }) => {
  const [name, setName] = useState('')
  const [color, setColor] = useState('#e8ffae')
  const [date, setDate] = useState([
    {
      day: 'mon',
      front: 1,
      halfFront: false,
      end: 2,
      halfEnd: false
    }
  ])

  const check = d => {
    return (
      plan[d.day].filter(item => {
        return (
          item.sequene >= d.front && item.sequene <= d.end && item.show === true
        )
      }).length === 0
    )
  }

  const addCourse = (d, iColor) => {
    if (check(d)) {
      plan[d.day] = plan[d.day].filter(item => {
        if (d.front % 2 === 0 && item.sequene === d.front - 1) {
          if (!item.end) {
            item.half = true
          }
        }
        if (d.end % 2 === 1 && item.sequene === d.end + 1) {
          if (!item.show) {
            item.half = true
          }
        }
        if (item.sequene >= d.front && item.sequene <= d.end) {
          if (item.sequene === d.end) {
            item.end = true
            item.name = name
            item.color = color
          }
          item.half = false
          item.show = true
          return item
        } else {
          return item
        }
      })
    } else {
      // console.log('ในรายการที่เพิ่มมีเวลาซ่้ำกันในวัน ' + d.day)
    }
  }

  const matchDay = day => {
    switch (day) {
      case 'mon':
        return 'จันทร์'
      case 'tue':
        return 'อังคาร'
      case 'wed':
        return 'พุธ'
      case 'thu':
        return 'พฤหัสบดี'
      case 'fir':
        return 'ศุกร์'
      case 'sat':
        return 'เสาร์'
      case 'sun':
        return 'อาทิตย์'
      default:
        return 'อื่นๆ'
    }
  }

  const genDate = (f, e) => {
    let hf = '00'
    let he = '00'
    if (f % 2 === 0) {
      hf = '30'
    }
    if (e % 2 === 0) {
      he = '30'
    }

    return `${Math.ceil(f / 2) + 7}:${hf} - ${Math.ceil(e / 2) + 7}:${he}`
  }

  const test = () => {
    const iColor = ''
    if (date.every(check)) {
      const positions = []
      const daytime = []
      date.forEach(d => {
        addCourse(d, iColor)

        daytime.push(
          `วัน:${matchDay(d.day)} เวลา:${genDate(d.front, d.end + 1)}`
        )
        positions.push({
          day: d.day,
          front: d.front,
          end: d.end
        })
      })

      console.warn('AAAA', daytime)

      const subject = {
        daytime,
        color: '',
        nametable: name,
        name: name,
        positions
      }
      plan.subject.push(subject)

      console.warn(plan)

      dispatch(UpdateSchedule(plan, 'ลบเรียบร้อยแล้ว'))
      onCloseModal()
      setDate([
        {
          day: 'mon',
          front: 1,
          halfFront: false,
          end: 2,
          halfEnd: false
        }
      ])

      setName('')
    } else {
      Toast.fire({
        type: 'error',
        title: 'วันเวลาซ้ำ'
      })
    }
  }

  const handleOnChange = (event, index, name, isNum) => {
    event.preventDefault()

    date[index][name] = isNum
      ? parseInt(event.target.value, 10)
      : event.target.value

    if (name === 'front') {
      if (date[index].end < date[index].front) {
        date[index].end = date[index].front
      }
    } else if (name === 'end') {
      if (date[index].end < date[index].front) {
        date[index].front = date[index].end
      }
    }

    setDate([...date])
  }

  function arrayRemove(arr, value) {
    return arr.filter(function(ele, index) {
      return index !== value
    })
  }
  return (
    <div>
      <div className="input-group mb-3">
        <input
          style={{ borderRight: '0px' }}
          type="text"
          value={name}
          onChange={e => setName(e.target.value)}
          className="input"
          placeholder="ชื่อที่แสดงบนตาราง"
        />

        <input
          style={{ width: '100px' }}
          type="color"
          value={color}
          list="data"
          onChange={e => setColor(e.target.value)}
          className="input form-control mt-2"
          placeholder="สี"
        />
        <datalist id="data">
          {ColorTable.map((item, key) => (
            <option key={key} value={item} />
          ))}
        </datalist>
      </div>
      <div className="container">
        <div className="columns">
          <div className="is-one-fifth">
            <button
              type="button"
              className="button my-2 mx-3"
              onClick={() =>
                setDate([
                  ...date,
                  {
                    day: 'mon',
                    front: 1,
                    halfFront: false,
                    end: 2,
                    halfEnd: false
                  }
                ])
              }
              disabled={date.length > 6}
            >
              เพิ่มวัน
            </button>
          </div>

          <div className="is-four-fifths">
            {date &&
              date.map((v, index) => {
                return (
                  <div className="my-2 ml-3" key={index}>
                    <div className="select mr-2">
                      <select
                        className="select is-info"
                        value={date[index].day}
                        onChange={e => handleOnChange(e, index, 'day', false)}
                      >
                        <option value="mon">จันทร์</option>
                        <option value="tue">อังคาร</option>
                        <option value="wed">พุธ</option>
                        <option value="thu">พฤหัสบดี</option>
                        <option value="fri">ศุกร์</option>
                        <option value="sat">เสาร์</option>
                        <option value="sun">อาทิตย์</option>
                      </select>
                    </div>

                    <div className="select mr-2">
                      <select
                        className="select is-info"
                        value={date[index].front}
                        onChange={e => handleOnChange(e, index, 'front', true)}
                      >
                        <option value={1}>8:00</option>
                        <option value={2}>8:30</option>
                        <option value={3}>9:00</option>
                        <option value={4}>9:30</option>
                        <option value={5}>10:00</option>
                        <option value={6}>10:30</option>
                        <option value={7}>11:00</option>
                        <option value={8}>11:30</option>
                        <option value={9}>12:00</option>
                        <option value={10}>12:30</option>
                        <option value={11}>13:00</option>
                        <option value={12}>13:30</option>
                        <option value={13}>14:00</option>
                        <option value={14}>14:30</option>
                        <option value={15}>15:00</option>
                        <option value={16}>15:30</option>
                        <option value={17}>16:00</option>
                        <option value={18}>16:30</option>
                        <option value={19}>17:00</option>
                        <option value={20}>17:30</option>
                        <option value={21}>18:00</option>
                        <option value={22}>18:30</option>
                        <option value={23}>19:00</option>
                        <option value={24}>19:30</option>
                        <option value={25}>20:00</option>
                        <option value={26}>20:30</option>
                        <option value={27}>21:00</option>
                        <option value={28}>21:30</option>
                      </select>
                    </div>
                    <div className="select mr-2">
                      <select
                        style={{ maxWidth: '6rem' }}
                        className="select is-info"
                        value={date[index].end}
                        onChange={e => handleOnChange(e, index, 'end', true)}
                      >
                        <option value={1}>8:30</option>
                        <option value={2}>9:00</option>
                        <option value={3}>9:30</option>
                        <option value={4}>10:00</option>
                        <option value={5}>10:30</option>
                        <option value={6}>11:00</option>
                        <option value={7}>11:30</option>
                        <option value={8}>12:00</option>
                        <option value={9}>12:30</option>
                        <option value={10}>13:00</option>
                        <option value={11}>13:30</option>
                        <option value={12}>14:00</option>
                        <option value={13}>14:30</option>
                        <option value={14}>15:00</option>
                        <option value={15}>15:30</option>
                        <option value={16}>16:00</option>
                        <option value={17}>16:30</option>
                        <option value={18}>17:00</option>
                        <option value={19}>17:30</option>
                        <option value={20}>18:00</option>
                        <option value={21}>18:30</option>
                        <option value={22}>19:00</option>
                        <option value={23}>19:30</option>
                        <option value={24}>20:00</option>
                        <option value={25}>20:30</option>
                        <option value={26}>21:00</option>
                        <option value={27}>21:30</option>
                        <option value={28}>22:00</option>
                      </select>
                    </div>

                    <button
                      className="button is-danger is-light ml-2"
                      type="button"
                      id="button-addon2"
                      disabled={index === 0}
                      onClick={() => setDate(arrayRemove(date, index))}
                    >
                      ลบ
                    </button>
                  </div>
                )
              })}
          </div>
        </div>
      </div>
      <hr />
      <div className="mt-4">
        <button
          type="button"
          className="button is-medium is-fullwidth is-success is-light"
          onClick={() => test()}
        >
          เพิ่ม
        </button>
      </div>
    </div>
  )
}

export default ModalAddCourse
