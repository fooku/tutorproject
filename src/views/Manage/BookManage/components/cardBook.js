import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import renderHTML from 'react-render-html'
import { deleteBook } from '../../../../actions/book'
import Swal from 'sweetalert2'

class CardBook extends Component {
  onHandleDelete(id) {
    const { dispatch } = this.props
    Swal.fire({
      title: 'Are you sure?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then(result => {
      if (result.value) {
        dispatch(deleteBook(id))
      }
    })
  }

  render() {
    let { name, detail, img, id } = this.props
    const url = '/bookmanage/' + id
    if (detail.length > 130) detail = detail.substring(0, 130) + '...'
    return (
      <React.Fragment>
        <div className="column is-4">
          <div className="card is-fixed-height">
            <Link to={url}>
              <div className="card-image">
                <figure className="image is-5by6 book-crop">
                  <img className="overlay" src={img} alt="Placeholder" />
                </figure>
                <div className="middle">
                  <div className="text has-text-weight-bold color-6">
                    <i className="fas fa-wrench fa-5x" />
                  </div>
                </div>
              </div>
            </Link>
            <div className="card-content">
              <div className="content">
                <h1 className="has-text-centered">{name}</h1>
                {renderHTML(detail)}
              </div>
            </div>

            <footer className="card-footer">
              <span
                className="card-footer-item b has-text-danger"
                onClick={() => this.onHandleDelete(id)}
              >
                Delete{' '}
              </span>
            </footer>
          </div>
        </div>
      </React.Fragment>
    )
  }
}

export default connect()(CardBook)
