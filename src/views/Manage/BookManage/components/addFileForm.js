import React, { Component } from 'react'
import { reduxForm } from 'redux-form'
import { connect } from 'react-redux'
import { compose } from 'recompose'
import axios from 'axios'
import baseurl from '../../../../services/baseurl'
import { getBook } from '../../../../actions/book'
import Swal from 'sweetalert2'

class addFileForm extends Component {
  state = {
    selectedFile: '',
    loaded: 0,
    total: 99
  }

  handleselectedFile = event => {
    this.setState({
      selectedFile: event.target.files[0],
      loaded: 0
    })

    console.log(this.state.selectedFile)
  }

  file = async () => {
    var formData = new FormData()
    formData.append('file', this.state.selectedFile)
    const token = await localStorage.getItem('token')
    axios
      .post(
        baseurl + '/restricted/book/example?idBook=' + this.props.bookID,
        formData,
        {
          onUploadProgress: progressEvent => {
            this.setState({
              loaded: progressEvent.loaded,
              total: progressEvent.total
            })
          },
          headers: {
            'Content-Type': 'multipart/form-data',
            Authorization: token
          }
        }
      )
      .then(() => {
        let timerInterval
        Swal.fire({
          title: 'Uploading',
          html:
            '<div class="sk-cube-grid">' +
            '<div class="sk-cube sk-cube1"></div>' +
            '<div class="sk-cube sk-cube2"></div>' +
            '<div class="sk-cube sk-cube3"></div>' +
            '<div class="sk-cube sk-cube4"></div>' +
            '<div class="sk-cube sk-cube5"></div>' +
            '<div class="sk-cube sk-cube6"></div>' +
            '<div class="sk-cube sk-cube7"></div>' +
            '<div class="sk-cube sk-cube8"></div>' +
            '<div class="sk-cube sk-cube9"></div>' +
            '</div>',
          timer: 5000,
          timerProgressBar: true,
          showConfirmButton: false,
          didOpen: () => {
            Swal.showLoading()
            const b = Swal.getHtmlContainer().querySelector('h4')
            timerInterval = setInterval(() => {
              b.textContent = Swal.getTimerLeft()
            }, 100)
          },
          willClose: () => {
            clearInterval(timerInterval)
          }
        }).then(result => {
          /* Read more about handling dismissals below */
          if (result.dismiss === Swal.DismissReason.timer) {
            console.log('I was closed by the timer')
          }
        })
      })
      .catch(error => console.log(error))
    console.log('file', this.state.selectedFile)
  }

  render() {
    const { handleSubmit, dispatch, bookID } = this.props
    console.log(this.state.loaded)
    if (this.state.loaded === this.state.total) {
      setTimeout(() => {
        dispatch(getBook(bookID))
      }, 3000)
    }
    return (
      <div onSubmit={handleSubmit}>
        <span className="is-size-2">Add File</span>
        <form className="modal-in mt-2">
          <br />
          <div className="file has-name is-fullwidth">
            <label className="file-label">
              <input
                className="file-input"
                type="file"
                accept=".pdf"
                name="file"
                onChange={this.handleselectedFile}
              />
              <span className="file-cta">
                <span className="file-icon">
                  <i className="fas fa-upload" />
                </span>
                <span className="file-label">Choose a file…</span>
              </span>
              <span className="file-name">
                {this.state.selectedFile && this.state.selectedFile.name}
              </span>
            </label>
          </div>
          <br />
          {this.state.loaded !== 0 ? (
            <progress
              className="progress is-primary"
              value={this.state.loaded}
              max={this.state.total}
            >
              {(this.state.loaded * 100) / this.state.total}
            </progress>
          ) : null}

          <div className="has-text-right mt-2">
            <button
              className="button is-info is-outlined"
              type="submit"
              onClick={this.file}
            >
              <span>สร้างวีดีโอ</span>
              <span className="icon is-small">
                <i className="fas fa-plus-circle" />
              </span>
            </button>
          </div>
        </form>
      </div>
    )
  }
}
const mapStateToProps = state => ({
  isFetching: state.book.isFetching
})

const WithReduxForm = reduxForm({
  form: 'login-form'
})

export default compose(
  WithReduxForm,
  connect(mapStateToProps)
)(addFileForm)
