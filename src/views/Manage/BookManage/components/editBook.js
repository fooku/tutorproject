import React, { Component } from 'react'
import { connect } from 'react-redux'
import { updateBook } from '../../../../actions/book'
import baseurl from '../../../../services/baseurl_image'
import { Editor } from 'react-draft-wysiwyg'
import { EditorState, convertToRaw, ContentState } from 'draft-js'
import draftToHtml from 'draftjs-to-html'
import htmlToDraft from 'html-to-draftjs'

import '../../../../../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
class EditBook extends Component {
  constructor(props) {
    super(props)
    const { name, detail, thumbnail, price, Timestamp } = this.props.book

    const blocksFromHtml = htmlToDraft(detail)
    const { contentBlocks, entityMap } = blocksFromHtml
    const contentState = ContentState.createFromBlockArray(
      contentBlocks,
      entityMap
    )
    this.state = {
      name,
      detail,
      thumbnail,
      price,
      Timestamp,
      editorState: EditorState.createWithContent(contentState)
    }
  }

  handleChangeName = event => {
    this.setState({ name: event.target.value })
  }
  handleChangeDetail = event => {
    this.setState({ detail: event.target.value })
    event.preventDefault()
  }
  handleChangePrice = event => {
    this.setState({ price: event.target.value })
    event.preventDefault()
  }
  handleChangeHour = event => {
    this.setState({ hour: event.target.value })
    event.preventDefault()
  }

  onEditorStateChange = editorState => {
    console.log(
      'editorState',
      draftToHtml(convertToRaw(editorState.getCurrentContent()))
    )
    this.setState({
      editorState: editorState,
      detail: draftToHtml(convertToRaw(editorState.getCurrentContent()))
    })
  }

  edit = () => {
    const { name, detail, thumbnail, price } = this.state
    const book = {
      name: name,
      price: price,
      detail: detail,
      thumbnail: thumbnail
    }
    this.props.dispatch(updateBook(this.props.book.id, book))
  }

  onHandleImg(img) {
    this.setState({
      thumbnail: baseurl + img
    })
  }
  renderListImage(list) {
    console.log(list)
    return (
      list &&
      list.map((imgs, i) => {
        console.log(imgs)

        return (
          <option
            key={i}
            value={imgs.img}
            onClick={() => this.onHandleImg(imgs.img)}
          >
            {imgs.name}
          </option>
        )
      })
    )
  }

  render() {
    const { img } = this.props
    console.log(img)
    return (
      <div className="columns">
        <div className="column has-text-centered">
          <figure className="image">
            <img src={this.state.thumbnail} alt="img course" />
          </figure>
          <br />
          <div className="select is-fullwidth is-multiple mt-3">
            <select multiple size={img.length > 8 ? '8' : img.length}>
              {this.renderListImage(img)}
            </select>
          </div>
        </div>
        <div className="column py-0">
          <input
            className="input is-large mb-3 has-text-centered"
            type="text"
            placeholder="Large input"
            value={this.state.name}
            onChange={this.handleChangeName}
          />
          <Editor
            className="mt-3"
            editorState={this.state.editorState}
            wrapperClassName="demo-wrapper"
            editorClassName="textarea"
            onEditorStateChange={this.onEditorStateChange}
          />
          {/* <textarea
            className="textarea mt-3"
            value={this.state.detail}
            onChange={this.handleChangeDetail}
            rows="9"
          /> */}
          <br />
          <nav className="level">
            <div className="level-item has-text-centered">
              <div>
                <p className="heading">Price</p>
                <input
                  className="input  has-text-centered"
                  min="0"
                  type="number"
                  value={this.state.price}
                  onChange={this.handleChangePrice}
                />
              </div>
            </div>
          </nav>
          <button
            className="button is-success is-outlined is-fullwidth my-3"
            onClick={this.edit}
            type="submit"
          >
            Save
          </button>
        </div>
      </div>
    )
  }
}

export default connect()(EditBook)
