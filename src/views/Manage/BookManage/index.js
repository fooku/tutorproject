import React, { Component } from 'react'
import Appfooter from '../../../component/Appfooter'
import Modal from 'react-responsive-modal'
import BookForm from './components/addBookForm'
import { connect } from 'react-redux'
import CardBook from './components/cardBook'
import CardBundle from '../../../component/Card/cardBundle'
import LoadingIn from '../../../component/LoadingIn'
import {
  addBook,
  listBooks,
  listTraceBooks,
  updateLinkBooks
} from '../../../actions/book'

class BookManage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      open: false,
      tap: 'BOOK',
      editLinkIndex: null,
      newLinkInput: '',
      currentP: 1,
      num: 10
    }
  }
  componentDidMount() {
    const { dispatch } = this.props
    window.scrollTo(0, 0)

    dispatch(listBooks())
    dispatch(listTraceBooks())

    this.setState({ open: false })
  }

  handleSubmit = (values, dispatch) => {
    let { img, name, price, detail } = values
    if (img === '' || img === undefined) {
      img =
        'https://images.unsplash.com/photo-1429497419816-9ca5cfb4571a?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1502&q=80'
    }

    if (name === '' || name === undefined) {
      name = 'New Book'
    }

    if (price === '' || price === undefined) {
      price = '0'
    }
    this.setState({ open: false })
    console.log(name, price, detail, img)
    const course = {
      name: name,
      price: price,
      detail: detail,
      thumbnail: img,
      type: 'EBOOK'
    }
    dispatch(addBook(course))
  }

  onOpenModal = () => {
    this.setState({ open: true })
  }

  onCloseModal = () => {
    this.setState({ open: false })
  }

  renderCard(list) {
    return (
      list &&
      list.map((course, index) => {
        console.log(course, index)
        return (
          <CardBook
            key={index}
            id={course.id}
            name={course.name}
            detail={course.detail}
            img={course.thumbnail}
          />
        )
      })
    )
  }

  renderCardBundle(list) {
    return (
      list &&
      list.map((course, index) => {
        console.log(course, index)
        return (
          <CardBundle
            key={index}
            id={course.id}
            name={course.name}
            detail={course.detail}
            img={course.thumbnail}
          />
        )
      })
    )
  }

  changeTap(key) {
    // const { dispatch } = this.props

    // if (key === 'BOOK') {
    //   dispatch(listBooks())
    // } else {
    //   dispatch(listTraceBooks())
    // }
    this.setState({
      tap: key
    })
  }

  handleChange(event) {
    this.setState({ newLinkInput: event.target.value })
  }

  renderRow(tracingBooks) {
    const { num, currentP } = this.state
    const num1 = num * currentP
    const num2 = num * (currentP - 1)

    // const result =
    //   tracingBooks &&
    //   tracingBooks.filter(tracingBook => {
    //     return (
    //       (tracingBook.ID.includes(this.state.search) ||
    //         tracingBook.emailuser.includes(this.state.search)) &&
    //       tracingBook.status.includes(this.state.filterStatus)
    //     )
    //   })

    return (
      tracingBooks &&
      tracingBooks.map((tracingBook, index) => {
        if (index < num1 && index >= num2) {
          return (
            <tr key={index}>
              <th>
                <figure className="image alligator-turtle">
                  <img src={tracingBook.book.thumbnail} alt="book item" />
                </figure>
              </th>
              <th>{tracingBook.book.name}</th>
              <th>{tracingBook.user.name}</th>
              <th>{tracingBook.user.email}</th>
              <th>{tracingBook.book.price}</th>
              <th>
                {index === this.state.editLinkIndex ? (
                  <textarea
                    className="textarea is-link"
                    placeholder="PDF link "
                    value={this.state.newLinkInput}
                    onChange={e => this.handleChange(e)}
                  />
                ) : (
                  tracingBook.link
                )}
              </th>
              <th>
                {index === this.state.editLinkIndex ? (
                  <>
                    <button
                      class="button is-success mr-2"
                      onClick={() => {
                        this.props.dispatch(
                          updateLinkBooks(
                            tracingBook.id,
                            this.state.newLinkInput
                          )
                        )
                        this.setState({
                          editLinkIndex: null,
                          newLinkInput: ''
                        })
                      }}
                    >
                      Ok
                    </button>
                    <button
                      class="button is-danger"
                      onClick={() => {
                        this.setState({
                          editLinkIndex: null,
                          newLinkInput: ''
                        })
                      }}
                    >
                      Cancel
                    </button>
                  </>
                ) : (
                  <button
                    className="button is-link"
                    onClick={() => {
                      this.setState({
                        editLinkIndex: index,
                        newLinkInput: tracingBook.link
                      })
                    }}
                  >
                    Edit Link
                  </button>
                )}
              </th>
            </tr>
          )
        } else {
          return null
        }
      })
    )
  }
  renderP(items) {
    // const result =
    //   orders &&
    //   orders.filter(order => {
    //     return (
    //       (order.ID.includes(this.state.search) ||
    //         order.emailuser.includes(this.state.search)) &&
    //       order.status.includes(this.state.filterStatus)
    //     )
    //   })
    let sum = Math.ceil(items.length / this.state.num)
    return (
      items &&
      [...Array(sum).keys()].map(index => {
        if (index < 10) {
          return (
            <li key={index}>
              {index + 1 === this.state.currentP ? (
                <button
                  className="pagination-link is-current"
                  aria-label="Page 1"
                  aria-current="page"
                  onClick={() => this.changeP(index + 1)}
                >
                  {index + 1}
                </button>
              ) : (
                <button
                  className="pagination-link"
                  aria-label="Page 1"
                  aria-current="page"
                  onClick={() => this.changeP(index + 1)}
                >
                  {index + 1}
                </button>
              )}
            </li>
          )
        } else if (index === 10) {
          return (
            <li key={index}>
              <span className="pagination-ellipsis">&hellip;</span>
            </li>
          )
        }
        return null
      })
    )
  }

  changeP(c) {
    this.setState({ currentP: c })
  }

  async changeCP(e) {
    const cp = await e.target.value

    this.setState({ currentP: 1, num: cp })
    window.scrollTo({
      top: 310,
      left: 0,
      behavior: 'smooth'
    })
  }

  render() {
    const { open, tap } = this.state
    const { isFetching, list, traceBook } = this.props
    if (isFetching) {
      return (
        <>
          <div className="hero is-warning is-medium">
            <div className="hero-body">
              <div className="container has-text-centered">
                <h1 className="animated fadeInLeft faster is-size-1">
                  eBook Manager
                </h1>
              </div>
            </div>
          </div>
          <div>
            <div className="container">
              <LoadingIn />
            </div>
          </div>
        </>
      )
    } else {
      return (
        <React.Fragment>
          <div className="hero is-warning is-medium">
            <div className="hero-body">
              <div className="container has-text-centered">
                <h1 className="animated fadeInLeft faster is-size-1">
                  eBook Manager
                </h1>
              </div>
            </div>
            <div class="hero-foot">
              <nav class="tabs is-boxed is-fullwidth">
                <div class="container">
                  <ul>
                    <li
                      class={tap === 'BOOK' ? 'is-active' : ''}
                      onClick={() => this.changeTap('BOOK')}
                    >
                      <a href>
                        <b>eBooks</b>
                      </a>
                    </li>
                    <li class={tap === 'TRACE' ? 'is-active' : ''}>
                      <a href onClick={() => this.changeTap('TRACE')}>
                        <b>Trace Purchase eBooks</b>
                      </a>
                    </li>
                  </ul>
                </div>
              </nav>
            </div>
          </div>
          <div>
            {tap === 'BOOK' ? (
              <div className="container">
                <div className="field has-addons has-addons-right mt-4">
                  <button
                    className="button is-primary"
                    onClick={this.onOpenModal}
                  >
                    Add Book
                  </button>
                </div>
                <Modal
                  open={open}
                  onClose={this.onCloseModal}
                  closeIconSize={52}
                  center
                >
                  <BookForm onSubmit={this.handleSubmit} />
                </Modal>

                <div className="columns is-multiline my-5">
                  {list && list.length > 0 ? (
                    this.renderCard(list)
                  ) : (
                    <div className="content is-large center my-5 py-5">
                      <h1>
                        {' '}
                        <i className="fas fa-search fa-2x mr-3" />
                        Book not found
                      </h1>
                    </div>
                  )}
                </div>
              </div>
            ) : (
              <div className="container my-5">
                <table className="table is-fullwidth is-hoverable is-bordered">
                  <thead className="center-table">
                    <tr>
                      <th>รูป</th>
                      <th>ช่ือหนังสือ </th>
                      <th>ผู้ซื้อ </th>
                      <th>Email ผู้ซื้อ</th>
                      <th>ราคา</th>
                      <th>link</th>
                      <th />
                    </tr>
                  </thead>
                  <tbody className="center-table">
                    {traceBook &&
                      traceBook.length > 0 &&
                      this.renderRow(traceBook)}
                  </tbody>
                </table>
                <nav
                  className="pagination is-right"
                  role="navigation"
                  aria-label="pagination"
                >
                  <div className="select">
                    <select
                      defaultValue={this.state.currentP}
                      onChange={e => this.changeCP(e)}
                    >
                      <option value="10">10</option>
                      <option value="25">25</option>
                      <option value="50">50</option>
                      <option value="100">100</option>
                    </select>
                  </div>
                  <ul className="pagination-list">
                    {traceBook.length > 0 ? this.renderP(traceBook) : null}
                  </ul>
                </nav>
              </div>
            )}
          </div>
          <Appfooter />
        </React.Fragment>
      )
    }
  }
}

const mapStateToProps = state => ({
  list: state.book.data,
  traceBook: state.book.traceBook,
  isFetching: state.book.isFetching
})

export default connect(mapStateToProps)(BookManage)
