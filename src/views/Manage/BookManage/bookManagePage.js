import React, { Component } from 'react'
import Appfooter from '../../../component/Appfooter'
import { connect } from 'react-redux'
import { addSection, editDoc } from '../../../actions/course'
import { Link } from 'react-router-dom'

import Modal from 'react-responsive-modal'

import renderHTML from 'react-render-html'
import Switch from 'react-switch'
import { listImg } from '../../../actions/img'
import LoadingIn from '../../../component/LoadingIn'
import { getBook, updateBookPublish } from '../../../actions/book'
import EditBook from './components/editBook'
import Swal from 'sweetalert2'
import AddFileForm from './components/addFileForm'
import baseurl from '../../../services/baseurl'
import { isBrowser } from 'react-device-detect'

class BookManagePage extends Component {
  constructor(props) {
    super(props)
    this.handleChange = this.handleChange.bind(this)

    this.state = {
      open: false,
      openAddDoc: false,
      linkDoc: '',
      id: null,
      editCourse: false,
      openPDF: false
    }
  }
  componentDidMount() {
    const { match, dispatch } = this.props
    const { book } = match.params

    console.log('book', this.props.book.detail)
    this.setState({ id: book })
    dispatch(getBook(book))
    dispatch(listImg())
  }
  componentWillReceiveProps() {
    this.setState({ editCourse: false })
  }

  componentDidUpdate() {
    if (this.openPDF) {
      this.setState({
        openPDF: false
      })
    }
  }

  handleSubmit = (values, dispatch) => {
    const { name } = values

    this.setState({ open: false })
    const section = {
      name: name
    }
    dispatch(addSection(section, this.state.id))
  }

  handleSubmitPDF = (values, dispatch) => {
    // this.setState({ open: false })
    console.log(values, dispatch)

    this.setState({
      openPDF: false
    })
    // dispatch(getCourseOne(this.props.idcourse))
  }

  onOpenModal = () => {
    this.setState({ open: true })
  }

  onCloseModal = () => {
    this.setState({ open: false, openAddDoc: false, linkDoc: '' })
  }

  onEditCourse = () => {
    this.setState({ editCourse: !this.state.editCourse })
  }

  handleChange(checked) {
    const { book, dispatch } = this.props

    dispatch(updateBookPublish(book.id, checked))
  }

  openInNewTab = url => {
    const newWindow = window.open(url, '_blank', 'noopener,noreferrer')
    if (newWindow) newWindow.opener = null
  }

  onOpenPDF() {
    if (!isBrowser) {
      this.openInNewTab(`${baseurl}/${this.props.book.link_example}`)
      return
    }

    const { innerHeight: height } = window

    let totalHeight = height - 150

    if (totalHeight > 850) {
      totalHeight = 850
    }
    const path = `src="${baseurl}/${this.props.book.link_example}#view=fitH"`
    Swal.fire({
      icon: 'info',
      width: 600,
      heightAuto: true,
      html:
        '<iframe class="mt-1" ' +
        'title="pdf"' +
        ' width="100%"' +
        'height="' +
        totalHeight +
        'px"' +
        path +
        '   />',
      showCloseButton: true,
      showCancelButton: false,
      focusConfirm: false,
      showConfirmButton: false,
      background: '#323639'
    })
  }

  onOpenModalPDF = () => {
    this.setState({ openPDF: true })
  }

  onCloseModalPDF = () => {
    this.setState({ openPDF: false })
  }

  renderCard(list) {
    return (
      list &&
      list.map((course, index) => {
        return (
          // <CardSection
          //   key={index}
          //   name={course.name}
          //   idsec={course.id}
          //   lectures={course.lectures}
          //   idcourse={this.state.id}
          // />
          null
        )
      })
    )
  }

  render() {
    const { isFetching, book, img, dispatch } = this.props
    const { linkDoc } = this.state
    if (isFetching || book.name === undefined) {
      return (
        <>
          <div className="hero is-primary is-medium">
            <div className="hero-body">
              <div className="container has-text-centered">
                <h1 className="animated fadeInLeft faster is-size-1">
                  Course Details
                </h1>
              </div>
            </div>
          </div>
          <div className="container my-5">
            <LoadingIn />
          </div>
        </>
      )
    } else {
      return (
        <React.Fragment>
          <div className="hero is-primary is-medium">
            <div className="hero-body">
              <div className="container has-text-centered">
                <h1 className="animated fadeInLeft faster is-size-1">
                  eBook Details
                </h1>
              </div>
            </div>
          </div>
          <div className="container">
            <nav className="level">
              <div className="level-left">
                <Link to="/bookmanage" className="button is-text my-4">
                  <i className="fas fa-chevron-left" />
                  Back
                </Link>
              </div>

              <div className="level-right">
                {this.state.editCourse ? (
                  <button
                    className="button my-4 is-warning"
                    onClick={this.onEditCourse}
                  >
                    <span className="icon">
                      <i className="fas fa-times-circle" />
                    </span>
                    <span>Cancel Edit Course</span>
                  </button>
                ) : (
                  <>
                    <button
                      className="button my-4 is-info mr-3"
                      onClick={this.onOpenModalPDF}
                    >
                      <span> Upload Example PDF</span>
                    </button>
                    <button
                      className="button my-4 is-link"
                      onClick={this.onEditCourse}
                    >
                      <span className="icon">
                        <i className="fas fa-file-signature" />
                      </span>
                      <span> Edit Book</span>
                    </button>
                  </>
                )}
              </div>
            </nav>

            {this.state.editCourse ? (
              <EditBook book={book} img={img} />
            ) : (
              <div className="columns is-multiline">
                <div className="column has-text-centered is-4">
                  <div
                    class="card wrap-card"
                    onClick={() => {
                      book.link_example === ''
                        ? this.onOpenModalPDF()
                        : this.onOpenPDF()
                    }}
                  >
                    <div class="card-image">
                      <figure class="image">
                        <img
                          className="overlay"
                          src={book.thumbnail}
                          alt="img course"
                        />
                      </figure>
                      <div className="middle">
                        <div className="text has-text-weight-bold color-6">
                          {book.link_example === '' ? (
                            <>
                              <i className="fas fa-file-upload fa-5x mb-3" />
                              <p className="is-size-5	has-text-weight-bold">
                                Upload file
                              </p>
                            </>
                          ) : (
                            <>
                              <i className="fas fa-eye fa-5x mb-3" />
                              <p className="is-size-5	has-text-weight-bold">
                                look inside
                              </p>
                            </>
                          )}
                        </div>
                      </div>
                    </div>

                    <div class="small-footer-card overlay">
                      <div class="media-content has-text-centered">
                        <p class="is-4 has-text-weight-bold">
                          {book.link_example === '' ? (
                            <>Update example</>
                          ) : (
                            <>
                              <i className="fas fa-eye" /> look inside
                            </>
                          )}
                        </p>
                      </div>

                      <div class="content" />
                    </div>
                  </div>

                  <div className="card py-3 px-4 mt-4 border-radius-15">
                    <div className="my-1">
                      <nav className="level">
                        <div className="level-item has-text-centered">
                          <div>
                            <p className="heading">Price</p>
                            <p className="title">{book.price}฿</p>
                          </div>
                        </div>
                      </nav>
                    </div>
                    <div className="mt-3">
                      <button className="button is-large is-primary is-fullwidth">
                        ตย. ปุ่ม
                      </button>
                    </div>
                  </div>
                </div>

                <div className="column py-0 is-8 pl-5">
                  <nav className="level mb-0">
                    <div className="level-left">
                      <div className="is-size-1 mb-4">{book.name}</div>
                    </div>

                    <div className="level-right">
                      <Switch
                        onChange={this.handleChange}
                        checked={book.publish}
                        height={42}
                        width={77}
                      />
                    </div>
                  </nav>
                  <div className="content">
                    {renderHTML(book.detail !== undefined ? book.detail : '')}
                  </div>
                </div>
                {/* <div className="column is-4">
                  <nav className="level">
                    <div className="level-item has-text-centered">
                      <div>
                        <p className="heading">Price</p>
                        <p className="title">{book.price}฿</p>
                      </div>
                    </div>
                  </nav>
                </div>
                <div className="column is-8 pl-5">
                  <button className="button mx-lg-3 mt-3 is-large is-success is-fullwidth is-outlined">
                    ตย. ปุ่ม
                  </button>
                </div> */}
              </div>
            )}
          </div>
          <div className="container my-5">
            <Modal
              open={this.state.openPDF}
              onClose={this.onCloseModalPDF}
              closeIconSize={52}
              center
            >
              <AddFileForm onSubmit={this.handleSubmitPDF} bookID={book.id} />
            </Modal>
            <Modal
              open={this.state.open}
              onClose={this.onCloseModal}
              closeIconSize={52}
              center
            >
              {/* <SectionForm onSubmit={this.handleSubmit} /> */}
            </Modal>
            <Modal
              open={this.state.openAddDoc}
              onClose={this.onCloseModal}
              closeIconSize={42}
              center
            >
              <span className="is-size-3 mb-4">เพิ่มเอกสาร</span>
              <input
                className="input my-4"
                type="text"
                placeholder="link เอกสาร"
                value={linkDoc}
                onChange={e => this.setState({ linkDoc: e.target.value })}
              />
              <button
                className="button is-fullwidth"
                onClick={() => {
                  dispatch(editDoc(book.id, linkDoc))
                  this.onCloseModal()
                }}
              >
                บันทึก
              </button>
            </Modal>
          </div>
          <Appfooter />
        </React.Fragment>
      )
    }
  }
}

const mapStateToProps = state => ({
  book: state.book.dataOne,
  img: state.img.img,
  isFetching: state.book.isFetching
})

export default connect(mapStateToProps)(BookManagePage)
