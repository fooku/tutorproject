import React, { Component } from 'react'
import Appfooter from '../../../component/Appfooter'
import { connect } from 'react-redux'
import {
  listMember,
  deleteMember,
  updateUsersType
} from '../../../actions/users'
import Swal from 'sweetalert2'
import LoadingIn from '../../../component/LoadingIn'
import axios from 'axios'
import BASE_URL from '../../../services/baseurl'
class UserManage extends Component {
  constructor(props) {
    super(props)
    this.state = { id: '', userType: '', currentP: 1, num: 10, search: '' }
  }

  componentDidMount() {
    const { dispatch } = this.props
    window.scrollTo(0, 0)

    console.log(Math.ceil(82 / 10))
    dispatch(listMember())
  }

  async resetPassword(e) {
    const token = await localStorage.getItem('token')
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, ฉันจะรีเซ็ตรหัสผ่าน'
    }).then(result => {
      if (result.value) {
        const URL = BASE_URL + '/restricted/passtwo'
        const user = {
          email: e
        }

        console.log(token)
        if (token === '') {
          return null
        }
        token &&
          axios
            .post(URL, user, { headers: { Authorization: token } })
            .then(res => {
              if (res.status === 200) {
                Swal.fire(
                  'รีเซ็ตรหัสผ่านเรียวร้อย รหัสใหม่คือ',
                  res.data.newpassword,
                  'success'
                )
              }
            })
            .catch(err => console.log(err))
      }
    })
  }

  renderUser(listUsers, dispatch) {
    const num1 = this.state.num * this.state.currentP
    const num2 = this.state.num * (this.state.currentP - 1)
    const result = listUsers.filter(user => {
      return (
        user.email.includes(this.state.search) ||
        user.name.includes(this.state.search) ||
        user.usertype.includes(this.state.search) ||
        user.telephonenumber.includes(this.state.search)
      )
    })
    return (
      result &&
      result.map((user, index) => {
        if (index < num1 && index >= num2) {
          return (
            <tr key={index}>
              <th scope="row">{index + 1}</th>
              <td>{user.email}</td>
              <td>{user.name}</td>
              <td>{user.telephonenumber}</td>
              <td>{this.formatDate(user.timestamp)}</td>
              {this.state.id === user.ID ? (
                <td>
                  <div className="select is-rounded">
                    <select
                      defaultValue={user.usertype}
                      onChange={e => this.onSelect(e, user.ID)}
                    >
                      <option value="member">member</option>
                      <option value="admin">admin</option>
                    </select>
                  </div>
                </td>
              ) : (
                <td>{user.usertype}</td>
              )}
              <td>
                <button
                  type="button"
                  className="button is-info mr-1"
                  onClick={() => this.resetPassword(user.email)}
                >
                  ResetPass
                </button>
                {this.state.id === user.ID ? (
                  <React.Fragment>
                    {user.usertype === this.state.userType ? (
                      <button
                        type="button"
                        className="button is-success"
                        title="Disabled button"
                        disabled
                        onClick={() => this.editUserType()}
                      >
                        save
                      </button>
                    ) : (
                      <button
                        type="button"
                        className="button is-success"
                        onClick={() => this.editUserType()}
                      >
                        save
                      </button>
                    )}{' '}
                    <button
                      type="button"
                      className="button is-warning"
                      onClick={() => this.editClose(user.ID)}
                    >
                      ยกเลิก
                    </button>{' '}
                  </React.Fragment>
                ) : (
                  <button
                    type="button"
                    className="button is-warning"
                    onClick={() => this.editOpen(user.ID, user.usertype)}
                  >
                    แก้ไข
                  </button>
                )}{' '}
                {user.usertype === 'admin' ? (
                  <button
                    type="button"
                    className="button is-danger"
                    title="Disabled button"
                    disabled
                    onClick={() => dispatch(deleteMember(user.ID))}
                  >
                    ลบ
                  </button>
                ) : (
                  <button
                    type="button"
                    className="button is-danger"
                    onClick={() =>
                      Swal({
                        title: 'แน่ใจไหม?',
                        text:
                          'ต้องการจะลบผู้ใช้ : ' + user.email + ' จริงๆใช่ปะ',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        cancelButtonText: 'ยกเลิก ไม่ลบแล้ว',
                        confirmButtonText: 'ใช่ , จะลบแน่ๆเลยละ'
                      }).then(result => {
                        if (result.value) {
                          dispatch(deleteMember(user.ID))
                        }
                      })
                    }
                  >
                    ลบ
                  </button>
                )}
              </td>
            </tr>
          )
        }

        return null
      })
    )
  }

  renderP(listUsers) {
    console.log('1', listUsers.length)
    const result = listUsers.filter(user => {
      return (
        user.email.includes(this.state.search) ||
        user.name.includes(this.state.search) ||
        user.usertype.includes(this.state.search) ||
        user.telephonenumber.includes(this.state.search)
      )
    })
    let sum = Math.ceil(result.length / this.state.num)
    console.log('sum', sum)
    return (
      listUsers &&
      [...Array(sum).keys()].map(index => {
        if (index < 10) {
          return (
            <li key={index}>
              {index + 1 === this.state.currentP ? (
                <button
                  className="pagination-link is-current"
                  aria-label="Page 1"
                  aria-current="page"
                  onClick={() => this.changeP(index + 1)}
                >
                  {index + 1}
                </button>
              ) : (
                <button
                  className="pagination-link"
                  aria-label="Page 1"
                  aria-current="page"
                  onClick={() => this.changeP(index + 1)}
                >
                  {index + 1}
                </button>
              )}
            </li>
          )
        } else if (index === 10) {
          return (
            <li key={index}>
              <span className="pagination-ellipsis">&hellip;</span>
            </li>
          )
        }
        return null
      })
    )
  }

  changeP(c) {
    console.log(c)
    this.setState({ currentP: c })
  }

  editUserType() {
    const user = {
      usertype: this.state.userType
    }
    this.props.dispatch(updateUsersType(this.state.id, user))
    this.setState({ id: '', userType: '' })
  }

  editOpen(id, usertype) {
    console.log(id)
    this.setState({ id: id, userType: usertype })
  }
  editClose() {
    this.setState({ id: '', userType: '' })
  }

  async onSelect(e, id) {
    const selectType = await e.target.value

    this.setState({ userType: selectType })
    console.log(selectType, id)
  }

  async changeCP(e) {
    const cp = await e.target.value

    this.setState({ currentP: 1, num: cp })
    window.scrollTo({
      top: 310,
      left: 0,
      behavior: 'smooth'
    })
  }

  formatDate(date) {
    let d = new Date(date)
    let month = '' + (d.getMonth() + 1)
    let day = '' + d.getDate()
    let year = d.getFullYear()

    if (month.length < 2) month = '0' + month
    if (day.length < 2) day = '0' + day

    return [day, month, year].join('-')
  }

  render() {
    const { list, dispatch, isFetching } = this.props

    if (isFetching) {
      return (
        <>
          <div className="hero is-info is-medium">
            <div className="hero-body">
              <div className="container has-text-centered">
                <h1 className="animated fadeInLeft faster is-size-1	">
                  Users Manager
                </h1>
              </div>
            </div>
          </div>
          <div className="container mt-3 mb">
            {' '}
            <LoadingIn />
          </div>
        </>
      )
    } else {
      return (
        <React.Fragment>
          <div className="hero is-info is-medium">
            <div className="hero-body">
              <div className="container has-text-centered">
                <h1 className="animated fadeInLeft faster is-size-1	">
                  Users Manager
                </h1>
              </div>
            </div>
          </div>
          <div className="container mt-3 mb">
            <div className="field has-addons has-addons-right">
              <div className="control mb-2">
                <input
                  className="input"
                  type="text"
                  placeholder="search"
                  onChange={e =>
                    this.setState({ search: e.target.value, currentP: 1 })
                  }
                />
              </div>
            </div>
            <table className="table is-fullwidth is-hoverable">
              <thead className="center-table">
                <tr>
                  <th>
                    <abbr>ลำดับ</abbr>
                  </th>
                  <th>อีเมล</th>
                  <th>
                    <abbr>ชื่อ</abbr>
                  </th>
                  <th>
                    <abbr>โทรศัพท์</abbr>
                  </th>
                  <th>
                    <abbr>วันที่สมัคร</abbr>
                  </th>
                  <th>
                    <abbr>บทบาท</abbr>
                  </th>
                  <th>
                    <abbr>จัดการสมาชิก</abbr>
                  </th>
                </tr>
              </thead>
              <tbody className="center-table">
                {list.length > 0 && this.renderUser(list, dispatch)}
              </tbody>
            </table>

            <nav
              className="pagination is-right"
              role="navigation"
              aria-label="pagination"
            >
              <div className="select">
                <select
                  defaultValue={this.state.currentP}
                  onChange={e => this.changeCP(e)}
                >
                  <option value="10">10</option>
                  <option value="25">25</option>
                  <option value="50">50</option>
                  <option value="100">100</option>
                </select>
              </div>
              <ul className="pagination-list">
                {list.length > 0 && this.renderP(list)}
              </ul>
            </nav>
          </div>
          <Appfooter />
        </React.Fragment>
      )
    }
  }
}

const mapStateToProps = state => ({
  list: state.users.listUsers,
  isFetching: state.users.isFetching
})

export default connect(mapStateToProps)(UserManage)
