import React, { Component } from 'react'
import Appfooter from '../../../../component/Appfooter'
import { connect } from 'react-redux'
import { getOrderOne, confirmPayment } from '../../../../actions/order'
import { Link } from 'react-router-dom'
import baseurl from '../../../../services/baseurl_image'
import Swal from 'sweetalert2'

class OrderDetailManagePage extends Component {
  constructor(props) {
    super(props)

    this.state = {
      bookCart: [],
      orderID: ''
    }
  }

  componentDidMount() {
    window.scrollTo(0, 0)
    const { match, dispatch } = this.props
    const { id } = match.params

    dispatch(getOrderOne(id))
  }

  componentDidUpdate() {
    const { order, isFetching } = this.props
    if (isFetching) {
      return
    }

    if (order.cart && order.ID !== this.state.orderID) {
      console.log('enter')
      this.setState({
        bookCart: order.cart.map(item => {
          return {
            courseid: item.courseid,
            coursesid: item.coursesid,
            item: item.item,
            name: item.name,
            price: item.price,
            img: item.img,
            bundle: item.bundle,
            type: item.type,
            link: ''
          }
        }),
        orderID: order.ID
      })
    }
  }

  objectsAreSame(x, y) {
    var objectsAreSame = true
    for (var propertyName in x) {
      if (x[propertyName] !== y[propertyName]) {
        objectsAreSame = false
        break
      }
    }
    return objectsAreSame
  }

  formatDate(date) {
    let d = new Date(date)
    let month = '' + (d.getMonth() + 1)
    let day = '' + d.getDate()
    let year = d.getFullYear()

    if (month.length < 2) month = '0' + month
    if (day.length < 2) day = '0' + day

    return [day, month, year].join('-')
  }

  renderRow() {
    return this.state.bookCart.map((cart, index) => {
      return (
        <tr key={index}>
          <td style={{ width: '10%' }}>
            {' '}
            <figure className="image alligator-turtle">
              <img src={cart.img} alt="cart item" />
            </figure>
          </td>
          <td>{cart.name}</td>
          <th>{cart.price}</th>
          <th>
            {cart.type === 'EBOOK' ? (
              <input
                component="input iss-link"
                className="input"
                placeholder="PDF Link"
                value={cart.link}
                onChange={event => {
                  let items = this.state.bookCart

                  this.setState({
                    bookCart: [
                      ...items.slice(0, index),
                      {
                        ...items[index],
                        link: event.target.value
                      },
                      ...items.slice(index + 1)
                    ]
                  })
                }}
              />
            ) : (
              <span>book only</span>
            )}
          </th>
        </tr>
      )
    })
  }

  onHandleConfirmPayment(id) {
    const { dispatch } = this.props

    Swal.fire({
      title: 'Are you sure?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then(result => {
      if (result.value) {
        const book = this.state.bookCart.filter(item => item.type === 'EBOOK')

        dispatch(
          confirmPayment(id, {
            items: book
          })
        )
      }
    })
  }

  render() {
    const { order } = this.props

    return (
      <React.Fragment>
        <div className="hero is-warning is-medium">
          <div className="hero-body">
            <div className="container">
              <h1 className="animated fadeInLeft faster is-size-1	">
                Manage Order Detail
              </h1>
              <h2 className="animated fadeInLeft "> Manage Order Detail</h2>
            </div>
          </div>
        </div>
        <div className="container my-5">
          <nav className="breadcrumb" aria-label="breadcrumbs">
            <ul>
              <li>
                <Link to={'/ordermanage'}>
                  <span className="icon is-small">
                    <i className="fas fa-clipboard-list" />
                  </span>
                  <span>Manage Order</span>
                </Link>
              </li>
              <li className="is-active">
                {' '}
                <Link to="/">
                  <span className="icon is-small">
                    <i className="fas fa-map-marker-alt" />
                  </span>
                  <span>Manage Order Detail</span>
                </Link>
              </li>
            </ul>
          </nav>

          <div className="columns is-multiline">
            <div className="column is-half">
              <p className="is-size-4 mb-2">ข้อมูลคำสั่งซื้อ</p>
              <p>{'คำสั่งซื้อ : ' + order.ID}</p>
              <p>{'วันที่สั่งซื้อ : ' + this.formatDate(order.timestamp)}</p>
              <p>{'ผู้ซื้อ : ' + order.emailuser}</p>
              <p>{'สถานะ : ' + order.status}</p>
              {order.address !== undefined ? (
                <React.Fragment>
                  <hr />
                  <p className="is-size-4 my-2">ที่อยู่จัดส่ง :</p>
                  <p>{'ชื่อ นามสกุล : ' + order.address.name}</p>
                  <p>{'เบอร์โทรศัพท์ : ' + order.address.tel}</p>
                  <p>{'ที่อยู่ : ' + order.address.address}</p>
                  <p>{'เขต/อำเภอ : ' + order.address.district}</p>
                  <p>{'จังหวัด : ' + order.address.province}</p>
                  <p>{'รหัสไปรษณีย์ : ' + order.address.postcode}</p>
                </React.Fragment>
              ) : null}
            </div>
            <div className="column is-half">
              <div className="columns is-multiline">
                <div className="column is-full" style={{ height: '180px' }}>
                  <p className="is-size-4 mb-2">ข้อมูลการโอน</p>
                  {order.payment !== undefined && order.payment.bank !== '' ? (
                    <React.Fragment>
                      <p>{'ธนาคาร : ' + order.payment.bank}</p>
                      <p>{'เบอร์โทร : ' + order.payment.telephonenumber}</p>
                      <p>{'วันที่ : ' + order.payment.date}</p>
                      <p>{'เวลา : ' + order.payment.time}</p>
                      <p>{'จำนวนเงินที่โอน : ' + order.payment.total}</p>
                    </React.Fragment>
                  ) : (
                    <p>ยังไม่มีการชำระเงิน</p>
                  )}
                </div>
                <div className="column is-full">
                  <p className="is-size-4 mb-2">รูปสลิปการโอนเงิน</p>
                  {order.payment2 && order.payment2 !== '' ? (
                    <figure className="image">
                      <img src={baseurl + order.payment2} alt="payment" />
                    </figure>
                  ) : (
                    <i className="fas fa-image fa-10x" />
                  )}
                </div>
              </div>
            </div>
            <div className="column is-full">
              <p className="is-size-4 mt-5 mb-2">รายการสินค้าที่ซื้อ</p>
              <table className="table is-fullwidth is-hoverable is-bordered">
                <thead>
                  <tr>
                    <th />
                    <th>ชื่อสินค้า </th>
                    <th>ราคา</th>
                    <th>Link</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th colSpan="3">ราคารวม</th>
                    <th>{order.total}</th>
                  </tr>
                </tfoot>
                <tbody>{this.renderRow()}</tbody>
              </table>
              {order.status === 'คำสั่งซื้อถูกอนุมัติแล้ว' ? (
                <button
                  className="button mt-3 is-large is-primary is-fullwidth"
                  disabled
                >
                  <i className="fas fa-check-circle mr-2" />
                  ยืนยันการชำระเงิน
                </button>
              ) : (
                <button
                  className="button mt-3 is-large is-primary is-fullwidth"
                  onClick={() => this.onHandleConfirmPayment(order.ID)}
                >
                  <i className="fas fa-check-circle mr-2" />
                  ยืนยันการชำระเงิน
                </button>
              )}
            </div>
          </div>
        </div>
        <Appfooter />
      </React.Fragment>
    )
  }
}

const mapStateToProps = state => ({
  order: state.order.order,
  isFetching: state.order.isFetching
})

export default connect(mapStateToProps)(OrderDetailManagePage)
