import React, { Component } from 'react'
import Appfooter from '../../../component/Appfooter'
import { connect } from 'react-redux'
import { listImg } from '../../../actions/img'
import Card from '../../../component/Card/cardImg'
import baseurl from '../../../services/baseurl'
import axios from 'axios'

class UploadImagePage extends Component {
  constructor(props) {
    super(props)

    this.state = {
      selectedFile: '',
      loaded: 0
    }
  }
  componentDidMount() {
    window.scrollTo(0, 0)
    const { dispatch } = this.props

    dispatch(listImg())
  }

  componentWillReceiveProps() {
    this.setState({
      selectedFile: '',
      loaded: 0
    })
  }

  handleselectedFile = event => {
    this.setState({
      selectedFile: event.target.files[0],
      loaded: 0
    })

    console.log(event.target.files[0])
  }

  renderCard(list, isFetching) {
    return (
      list &&
      list.map((imgs, index) => {
        return (
          <Card
            key={index}
            id={imgs.id}
            img={imgs.img}
            name={imgs.name}
            isFetching={isFetching}
          />
        )
      })
    )
  }

  file = async () => {
    const { dispatch } = this.props
    const { selectedFile } = this.state
    if (selectedFile !== '') {
      var formData = new FormData()
      formData.append('file', this.state.selectedFile)
      const token = await localStorage.getItem('token')
      axios
        .post(baseurl + '/restricted/img', formData, {
          onUploadProgress: progressEvent => {
            this.setState({
              loaded: progressEvent.loaded,
              total: progressEvent.total
            })
            console.log(progressEvent.loaded, progressEvent.total)
          },
          headers: {
            'Content-Type': 'multipart/form-data',
            Authorization: token
          }
        })
        .then(res => {
          console.log(res)
          dispatch(listImg())
        })
    }
  }

  render() {
    const { img, isFetching } = this.props
    const { selectedFile, loaded, total } = this.state

    return (
      <React.Fragment>
        <div className="hero is-warning is-medium">
          <div className="hero-body">
            <div className="container has-text-centered">
              <h1 className="animated fadeInLeft faster is-size-1	">
                Upload Image
              </h1>
            </div>
          </div>
        </div>
        <div className="container my-5">
          <div className="columns is-multiline">
            <div className="column is-9">
              <div className="file has-name is-fullwidth">
                <label className="file-label">
                  <input
                    className="file-input"
                    type="file"
                    accept="image/*"
                    name="file"
                    onChange={this.handleselectedFile}
                  />
                  <span className="file-cta">
                    <span className="file-icon">
                      <i className="fas fa-upload" />
                    </span>
                    <span className="file-label">Choose a file…</span>
                  </span>
                  <span className="file-name">
                    {selectedFile && selectedFile.name !== undefined
                      ? selectedFile.name
                      : ''}
                  </span>
                </label>
              </div>
              {loaded !== 0 ? (
                <progress
                  className="progress is-primary"
                  value={loaded}
                  max={total}
                >
                  {(loaded * 100) / total}
                </progress>
              ) : null}
            </div>
            <div className="column is-1">
              <button
                className="button is-link is-outlined"
                onClick={this.file}
              >
                <span className="icon mr-1">
                  <i className="fas fa-cloud-upload-alt" />
                </span>
                Upload Image
              </button>
            </div>
          </div>

          <div className="columns is-multiline">
            {img && img.length > 0 && this.renderCard(img, isFetching)}
          </div>
        </div>
        <Appfooter />
      </React.Fragment>
    )
  }
}

const mapStateToProps = state => ({
  img: state.img.img,
  isFetching: state.img.isFetching
})

export default connect(mapStateToProps)(UploadImagePage)
