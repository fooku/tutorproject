import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import api from '../../services/api'
import Swal from 'sweetalert2'
import AuthImg from '../../img/auth.jpg'

class ProfilePage extends Component {
  constructor(props) {
    super(props)
    const Img = styled.abbr`
      background: url(${AuthImg}) center;
      background-size: cover;
      height: 100vh;
      display: flex;
    `

    this.state = {
      pass: '',
      passNew: '',
      passNewTwo: '',
      Img
    }
  }

  componentDidMount() {
    window.scrollTo(0, 0)
  }

  componentWillReceiveProps() {
    this.setState({
      pass: '',
      passNew: '',
      passNewTwo: ''
    })
  }

  async changePassword(email) {
    const { pass, passNew, passNewTwo } = this.state
    if (passNew === passNewTwo) {
      const data = {
        email: email,
        password: pass,
        newpassword: passNew
      }
      try {
        await api.changePass(data)
        Swal.fire({
          title: 'เปลี่ยนรหัสผ่านเรียบร้อยแล้ว',
          type: 'success'
        })
      } catch (error) {
        console.log('error', error.response.data)
        Swal.fire({
          title: error.response.data.message,
          type: 'warning'
        })
      }
    }
  }

  onChangeInput(name, even) {
    if (name === 'pass') {
      this.setState({
        pass: even.target.value
      })
    } else if (name === 'passNew') {
      this.setState({
        passNew: even.target.value
      })
    } else if (name === 'passNewTwo') {
      this.setState({
        passNewTwo: even.target.value
      })
    }
  }

  render() {
    const { list } = this.props
    const { pass, passNew, passNewTwo, Img } = this.state
    const screenSize = window.screen.width < 1275
    const dis =
      pass !== '' &&
      passNew !== '' &&
      passNewTwo !== '' &&
      passNew.length > 5 &&
      passNewTwo.length > 5
    return screenSize ? (
      <>
        <div className="hero is-light is-medium mb-4">
          <div className="hero-body">
            <div className="container has-text-centered	">
              <h1 className="animated flipInX is-size-1">Reset Password</h1>
              <h2 className="animated flipInX ">เปลี่ยนรหัสผ่าน</h2>
            </div>
          </div>
        </div>
        <div className="container">
          <div className="columns">
            <div className="column has-text-grey-dark">
              <div className="margin-pro">
                <div className="field">
                  <p className="control has-icons-left has-icons-right">
                    <input
                      className={'input'}
                      type="text"
                      placeholder="รหัสผ่านเดิม"
                      value={pass}
                      onChange={e => this.onChangeInput('pass', e)}
                    />
                    <span className="icon is-small is-left">
                      <i className="fas fa-unlock" />
                    </span>
                  </p>
                </div>
                <div className="field">
                  <p className="control has-icons-left has-icons-right">
                    <input
                      className="input"
                      type="text"
                      placeholder="รหัสผ่านใหม่"
                      value={passNew}
                      onChange={e => this.onChangeInput('passNew', e)}
                    />
                    <span className="icon is-small is-left">
                      <i className="fas fa-key" />
                    </span>
                  </p>
                </div>

                <div className="field">
                  <p className="control has-icons-left has-icons-right">
                    <input
                      className="input"
                      type="text"
                      placeholder="ยืนยันรหัสผ่านใหม่"
                      value={passNewTwo}
                      onChange={e => this.onChangeInput('passNewTwo', e)}
                    />
                    <span className="icon is-small is-left">
                      <i className="fas fa-key" />
                    </span>
                  </p>
                </div>

                <p className="has-text-danger	">รหัสผ่านใหม่ 6 หลักขึ้นไป *</p>
                <div className="buttons has-addons is-centered mt-4">
                  <button
                    className={'button is-medium is-success'}
                    disabled={!dis}
                    onClick={() => this.changePassword(list.email)}
                  >
                    เปลี่ยนรหัสผ่าน
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    ) : (
      <React.Fragment>
        <div className="hero is-primary is-fullheight">
          <Img>
            <div className="container">
              <div className="columns mt padding_10 animated pulse">
                <div className="column profile has-text-grey-dark">
                  <div className="margin-pro">
                    <p className="is-size-1 mb-4">เปลี่ยนรหัสผ่าน</p>

                    <div className="field">
                      <p className="control has-icons-left has-icons-right">
                        <input
                          className={'input'}
                          type="text"
                          placeholder="รหัสผ่านเดิม"
                          value={pass}
                          onChange={e => this.onChangeInput('pass', e)}
                        />
                        <span className="icon is-small is-left">
                          <i className="fas fa-unlock" />
                        </span>
                      </p>
                    </div>
                    <div className="field">
                      <p className="control has-icons-left has-icons-right">
                        <input
                          className="input"
                          type="text"
                          placeholder="รหัสผ่านใหม่"
                          value={passNew}
                          onChange={e => this.onChangeInput('passNew', e)}
                        />
                        <span className="icon is-small is-left">
                          <i className="fas fa-key" />
                        </span>
                      </p>
                    </div>

                    <div className="field">
                      <p className="control has-icons-left has-icons-right">
                        <input
                          className="input"
                          type="text"
                          placeholder="ยืนยันรหัสผ่านใหม่"
                          value={passNewTwo}
                          onChange={e => this.onChangeInput('passNewTwo', e)}
                        />
                        <span className="icon is-small is-left">
                          <i className="fas fa-key" />
                        </span>
                      </p>
                    </div>

                    <p className="has-text-danger	">
                      รหัสผ่านใหม่ 6 หลักขึ้นไป *
                    </p>
                    <div className="buttons has-addons is-centered mt-4">
                      <button
                        className={'button is-medium is-success'}
                        disabled={!dis}
                        onClick={() => this.changePassword(list.email)}
                      >
                        เปลี่ยนรหัสผ่าน
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </Img>
        </div>
      </React.Fragment>
    )
  }
}

const mapStateToProps = state => ({
  list: state.auth.currentUser,
  isFetching: state.users.isFetching
})

export default connect(mapStateToProps)(ProfilePage)
