import React from 'react'
import { Link } from 'react-router-dom'
import ErrorMessage from '../../../component/ErrorMessage'
import { Field, reduxForm } from 'redux-form'
import { connect } from 'react-redux'
import { compose } from 'recompose'
import './Login.css'

const LoginForm = props => {
  const { error, handleSubmit, isFetching } = props
  return (
    <div className="padding_login" onSubmit={handleSubmit}>
      <form className="columns form_login">
        <div className="column margin-login has-text-centered">
          <label className="label is-size-3">LOGIN</label>
          <br />
          <div className="field">
            <div className="control has-icons-left">
              <Field
                component="input"
                name="email"
                className="input"
                type="email"
                placeholder="Email"
              />
              <span className="icon is-small is-left">
                <i className="fas fa-envelope" />
              </span>
            </div>
          </div>
          <br />
          <div className="field">
            <p className="control has-icons-left">
              <Field
                component="input"
                name="password"
                className="input"
                type="password"
                placeholder="Password"
              />
              <span className="icon is-small is-left">
                <i className="fas fa-lock" />
              </span>
            </p>
          </div>
          {error ? <ErrorMessage message={error.login} /> : null}

          <p>
            Not registered?
            <Link to="/register" className="ml-2">
              Create a new account
            </Link>
          </p>
          <br />
          <div className="field is-grouped is-grouped-centered">
            {isFetching ? (
              <React.Fragment>
                <button
                  className="button is-success is-loading mr-3"
                  type="submit"
                >
                  Login
                </button>
                <div className="control">
                  <Link
                    to="/"
                    className="button is-text"
                    title="Disabled button"
                    disabled
                  >
                    Cancel
                  </Link>
                </div>
              </React.Fragment>
            ) : (
              <React.Fragment>
                <button className="button is-success mr-3" type="submit">
                  Login
                </button>
                <div className="control">
                  <Link to="/" className="button is-text">
                    Cancel
                  </Link>
                </div>
              </React.Fragment>
            )}
          </div>
        </div>
      </form>
    </div>
  )
}
const mapStateToProps = state => ({
  isFetching: state.auth.isFetching,
  error: state.auth.error
})

const WithReduxForm = reduxForm({
  form: 'login-form'
})

export default compose(
  WithReduxForm,
  connect(mapStateToProps)
)(LoginForm)
