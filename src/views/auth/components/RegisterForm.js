import React from 'react'
import { Link } from 'react-router-dom'
import { Field, reduxForm } from 'redux-form'
import { compose } from 'recompose'
import { connect } from 'react-redux'
import './Register.css'

import ErrorMessage from '../../../component/ErrorMessage'

const RegisterForm = props => {
  const { error, handleSubmit, isFetching } = props
  // const [isAccept, setIsAccept] = useState(false)

  return (
    <form onSubmit={handleSubmit} className="padding_register">
      <div className="columns form_register">
        <div className="column is-one-third register" />
        <div className="column form_register2 has-text-left	">
          <label className="label is-size-3">REGISTRATION INFO</label>
          <br />
          <div className="field">
            <div className="control has-icons-left">
              <Field
                component="input"
                name="email"
                className="input "
                type="email"
                placeholder="Email"
              />
              <span className="icon is-small is-left">
                <i className="fas fa-envelope" />
              </span>
            </div>
          </div>
          <br />
          <div className="field">
            <p className="control has-icons-left">
              <Field
                component="input"
                name="password"
                className="input "
                type="password"
                placeholder="Password"
              />
              <span className="icon is-small is-left">
                <i className="fas fa-lock" />
              </span>
            </p>
          </div>
          <br />
          <div className="field">
            <p className="control has-icons-left">
              <Field
                component="input"
                name="passwordConfirm"
                className="input "
                type="password"
                placeholder="Password Confirm"
              />
              <span className="icon is-small is-left">
                <i className="fas fa-lock" />
              </span>
            </p>
          </div>
          <br />
          <div className="field">
            <p className="control has-icons-left">
              <Field
                component="input"
                name="name"
                className="input "
                type="text"
                placeholder="name"
              />
              <span className="icon is-small is-left">
                <i className="fas fa-user" />
              </span>
            </p>
          </div>
          <br />
          <div className="field">
            <p className="control has-icons-left">
              <Field
                component="input"
                name="tel"
                className="input "
                type="text"
                placeholder="Telephone Number"
              />
              <span className="icon is-small is-left">
                <i className="fas fa-mobile-alt" />
              </span>
            </p>
          </div>
          <br />

          <label class="checkbox mb-4">
            {/* <input value="isAccept" type="checkbox" /> */}
            <Field
              name="isAccept"
              id="isAccept"
              component="input"
              type="checkbox"
            />
            <span className="ml-2">ฉันยอมรับ</span>
            <Link to="/policy" target="_blank" rel="noopener noreferrer">
              นโยบายความเป็นส่วนตัว
            </Link>
          </label>

          {error ? <ErrorMessage message={error.register} /> : null}
          <div className="field is-grouped">
            {isFetching ? (
              <React.Fragment>
                <div className="control">
                  <button
                    className="button is-success is-loading"
                    type="submit"
                  >
                    Create account
                  </button>
                </div>
                <div className="control">
                  <Link
                    to="/"
                    className="button is-text"
                    title="Disabled button"
                    disabled
                  >
                    Cancel
                  </Link>
                </div>
              </React.Fragment>
            ) : (
              <React.Fragment>
                <div className="control">
                  <button className="button is-success" type="submit">
                    Create account
                  </button>
                </div>
                <div className="control">
                  <Link to="/" className="button is-text">
                    Cancel
                  </Link>
                </div>
              </React.Fragment>
            )}
          </div>
        </div>
      </div>
    </form>
  )
}
const WithReduxForm = reduxForm({
  form: 'login-register'
})

const mapStateToProps = state => ({
  isFetching: state.auth.isFetching,
  error: state.auth.error
})

export default compose(
  WithReduxForm,
  connect(mapStateToProps)
)(RegisterForm)
