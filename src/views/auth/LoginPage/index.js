import React from 'react'
import styled from 'styled-components'
import LoginForm from '../components/LoginForm'
import { login } from '../../../actions/auth'
import AuthImg from '../../../img/auth.jpg'
const handleSubmit = (values, dispatch) => {
  const { email, password } = values

  const user = {
    email: email,
    password: password
  }

  dispatch(login(user))
}
const Img = styled.abbr`
  background: url(${AuthImg}) center center;
  background-size: cover;
  height: 100vh;
  display: flex;
`

export default () => (
  <Img>
    <LoginForm onSubmit={handleSubmit} />
  </Img>
)
