import React from 'react'
import RegisterForm from '../components/RegisterForm'
import styled from 'styled-components'
import { errorRegister, register } from '../../../actions/auth'
import AuthImg from '../../../img/auth.jpg'

const handleSubmit = (values, dispatch) => {
  const { email, password, passwordConfirm, name, tel, isAccept } = values
  console.log(email, password, passwordConfirm, name, tel, isAccept)
  if (!email || !password || !passwordConfirm || !name || !tel) {
    dispatch(errorRegister('กรุณากรอกข้อมูลให้ครบ'))
    return
  }
  if (password !== passwordConfirm) {
    dispatch(errorRegister('รหัสไม่ตรงกัน'))
    return
  }
  if (tel.length !== 10) {
    dispatch(errorRegister('กรุณากรอกเบอร์โทรให้ครบ 10 หลัก'))
    return
  }
  if (!isAccept) {
    dispatch(errorRegister('กรุณายอมรับข้อตกลง'))
    return
  }
  const user = {
    email: email,
    password: password,
    name: name,
    telephonenumber: tel
  }
  dispatch(register(user))
}
const Img = styled.abbr`
  background: url(${AuthImg}) center center;
  background-size: cover;
  height: 100vh;
  display: flex;
`
export default () => (
  <Img>
    <RegisterForm onSubmit={handleSubmit} />
  </Img>
)
