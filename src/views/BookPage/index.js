import React, { Component } from 'react'
import Appfooter from '../../component/Appfooter'
import { connect } from 'react-redux'
import LoadingIn from '../../component/LoadingIn'
import { isMobile } from 'react-device-detect'
import Card from './components/bookCart'
import { listBooksPublished } from '../../actions/book'

class BookPage extends Component {
  componentDidMount() {
    const { dispatch } = this.props
    window.scrollTo(0, 0)

    dispatch(listBooksPublished())
  }

  renderCard(list) {
    return (
      list &&
      list.map((course, index) => {
        console.log(course, index)
        return (
          <Card
            key={index}
            id={course.id}
            name={course.name}
            detail={course.detail}
            img={course.thumbnail}
            price={course.price}
            path="/book/"
          />
        )
      })
    )
  }

  render() {
    const { list, isFetching } = this.props

    if (isFetching) {
      return (
        <>
          {isMobile ? (
            <div className="hero is-light is-medium">
              <div className="hero-body">
                <div className="container has-text-centered">
                  <h2 className="animated flipInX is-size-1">Books</h2>
                  <h3 className="animated flipInX ">หนังสือ</h3>
                </div>
              </div>
            </div>
          ) : (
            <div className="hero is-light is-medium">
              <div className="hero-body">
                <div className="container has-text-centered">
                  <h1 className="animated flipInX  is-size-1	">Books</h1>
                  <h2 className="animated flipInX ">หนังสือ</h2>
                </div>
              </div>
            </div>
          )}
          <div className="container my-5">
            <LoadingIn />
          </div>
        </>
      )
    } else {
      return (
        <React.Fragment>
          <div className="hero is-light is-medium">
            <div className="hero-body has-text-dark	">
              <div className="container has-text-centered	">
                <h1 className="animated flipInX is-size-1">eBooks</h1>
                <h2 className="animated flipInX ">หนังสือ</h2>
              </div>
            </div>
          </div>
          <div className="container">
            <div className="columns is-vcentered my-5">
              <div className="column is-12 has-text-centered">
                {list ? (
                  <div className="columns is-multiline">
                    {list.length > 0 && this.renderCard(list)}
                  </div>
                ) : (
                  <>
                    <p>
                      <i className="far fa-clock fa-5x mb-4" />
                    </p>
                    <p className="is-size-3">Coming Soon!</p>
                  </>
                )}
              </div>
            </div>
          </div>
          <Appfooter />
        </React.Fragment>
      )
    }
  }
}

const mapStateToProps = state => ({
  list: state.book.data,
  isFetching: state.book.isFetching
})

export default connect(mapStateToProps)(BookPage)
