import React, { Component } from 'react'
import Appfooter from '../../../component/Appfooter'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import CardSection from '../../../component/Card/cardSectionPublish'
import renderHTML from 'react-render-html'
import { addItem } from '../../../actions/cart'
import LoadingIn from '../../../component/LoadingIn'
import Swal from 'sweetalert2'
import { push } from 'connected-react-router'
import { getBookPublished } from '../../../actions/book'
import baseurl from '../../../services/baseurl'
import { isBrowser } from 'react-device-detect'

class DetailPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      id: null
    }
  }
  componentDidMount() {
    const { match, dispatch } = this.props
    const { book } = match.params

    this.setState({ id: book })
    dispatch(getBookPublished(book))
  }

  renderCard(list) {
    return (
      list &&
      list.map((course, index) => {
        return (
          <CardSection
            key={index}
            name={course.name}
            idsec={course.id}
            lectures={course.lectures}
            idcourse={this.state.id}
          />
        )
      })
    )
  }

  onHandleAddItem(book) {
    const { dispatch, currentUser, isAuthenticated } = this.props
    if (isAuthenticated) {
      const data = {
        item: book.id,
        name: book.name,
        price: parseInt(book.price, 10),
        img: book.thumbnail,
        type: book.type
      }
      dispatch(addItem(currentUser.ID, data))
    } else {
      Swal.fire({
        title: 'Please login.',
        text: 'กรุณา login เพื่อเข้าสู่ระบบ',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'login'
      }).then(result => {
        if (result.value) {
          dispatch(push('/login'))
        }
      })
    }
  }

  openInNewTab = url => {
    const newWindow = window.open(url, '_blank', 'noopener,noreferrer')
    if (newWindow) newWindow.opener = null
  }

  onOpenPDF() {
    if (!isBrowser) {
      this.openInNewTab(`${baseurl}/${this.props.book.link_example}`)
      return
    }

    const { innerHeight: height } = window

    let totalHeight = height - 150

    if (totalHeight > 850) {
      totalHeight = 850
    }
    const path = `src="${baseurl}/${this.props.book.link_example}#view=fitH"`
    Swal.fire({
      icon: 'info',
      width: 600,
      heightAuto: true,
      html:
        '<iframe class="mt-1" ' +
        'title="pdf"' +
        ' width="100%"' +
        'height="' +
        totalHeight +
        'px"' +
        path +
        '   />',
      showCloseButton: true,
      showCancelButton: false,
      focusConfirm: false,
      showConfirmButton: false,
      background: '#323639'
    })
  }

  render() {
    const { isFetching, book, cart } = this.props
    let haveItem = false
    haveItem =
      cart.filter(item => {
        return item.item === book.id
      }).length > 0

    if (isFetching) {
      return (
        <>
          <div className="hero is-light is-medium">
            <div className="hero-body">
              <div className="container has-text-centered">
                <h1 className="animated flipInX is-size-1">Course Details</h1>
              </div>
            </div>
          </div>
          <div className="container my-5">
            <LoadingIn />
          </div>
        </>
      )
    } else {
      return (
        <React.Fragment>
          <div className="hero is-light is-medium">
            <div className="hero-body">
              <div className="container has-text-centered">
                <h1 className="animated flipInX is-size-1">eBook Details</h1>
              </div>
            </div>
          </div>
          <div className="container mb-5">
            <nav className="level">
              <div className="level-left">
                <Link to="/book" className="button is-text my-4">
                  <i className="fas fa-chevron-left" />
                  Back
                </Link>
              </div>
            </nav>

            <div className="columns is-multiline">
              <div className="column is-4 has-text-centered">
                <div
                  class="card wrap-card"
                  onClick={() => {
                    book.link_example === ''
                      ? Swal.fire(
                          'ขออภัย',
                          'ยังไม่มีตัวอย่างในขณะนี้',
                          'warning'
                        )
                      : this.onOpenPDF()
                  }}
                >
                  <div class="card-image">
                    <figure class="image">
                      <img
                        className="overlay"
                        src={book.thumbnail}
                        alt="img course"
                      />
                    </figure>
                    <div className="middle">
                      <div className="text has-text-weight-bold color-6">
                        {book.link_example === '' ? (
                          <></>
                        ) : (
                          <>
                            <i className="fas fa-eye fa-5x mb-3" />
                            <p className="is-size-5	has-text-weight-bold">
                              look inside
                            </p>
                          </>
                        )}
                      </div>
                    </div>
                  </div>

                  <div class="small-footer-card overlay">
                    <div class="media-content has-text-centered">
                      <p class="is-4 has-text-weight-bold">
                        {book.link_example === '' ? (
                          <>-</>
                        ) : (
                          <>
                            <i className="fas fa-eye" /> look inside
                          </>
                        )}
                      </p>
                    </div>

                    <div class="content" />
                  </div>
                </div>

                <div className="card py-3 px-4 mt-4 border-radius-15">
                  <div className="my-1">
                    <nav className="level">
                      <div className="level-item has-text-centered">
                        <div>
                          <p className="heading">Price</p>
                          <p className="title">{book.price}฿</p>
                        </div>
                      </div>
                    </nav>
                  </div>
                  <div className="mt-3">
                    {haveItem ? (
                      <Link
                        to="/cart"
                        className="button is-large is-info is-fullwidth"
                      >
                        ไปที่รถเข็น
                      </Link>
                    ) : (
                      <button
                        className="button is-large is-primary is-fullwidth"
                        onClick={() => this.onHandleAddItem(book)}
                      >
                        เพิ่มลงรถเข็น
                      </button>
                    )}
                  </div>
                </div>
              </div>
              <div className="column is-8 pl-5 pt-0">
                <div className="is-size-1 mb-4">{book.name}</div>

                <div className="content">
                  {book.detail !== undefined ? renderHTML(book.detail) : null}
                </div>
              </div>
              {/* <div className="column is-3">
                <br />
                <nav className="level">
                  <div className="level-item has-text-centered">
                    <div>
                      <p className="heading">Price</p>
                      <p className="title">{book.price}฿</p>
                    </div>
                  </div>
                </nav>
              </div>
              <div className="column is-9">
                {haveItem ? (
                  <Link
                    to="/cart"
                    className="button mx-3 mt-3 is-large is-info"
                  >
                    ไปที่รถเข็น
                  </Link>
                ) : (
                  <button
                    className="button mx-lg-3 mt-4 is-large is-primary"
                    onClick={() => this.onHandleAddItem(book)}
                  >
                    เพิ่มลงรถเข็น
                  </button>
                )}
              </div> */}
            </div>
          </div>
          <Appfooter />
        </React.Fragment>
      )
    }
  }
}

const mapStateToProps = state => ({
  book: state.book.dataOne,
  isFetching: state.book.isFetching,
  currentUser: state.auth.currentUser,
  cart: state.cart.cart,
  isAuthenticated: state.auth.isAuthenticated
})

export default connect(mapStateToProps)(DetailPage)
