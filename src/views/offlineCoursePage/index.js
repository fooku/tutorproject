import React, { Component } from 'react'
import Appfooter from '../../component/Appfooter'
import { connect } from 'react-redux'
import ReactPlayer from 'react-player'
import Private1 from '../../img/private1.jpg'
import Private2 from '../../img/private2.jpg'
import Private3 from '../../img/private3.jpg'
import Line from '../../img/lineqrcode.jpg'
import { Link } from 'react-router-dom'

import Map from '../../img/map.jpg'

class offlineCoursePage extends Component {
  constructor(props) {
    super(props)

    this.setWrapperRef = this.setWrapperRef.bind(this)
    this.handleClickOutside = this.handleClickOutside.bind(this)
    this.state = {
      activeModel: false
    }
  }

  componentDidMount() {
    document.addEventListener('mousedown', this.handleClickOutside)
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside)
  }

  /**
   * Set the wrapper ref
   */
  setWrapperRef(node) {
    this.wrapperRef = node
  }

  /**
   * Alert if clicked on outside of element
   */
  handleClickOutside(event) {
    if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      this.setState({
        activeModel: false
      })
    }
  }

  render() {
    return (
      <React.Fragment>
        <div className="hero is-light is-medium">
          <div className="hero-body">
            <div className="container has-text-centered	">
              <h1 className="animated flipInX is-size-1">In-Person Lessons</h1>
              <h2 className="animated flipInX ">
                คอร์สเรียนสดที่จะช่วยพัฒนาทั้ง 4 ทักษะในการใช้ภาษาอังกฤษของคุณ
              </h2>
            </div>
          </div>
        </div>
        <div className="container my-5">
          <div className="columns is-multiline is-vcentered mb-4">
            <div className="column is-5 has-text-centered">
              <div
                className="has-text-right my-5 ff3"
                style={{ display: 'inline-block' }}
              >
                <p className="is-size-2">เรียนภาษาอังกฤษแบบส่วนตัว</p>
                <p className="is-size-3">
                  <i className="fas fa-map-marker-alt mr-3 has-text-danger jackInTheBox animated" />
                  <span
                    className="px-2 has-text-white-bis	"
                    style={{ backgroundColor: '#1C2833' }}
                  >
                    นครราชสีมา
                  </span>
                </p>
              </div>
            </div>
            <div className="column is-7">
              <ReactPlayer
                className="react-player"
                url="https://youtu.be/w5NOF9PbeI8"
                width="100%"
                controls
              />
            </div>
          </div>

          <div className="columns is-multiline">
            <div className="column is-5 mt-2">
              <div className="columns is-gapless is-mobile">
                <div className="column has-text-centered">
                  <figure className="image">
                    <img src={Private1} alt="Private1" />
                  </figure>
                  <p className="mt-3 is-size-6">TOEIC / CUTEP / IELTS</p>
                </div>
                <div className="column has-text-centered">
                  {' '}
                  <figure className="image">
                    <img src={Private2} alt="Private2" height="440" />
                  </figure>
                  <p className="mt-3 is-size-6">
                    GAT / 9 <span className="ff3">สามัญ</span>
                  </p>
                </div>
              </div>
            </div>
            <div className="column is-7">
              <p className="is-size-2 mt-1 mb-3">One-to-one Lessons</p>
              <p className="is-size-5 mb-3 ff3">
                คอร์สเรียนตัวต่อตัวที่จะช่วยเตรียมความพร้อมในการสอบให้กับคุณ
                บทเรียนที่ถูกออกแบบและปรับระดับให้เหมาะกับผู้เรียนเป็นรายบุคคล
                จังหวะการสอนที่พอดีกับผู้เรียน
                และแบบฝึกหัดพร้อมการอธิบายเหตุผลของวิธีเลือกคำตอบอย่างละเอียด
                เข้าใจง่าย
                เพิ่อสร้างพิ้นฐานที่ดีและเพิ่มความสามารถในการเข้าใจภาษาอังกฤษได้อย่างมีประสิทธิภาพ
                เข้าห้องสอบได้อย่างมั่นใจ
                และได้พัฒนาทักษะสำคัญพร้อมนำไปใช้ได้จริงหลังจบคอร์ส
              </p>
              <p className="is-size-5 mb-2 ff3">
                สามารถเลือกเวลาเรียนได้ตามต้องการ ตั้งแต่ 8:00 - 19:00 น. ทุกวัน
                จันทร์-อาทิตย์
              </p>
              <p className="is-size-5 ff3 ">
                เรียนครั้งละ 2 ชั่วโมง{' '}
                <Link to="/schedule" className="button ml-3 is-link is-light">
                  ดูตารางเรียนคลิกที่นี่
                </Link>
              </p>
            </div>
            <div className="column is-5">
              <figure className="image mt-1">
                <img src={Private3} alt="Private3" />
              </figure>
            </div>
            <div className="column is-7">
              <p className="is-size-2 mb-3">Conversation Classes</p>
              <p className="is-size-5 mb-3 ff3">
                คอร์สเรียนสำหรับผู้ที่ต้องการเน้นทักษะการสื่อสารโดนเฉพาะการพูดและฟังนี้ถูกออกแบบด้วยหลักสูตรการสอนภาษาอังกฤษสำหรับผู้ใหญ่
                จาก Cambridge University ที่เน้นความสำคัญกับผู้เรียนเป็นหลัก
                โดนจัดกลุ่มผู้เรียนตามความสามารถในการใช้ภาษาอังกฤษให้มีความ
                ใกล้เคียงกันใน 1 กลุ่ม
                (นัดล่วงหน้าเพื่อทดสอบวัดระดับก่อนเข้าร่วมคลาสเรียน ฟรี)
                คลาสเรียนจำกัดที่นั่งเพียง 6 คน/คลาส
                เพื่อให้ผู้สอนสามารถดูแลทุกคนได้อย่างใกล้ชิด
              </p>
              <p className="is-size-5 ff3">
                เรียนสัปดาห์ละ 1 ครั้งๆ ละ 2 ซั่วโมง วันเสาร์หรือวันอาทิตย์
              </p>
            </div>
          </div>

          <div className="columns is-multiline is-vcentered mt">
            <div className="column is-5 has-text-centered">
              <div
                className="has-text-left is-size-4 my-5 ff3"
                style={{ display: 'inline-block' }}
              >
                <p className="mb-2 ff3 has-text-weight-bold is-size-3">
                  ติดต่อผู้สอน
                </p>
                <p className="mb-2">
                  <i
                    className="fab fa-facebook-square mr-2"
                    style={{ color: '#3b5998' }}
                  />
                  <a
                    target="_blank"
                    href="https://www.facebook.com/wellbalancedKORAT"
                    rel="noopener noreferrer"
                    style={{ color: '#4a4a4a' }}
                  >
                    Well-Balanced English
                  </a>
                </p>
                <div
                  className="mb-2"
                  ref={this.setWrapperRef}
                  onClick={() => this.setState({ activeModel: true })}
                >
                  <i
                    className="fab fa-line mr-2"
                    style={{ color: '#28B463' }}
                  />
                  <span style={{ cursor: 'pointer' }}>line QR Code</span>
                </div>
                <p>
                  <i
                    className="fas fa-phone-square-alt mr-2"
                    style={{ color: '#E74C3C' }}
                  />
                  <span>082-953-6564</span>
                </p>
              </div>
            </div>
            <div className="column is-7 has-text-centered is-size-1">
              {' '}
              <figure className="image mt-1">
                <img src={Map} alt="map" />
              </figure>
            </div>
            <div
              className={this.state.activeModel ? 'modal is-active' : 'modal'}
            >
              <div className="modal-background" />
              <div
                className="modal-content"
                style={{ backgroundColor: '#fff', width: '400px' }}
                ref={this.setWrapperRef}
              >
                <p
                  className="image"
                  style={{
                    display: 'block',
                    marginLeft: 'auto',
                    marginRight: 'auto',
                    marginTop: '1rem',
                    marginBottom: '1rem',
                    width: '90%'
                  }}
                >
                  <img
                    style={{ width: '400px' }}
                    src={Line}
                    alt="line qr code"
                  />
                </p>
              </div>
            </div>
          </div>
        </div>

        <Appfooter />
      </React.Fragment>
    )
  }
}

const mapStateToProps = state => ({})

export default connect(mapStateToProps)(offlineCoursePage)
