import React, { Component } from 'react'
import Appfooter from '../../component/Appfooter'
import { connect } from 'react-redux'
import { listOrder } from '../../actions/order'
import { Link } from 'react-router-dom'
import LoadingIn from '../../component/LoadingIn'

class OrderPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      id: '',
      userType: '',
      currentP: 1,
      num: 10,
      search: '',
      filterStatus: ''
    }
  }
  async componentDidMount() {
    const { dispatch, currentUser } = this.props

    if (currentUser.ID !== undefined) {
      dispatch(listOrder(currentUser.ID))
    } else {
      dispatch(listOrder('no'))
    }
  }

  formatDate(date) {
    let d = new Date(date)
    let month = '' + (d.getMonth() + 1)
    let day = '' + d.getDate()
    let year = d.getFullYear()

    if (month.length < 2) month = '0' + month
    if (day.length < 2) day = '0' + day

    return [day, month, year].join('-')
  }

  renderRow(orders) {
    const { num, currentP } = this.state
    const num1 = num * currentP
    const num2 = num * (currentP - 1)

    const result =
      orders &&
      orders.filter(order => {
        return (
          (order.ID.includes(this.state.search) ||
            order.emailuser.includes(this.state.search)) &&
          order.status.includes(this.state.filterStatus)
        )
      })
    return (
      result &&
      result.map((order, index) => {
        if (index < num1 && index >= num2) {
          var color = ''
          var statusColor
          if (order.status === 'รอโอนเงิน') {
            statusColor = <td className="has-text-dark">{order.status}</td>
            color = ' has-text-dark'
          } else if (order.status === 'รอเช็คยอดเงิน') {
            statusColor = <td className="color-5">{order.status}</td>
            color = ' color-5'
          } else if (order.status === 'คำสั่งซื้อถูกอนุมัติแล้ว') {
            statusColor = <td className="has-text-success">{order.status}</td>
            color = ' has-text-success'
          }
          return (
            <tr key={index}>
              <th>
                <i className={'fas fa-circle mr-3' + color} />
                {order.ID}
              </th>
              <td>{this.formatDate(order.timestamp)}</td>
              <td>{order.emailuser}</td>
              <td>{order.total}</td>
              {statusColor}
              <td>
                <Link
                  to={'/order/payment/' + order.ID}
                  className="is-primary"
                  onClick={this.closeBurger}
                >
                  <span>แจ้งชำระเงิน</span>
                </Link>
                {' | '}
                <Link
                  to={'/order/' + order.ID}
                  className="color-6"
                  onClick={this.closeBurger}
                >
                  <span>ดูรายละเอียด</span>
                </Link>
              </td>
            </tr>
          )
        } else {
          return null
        }
      })
    )
  }

  renderP(orders) {
    const result =
      orders &&
      orders.filter(order => {
        return (
          (order.ID.includes(this.state.search) ||
            order.emailuser.includes(this.state.search)) &&
          order.status.includes(this.state.filterStatus)
        )
      })
    let sum = Math.ceil(result.length / this.state.num)
    return (
      orders &&
      [...Array(sum).keys()].map(index => {
        if (index < 10) {
          return (
            <li key={index}>
              {index + 1 === this.state.currentP ? (
                <button
                  className="pagination-link is-current"
                  aria-label="Page 1"
                  aria-current="page"
                  onClick={() => this.changeP(index + 1)}
                >
                  {index + 1}
                </button>
              ) : (
                <button
                  className="pagination-link"
                  aria-label="Page 1"
                  aria-current="page"
                  onClick={() => this.changeP(index + 1)}
                >
                  {index + 1}
                </button>
              )}
            </li>
          )
        } else if (index === 10) {
          return (
            <li key={index}>
              <span className="pagination-ellipsis">&hellip;</span>
            </li>
          )
        }
        return null
      })
    )
  }
  async changeCP(e) {
    const cp = await e.target.value

    this.setState({ currentP: 1, num: cp })
    window.scrollTo({
      top: 310,
      left: 0,
      behavior: 'smooth'
    })
  }
  changeP(c) {
    this.setState({ currentP: c })
  }

  handleChange(event) {
    this.setState({ filterStatus: event.target.value })
  }

  render() {
    const { orders, isFetching } = this.props
    if (isFetching) {
      return (
        <>
          <div className="hero is-warning is-medium">
            <div className="hero-body">
              <div className="container has-text-centered">
                <h1 className="animated flipInX is-size-1	">
                  Your Order Status
                </h1>
                <h2 className="animated flipInX ">รายละเอียดการสั่งซื้อ</h2>
              </div>
            </div>
          </div>
          <div className="container my-5">
            <LoadingIn />
          </div>
        </>
      )
    } else {
      return (
        <React.Fragment>
          <div className="hero is-warning is-medium">
            <div className="hero-body">
              <div className="container has-text-centered">
                <h1 className="animated flipInX is-size-1	">
                  Your Order Status
                </h1>
                <h2 className="animated flipInX ">รายละเอียดการสั่งซื้อ</h2>
              </div>
            </div>
          </div>
          <div className="container my-5">
            {orders && orders.length > 0 ? (
              <>
                <nav className="level">
                  <div className="level-left">
                    <div className="select">
                      <select
                        value={this.state.value}
                        onChange={e => this.handleChange(e)}
                      >
                        <option value="">ทั้งหมด</option>
                        <option value="รอโอนเงิน">รอโอนเงิน</option>
                        <option value="รอเช็คยอดเงิน">รอเช็คยอดเงิน</option>
                        <option value="คำสั่งซื้อถูกอนุมัติแล้ว">
                          คำสั่งซื้อถูกอนุมัติแล้ว
                        </option>
                      </select>
                    </div>
                  </div>

                  <div className="level-right">
                    <div className="field has-addons has-addons-right">
                      <div className="control mb-2">
                        <input
                          className="input"
                          type="text"
                          placeholder="ค้นหา"
                          onChange={e =>
                            this.setState({
                              search: e.target.value,
                              currentP: 1
                            })
                          }
                        />
                      </div>
                    </div>
                  </div>
                </nav>

                <div style={{ overflow: 'auto' }}>
                  <table className="table is-fullwidth is-hoverable is-bordered mb-4">
                    <thead className="">
                      <tr>
                        <th>คำสั่งซื้อ #</th>
                        <th>วันที่ </th>
                        <th>ถึง </th>
                        <th>ยอดรวม </th>
                        <th>สถานะคำสั่งซื้อ </th>
                        <th>~</th>
                      </tr>
                    </thead>
                    <tbody className="">
                      {orders && orders.length > 0 && this.renderRow(orders)}
                    </tbody>
                  </table>
                </div>

                <nav
                  className="pagination is-right"
                  role="navigation"
                  aria-label="pagination"
                >
                  <div className="select">
                    <select
                      defaultValue={this.state.currentP}
                      onChange={e => this.changeCP(e)}
                    >
                      <option value="10">10</option>
                      <option value="25">25</option>
                      <option value="50">50</option>
                      <option value="100">100</option>
                    </select>
                  </div>
                  <ul className="pagination-list">
                    {orders.length > 0 ? this.renderP(orders) : null}
                  </ul>
                </nav>
              </>
            ) : (
              <div className="columns is-vcentered">
                <div className="column is-12 has-text-centered">
                  <p>
                    <i className="fas fa-history fa-5x mb-4" />
                  </p>
                  <p>Your order is empty. Keep shopping to find a course!</p>
                  <Link to="/course" className="button is-primary mt-4">
                    Keep Shopping
                  </Link>
                </div>
              </div>
            )}
          </div>
          <Appfooter />
        </React.Fragment>
      )
    }
  }
}

const mapStateToProps = state => ({
  currentUser: state.auth.currentUser,
  orders: state.order.orders,
  isFetching: state.order.isFetching
})

export default connect(mapStateToProps)(OrderPage)
