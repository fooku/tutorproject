import React, { Component } from 'react'
import Appfooter from '../../../component/Appfooter'
import { connect } from 'react-redux'
import { getOrderOne, payment } from '../../../actions/order'
import { Link } from 'react-router-dom'
import baseurl from '../../../services/baseurl'
import axios from 'axios'
import Swal from 'sweetalert2'

const Toast = Swal.mixin({
  toast: true,
  position: 'center',
  showConfirmButton: false,
  timer: 3000
})

class PaymentPage extends Component {
  constructor(props) {
    super(props)

    this.state = {
      tell: '',
      bank: '',
      date: '',
      time: '',
      total: 0,
      selectedFile: '',
      loaded: 0
    }
  }

  componentDidMount() {
    window.scrollTo(0, 0)
    const { match, dispatch } = this.props
    const { id } = match.params

    dispatch(getOrderOne(id))
  }

  handleselectedFile = event => {
    this.setState({
      selectedFile: event.target.files[0],
      loaded: 0
    })
  }

  formatDate(date) {
    let d = new Date(date)
    let month = '' + (d.getMonth() + 1)
    let day = '' + d.getDate()
    let year = d.getFullYear()

    if (month.length < 2) month = '0' + month
    if (day.length < 2) day = '0' + day

    return [day, month, year].join('-')
  }
  renderRow(list) {
    return (
      list &&
      list.map((cart, index) => {
        return (
          <tr key={index}>
            <td style={{ width: '10%' }}>
              {' '}
              <figure className="image alligator-turtle">
                <img src={cart.img} alt="cart item" />
              </figure>
            </td>
            <td>{cart.name}</td>
            <th>{cart.price}</th>
          </tr>
        )
      })
    )
  }
  onChangeInput(name, even) {
    if (name === 'tell') {
      this.setState({
        tell: even.target.value
      })
    } else if (name === 'bank') {
      this.setState({
        bank: even.target.value
      })
    } else if (name === 'date') {
      this.setState({
        date: even.target.value
      })
    } else if (name === 'time') {
      this.setState({
        time: even.target.value
      })
    } else if (name === 'total') {
      this.setState({
        total: even.target.value
      })
    }
  }

  file = async () => {
    const { dispatch, order } = this.props
    const { selectedFile, bank, tell, total, time, date } = this.state
    const data = {
      telephonenumber: tell,
      bank: bank,
      time: time,
      date: date,
      total: Number(total)
    }
    console.log('data', data)
    if (selectedFile !== '' && bank !== '' && tell !== '' && total !== '') {
      var formData = new FormData()
      formData.append('file', this.state.selectedFile)
      const token = await localStorage.getItem('token')
      axios
        .post(baseurl + '/restricted/payment2?idorder=' + order.ID, formData, {
          onUploadProgress: progressEvent => {
            this.setState({
              loaded: progressEvent.loaded,
              total: progressEvent.total
            })
            console.log(progressEvent.loaded, progressEvent.total)
          },
          headers: {
            'Content-Type': 'multipart/form-data',
            Authorization: token
          }
        })
        .then(res => {
          console.log(res)
          dispatch(payment(order.ID, data))
        })
    } else {
      Toast.fire({
        type: 'error',
        title: 'กรุณากรอกข้อมูลให้ครบ'
      })
    }
  }

  render() {
    const { order } = this.props
    const { tell, bank, date, time, total, selectedFile } = this.state

    return (
      <React.Fragment>
        <div className="hero is-warning is-medium">
          <div className="hero-body">
            <div className="container">
              <h1 className="animated fadeInLeft faster is-size-1	">Payment</h1>
              <h2 className="animated fadeInLeft "> Payment Details</h2>
            </div>
          </div>
        </div>
        <div className="container my-5">
          <nav className="breadcrumb" aria-label="breadcrumbs">
            <ul>
              <li>
                <Link to={'/order'}>
                  <span className="icon is-small">
                    <i className="fas fa-clipboard-list" />
                  </span>
                  <span>Orders</span>
                </Link>
              </li>
              <li className="is-active">
                {' '}
                <Link to={'/ordermanage'} className="is-primary">
                  <span className="icon is-small">
                    <i className="fab fa-btc" />
                  </span>
                  <span>Payment</span>
                </Link>
              </li>
            </ul>
          </nav>
          <div className="columns is-multiline">
            <div className="column is-half">
              <p className="is-size-3 mb-3">ข้อมูลคำสั่งซื้อ</p>
              <p className="is-size-5 mb-1">{'คำสั่งซื้อ : ' + order.ID}</p>
              <p className="is-size-5 mb-1">
                {'วันที่สั่งซื้อ : ' + this.formatDate(order.timestamp)}
              </p>
              <p className="is-size-5 mb-1">{'ผู้ซื้อ : ' + order.emailuser}</p>
              <p className="is-size-5 mb-1">{'สถานะ : ' + order.status}</p>
            </div>
            <div className="column is-two-fifths">
              <p className="is-size-3 mb-3">แจ้งการชำระเงิน</p>
              {order.status === 'รอโอนเงิน' ? (
                <React.Fragment>
                  <div className="field">
                    <p className="control has-icons-left has-icons-right">
                      <input
                        className="input"
                        type="text"
                        placeholder="เบอร์โทรศัพท์"
                        value={tell}
                        onChange={e => this.onChangeInput('tell', e)}
                      />
                      <span className="icon is-small is-left">
                        <i className="fas fa-mobile-alt" />
                      </span>
                    </p>
                  </div>
                  <div className="field">
                    <div className="control is-expanded has-icons-left">
                      <div className="select is-fullwidth">
                        <select
                          name="country"
                          value={bank}
                          onChange={e => this.onChangeInput('bank', e)}
                        >
                          <option value="">กรุณาเลือกธนาคาร</option>
                          <option value="บัญชีธนาคารกสิกรไทยเลขที่ 037-2-91740-7">
                            บัญชีธนาคารกสิกรไทยเลขที่ 037-2-91740-7
                          </option>
                          <option value="พร้อมเพย 082-953-6564">
                            พร้อมเพย 082-953-6564
                          </option>
                        </select>
                        <span className="icon is-small is-left">
                          <i className="fas fa-university" />
                        </span>
                      </div>
                    </div>
                  </div>
                  <div className="field">
                    <p className="control has-icons-left has-icons-right">
                      <input
                        className="input"
                        type="date"
                        placeholder="วันที่โอน"
                        value={date}
                        onChange={e => this.onChangeInput('date', e)}
                      />
                      <span className="icon is-small is-left">
                        <i className="fas fa-calendar-check" />
                      </span>
                    </p>
                  </div>
                  <div className="field">
                    <p className="control has-icons-left has-icons-right">
                      <input
                        className="input"
                        type="time"
                        placeholder="เวลาที่โอน"
                        value={time}
                        onChange={e => this.onChangeInput('time', e)}
                      />
                      <span className="icon is-small is-left">
                        <i className="fas fa-clock" />
                      </span>
                    </p>
                  </div>
                  <div className="field">
                    <p className="control has-icons-left has-icons-right">
                      <input
                        className="input"
                        type="number"
                        placeholder="จำนวนเงินที่โอน"
                        min="0"
                        value={total}
                        onChange={e => this.onChangeInput('total', e)}
                      />
                      <span className="icon is-small is-left">
                        <i className="fas fa-hand-holding-usd" />
                      </span>
                    </p>
                  </div>
                  <div className="file has-name is-fullwidth">
                    <label className="file-label">
                      <input
                        className="file-input"
                        type="file"
                        accept="image/*"
                        name="file"
                        onChange={this.handleselectedFile}
                      />
                      <span className="file-cta">
                        <span className="file-icon">
                          <i className="fas fa-upload" />
                        </span>
                        <span className="file-label">Choose a file…</span>
                      </span>
                      <span className="file-name">
                        {selectedFile && selectedFile.name !== undefined
                          ? selectedFile.name
                          : ''}
                      </span>
                    </label>
                  </div>
                  <button
                    className="button is-link is-outlined is-fullwidth my-3"
                    onClick={this.file}
                  >
                    <span className="icon mr-1">
                      <i className="fas fa-paper-plane" />
                    </span>
                    ยืนยัน
                  </button>
                </React.Fragment>
              ) : (
                <article className="message is-success">
                  <div className="message-header">
                    <p>แจ้งการชำระเงินเรียบร้อยแล้ว</p>
                  </div>
                  <div className="message-body">
                    เมือมีปัญหาเกียวกับการซื้อคอร์ส ติดต่อ
                    <div className="content">
                      <ul type="i">
                        <li>โทร 082-953-6564</li>
                      </ul>
                      {/* <ul type="i">
                        <li>@ line</li>
                      </ul> */}
                      <ul type="i">
                        <li>
                          <a href="https://www.facebook.com/wellbalancedKORAT">
                            Facebook
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                </article>
              )}
            </div>
          </div>
        </div>
        <Appfooter />
      </React.Fragment>
    )
  }
}

const mapStateToProps = state => ({
  order: state.order.order
})

export default connect(mapStateToProps)(PaymentPage)
