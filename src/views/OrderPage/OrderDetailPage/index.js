import React, { Component } from 'react'
import Appfooter from '../../../component/Appfooter'
import { connect } from 'react-redux'
import { getOrderOne } from '../../../actions/order'
import { Link } from 'react-router-dom'

class OrderDetailPage extends Component {
  componentDidMount() {
    window.scrollTo(0, 0)
    const { match, dispatch } = this.props
    const { id } = match.params

    dispatch(getOrderOne(id))
  }

  formatDate(date) {
    let d = new Date(date)
    let month = '' + (d.getMonth() + 1)
    let day = '' + d.getDate()
    let year = d.getFullYear()

    if (month.length < 2) month = '0' + month
    if (day.length < 2) day = '0' + day

    return [day, month, year].join('-')
  }
  renderRow(list) {
    return (
      list &&
      list.map((cart, index) => {
        return (
          <tr key={index}>
            <td style={{ width: '10%' }}>
              {' '}
              <figure className="image alligator-turtle">
                <img src={cart.img} alt="cart item" />
              </figure>
            </td>
            <td>{cart.name}</td>
            <th>{cart.price}</th>
          </tr>
        )
      })
    )
  }

  render() {
    const { order } = this.props
    console.log('order >>', order)
    console.log(order !== undefined)
    return (
      <React.Fragment>
        <div className="hero is-warning is-medium">
          <div className="hero-body">
            <div className="container">
              <h1 className="animated fadeInLeft faster is-size-1	">
                Manage Order Detail
              </h1>
              <h2 className="animated fadeInLeft "> Manage Order Detail</h2>
            </div>
          </div>
        </div>
        <div className="container my-5">
          <nav className="breadcrumb" aria-label="breadcrumbs">
            <ul>
              <li>
                <Link to={'/order'}>
                  <span className="icon is-small">
                    <i className="fas fa-clipboard-list" />
                  </span>
                  <span>Orders</span>
                </Link>
              </li>
              <li className="is-active">
                {' '}
                <Link to={'/ordermanage'} className="is-primary">
                  <span className="icon is-small">
                    <i className="fas fa-map-marker-alt" />
                  </span>
                  <span>Order Detail</span>
                </Link>
              </li>
            </ul>
          </nav>
          <div className="columns is-multiline">
            <div className="column is-one-third">
              <p className="is-size-3 mb-3">ข้อมูลคำสั่งซื้อ</p>
              <p className="is-size-5 mb-1">{'คำสั่งซื้อ : ' + order.ID}</p>
              <p className="is-size-5 mb-1">
                {'วันที่สั่งซื้อ : ' + this.formatDate(order.timestamp)}
              </p>
              <p className="is-size-5 mb-1">{'ผู้ซื้อ : ' + order.emailuser}</p>
              <p className="is-size-5 mb-1">{'สถานะ : ' + order.status}</p>

              {order.address !== undefined ? (
                <React.Fragment>
                  <hr />
                  <p className="is-size-4 mt-4">ที่อยู่จัดส่ง :</p>
                  <p className="has-text-danger mb-1">
                    ** สำหรับคอร์สที่มีการจัดส่งหนังสือ
                  </p>
                  <p className="is-size-5 mb-1">
                    {'ชื่อ นามสกุล : ' + order.address.name}
                  </p>
                  <p className="is-size-5 mb-1">
                    {'เบอร์โทรศัพท์ : ' + order.address.tel}
                  </p>
                  <p className="is-size-5 mb-1">
                    {'ที่อยู่ : ' + order.address.address}
                  </p>
                  <p className="is-size-5 mb-1">
                    {'เขต/อำเภอ : ' + order.address.district}
                  </p>
                  <p className="is-size-5 mb-1">
                    {'จังหวัด : ' + order.address.province}
                  </p>
                  <p className="is-size-5 mb-1">
                    {'รหัสไปรษณีย์ : ' + order.address.postcode}
                  </p>
                  <article className="message is-warning mt-3">
                    <div className="message-body">
                      เกิดปัญหาสำหรับที่อยู่จัดส่ง ติดต่อ
                      <div className="content">
                        <ul type="i">
                          <li>โทร 082-953-6564</li>
                        </ul>
                        <ul type="i">
                          <li>
                            <a href="https://www.facebook.com/wellbalancedKORAT">
                              Facebook
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </article>
                </React.Fragment>
              ) : null}
            </div>
            <div className="column">
              <p className="is-size-3 mb-2">รายการสินค้าที่ซื้อ</p>
              <table className="table is-fullwidth is-hoverable is-bordered">
                <thead>
                  <tr>
                    <th />
                    <th>ชื่อสินค้า </th>
                    <th>ราคา</th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th colSpan="2">ราคารวม</th>
                    <th>{order.total}</th>
                  </tr>
                </tfoot>
                <tbody>{this.renderRow(order.cart)}</tbody>
              </table>
            </div>
          </div>
        </div>
        <Appfooter />
      </React.Fragment>
    )
  }
}

const mapStateToProps = state => ({
  order: state.order.order
})

export default connect(mapStateToProps)(OrderDetailPage)
