import React, { Component } from 'react'
import Appfooter from '../../component/Appfooter'
import howToStep1 from '../../img/howToStep1.jpg'
import howToStep2 from '../../img/howToStep2.jpg'
import howToStep3x1 from '../../img/QR_Promptpay.jpg'
import howToStep3x2 from '../../img/step3-2.jpg'
import howToStep3x3 from '../../img/step3-3.jpg'
import howToStep3x5 from '../../img/step3-5.jpg'

class HowtoPage extends Component {
  componentDidMount() {
    window.scrollTo(0, 0)
  }

  onDocumentLoadSuccess({ numPages }) {
    // setNumPages(numPages);
  }

  render() {
    return (
      <React.Fragment>
        <div className="hero is-light is-medium">
          <div className="hero-body">
            <div className="container has-text-centered	">
              <h1 className="animated flipInX is-size-1"> How To Order</h1>
              <h2 className="animated flipInX ">Follow these 3 steps</h2>
            </div>
          </div>
        </div>
        <div className="container my-5">
          <div>
            <p className="is-size-3">1. สร้างบัญชีผู้ใช้งาน</p>
            <img className="my-4" src={howToStep1} alt="howToStep1" />
          </div>
          <div>
            <p className="is-size-3">2. เลือกคอร์สเรียนหรือeBookที่ต้องการ</p>
            <img className="my-4" src={howToStep2} alt="howToStep2" />
          </div>
          <div>
            <p className="is-size-3">3. ชำระเงิน</p>

            <div className="ml-5">
              <p className="is-size-4 mb-4">ขั้นตอนการชำระเงิน</p>
              <p className="is-size-5">1. โอนเงินผ่านบัญชีธนาคาร</p>
              <div className="columns my-3">
                <div className="column is-one-third">
                  <img src={howToStep3x1} alt="promptpay" />
                </div>
                <div className="column">
                  <div className="columns is-multiline">
                    <div className="column is-full">
                      <article className="media">
                        <figure className="media-left">
                          <p className="image is-64x64">
                            <img
                              src="https://i.pinimg.com/originals/b4/ed/2f/b4ed2f55bd95c9689eec4d7aa6c14788.jpg"
                              alt="scb logo"
                            />
                          </p>
                        </figure>
                        <div className="media-content">
                          <div className="content">
                            <p>
                              บัญชีธนาคารกสิกรไทยเลขที่ 037-2-91740-7 <br />
                              ชื่อบัญชี อภิชญา กัฬหะสุต
                            </p>
                          </div>
                        </div>
                      </article>
                    </div>
                  </div>
                </div>
              </div>
              <p className="is-size-5">
                2. แจ้งการโอนเงินโดยเข้าไปที่{' '}
                <span style={{ textDecoration: 'underline' }}>Order</span>{' '}
                จากนั้นคลิกที่{' '}
                <span style={{ textDecoration: 'underline' }}>
                  แจ้งชำระเงิน
                </span>
              </p>
              <img className="mt-3" src={howToStep3x2} alt="Step3-2" />
              <p className="is-size-5">3. ยืนยันการชำระเงิน</p>
              <img src={howToStep3x3} alt="Step3-2" />
              <p className="is-size-5">
                4. รอผู้ดูแลตรวจสอบการชำระเงิน
                <span className="ml-3" style={{ color: '#CD6155' }}>
                  (&nbsp;ในกรณีที่มีการจัดส่งเอกสาร แจ้งที่อยู่ได้ทาง{' '}
                  <a href="https://www.facebook.com/wellbalancedKORAT">
                    Facebook
                  </a>{' '}
                  &nbsp;)
                </span>
              </p>
              <p className="is-size-5 mt-4">
                5. ไปที่ my corner เพื่อเริ่มบทเรียน หรือ ดาวน์โหลด eBook
              </p>
              <img src={howToStep3x5} alt="Step3-5" />
            </div>
          </div>
        </div>
        <Appfooter />
      </React.Fragment>
    )
  }
}

export default HowtoPage
