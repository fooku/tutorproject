import React, { Component } from 'react'
import Appfooter from '../../../component/Appfooter'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import CardSection from '../../../component/Card/cardSectionPublish'
import renderHTML from 'react-render-html'
import { addItem } from '../../../actions/cart'
import { getBundlePublish } from '../../../actions/bundle'
import LoadingIn from '../../../component/LoadingIn'

class BundleDetailPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      id: null
    }
  }
  componentDidMount() {
    const { match, dispatch } = this.props
    const { bundle } = match.params

    this.setState({ id: bundle })
    dispatch(getBundlePublish(bundle))
  }

  renderCard(list) {
    return (
      list &&
      list.map((course, index) => {
        return (
          <CardSection
            key={index}
            name={course.name}
            idsec={course.id}
            lectures={course.lectures}
            idcourse={this.state.id}
          />
        )
      })
    )
  }

  onHandleAddItem(bundle) {
    const { dispatch, currentUser } = this.props
    let coursesid = []
    bundle.idcourse.forEach(element => {
      coursesid.push(element.id)
    })
    const data = {
      courseid: bundle.id,
      coursesid,
      name: bundle.name,
      price: parseInt(bundle.price, 10),
      img: bundle.thumbnail,
      bundle: true
    }
    dispatch(addItem(currentUser.ID, data))
  }

  renderBox(list) {
    const { num } = this.state
    return (
      list &&
      list.map((cart, index) => {
        return (
          <div className="box my-2" key={index}>
            <article className="media">
              <div className="media-left">
                <figure className="image alligator-turtle">
                  <img src={cart.thumbnail} alt="cart item" />
                </figure>
              </div>
              <div className="media-content">
                <div className="content">
                  <div className="columns">
                    <div className="column is-half is-size-4">
                      {' '}
                      <p>
                        {' '}
                        <strong> {cart.name}</strong>
                      </p>
                    </div>
                    {/* <div className="column has-text-right	has-text-weight-bold is-size-4">
                      <span>{'฿' + cart.price}</span>
                    </div> */}
                    <div className="column has-text-right	has-text-weight-bold">
                      <span
                        style={{ cursor: 'pointer' }}
                        onClick={() =>
                          num === index
                            ? this.setState({ num: 100 })
                            : this.setState({ num: index })
                        }
                      >
                        อ่านรายละเอียดเพิ่มเติม
                      </span>
                    </div>
                  </div>
                  {num === index ? <p>{renderHTML(cart.detail)}</p> : null}
                </div>
              </div>
            </article>
          </div>
        )
      })
    )
  }

  render() {
    const { isFetching, bundle, cart } = this.props
    let haveItem = false
    haveItem =
      cart.filter(item => {
        return item.courseid === bundle.id
      }).length > 0

    if (isFetching) {
      return (
        <>
          <div className="hero is-light is-medium">
            <div className="hero-body">
              <div className="container has-text-centered">
                <h1 className="animated flipInX is-size-1">Bundle Details</h1>
              </div>
            </div>
          </div>
          <div className="columns is-vcentered my-5">
            <LoadingIn />
          </div>
        </>
      )
    } else if (
      bundle.message !== undefined &&
      bundle.message === 'Bundle hasn’t Published'
    ) {
      return (
        <>
          <div className="hero is-light is-medium">
            <div className="hero-body">
              <div className="container has-text-centered">
                <h1 className="animated flipInX is-size-1">Bundle Details</h1>
              </div>
            </div>
          </div>
          <div className="columns is-vcentered my-5">
            <div className="column is-12 has-text-centered">
              <p>
                <i className="fas fa-history fa-5x mb-4" />
              </p>
              <p>Bundle is empty. Keep shopping to find a course!</p>
              <Link to="/course" className="button is-primary mt-4">
                Keep Shopping
              </Link>
            </div>
          </div>
          <Appfooter />
        </>
      )
    } else {
      return (
        <>
          <div className="hero is-light is-medium">
            <div className="hero-body">
              <div className="container has-text-centered">
                <h1 className="animated flipInX is-size-1">Bundle Details</h1>
              </div>
            </div>
          </div>
          <div className="container">
            <nav className="level">
              <div className="level-left">
                <Link to="/course" className="button is-text my-4">
                  <i className="fas fa-chevron-left" />
                  Back
                </Link>
              </div>
            </nav>

            <div className="columns is-multiline mb-4">
              <div className="column is-half has-text-centered">
                <figure className="image">
                  <img src={bundle.thumbnail} alt="img bundle" />
                </figure>
              </div>
              <div className="column is-half py-0">
                <div className="is-size-1 has-text-centered mb-2">
                  {bundle.name}
                </div>

                <div>
                  {bundle.detail !== undefined
                    ? renderHTML(bundle.detail)
                    : null}
                </div>
              </div>
              <div className="column is-half">
                <p className="is-size-3 has-text-centered my-4">
                  ประกอบด้วยคอร์ส
                </p>
                {bundle.idcourse &&
                  bundle.idcourse.length > 0 &&
                  this.renderBox(bundle.idcourse)}
              </div>
              <div className="column is-half">
                {haveItem ? (
                  <nav className="level">
                    <div className="level-item has-text-centered">
                      <div>
                        <p className="heading">Days</p>
                        <p className="title">{bundle.hour}</p>
                      </div>
                    </div>
                    <div className="level-item has-text-centered">
                      <div>
                        <p className="heading">Price</p>
                        <p className="title">{bundle.price}฿</p>
                      </div>
                    </div>
                    <div className="level-item has-text-centered">
                      <Link
                        to="/cart"
                        className="button mx-3 mt-3 is-large is-info is-fullwidth"
                      >
                        ไปที่รถเข็น
                      </Link>
                    </div>
                  </nav>
                ) : (
                  <nav className="level">
                    <div className="level-item has-text-centered">
                      <div>
                        <p className="heading">Days</p>
                        <p className="title">{bundle.hour}</p>
                      </div>
                    </div>
                    <div className="level-item has-text-centered">
                      <div>
                        <p className="heading">Price</p>
                        <p className="title">{bundle.price}฿</p>
                      </div>
                    </div>
                    <div className="level-item has-text-centered">
                      <button
                        className="button mx-lg-3 mt-4 is-large is-primary is-fullwidth"
                        onClick={() => this.onHandleAddItem(bundle)}
                      >
                        เพิ่มลงรถเข็น
                      </button>
                    </div>
                  </nav>
                )}
              </div>
            </div>
          </div>

          <Appfooter />
        </>
      )
    }
  }
}

const mapStateToProps = state => ({
  bundle: state.bundle.dataOne,
  isFetching: state.bundle.isFetching,
  currentUser: state.auth.currentUser,
  cart: state.cart.cart
})

export default connect(mapStateToProps)(BundleDetailPage)
