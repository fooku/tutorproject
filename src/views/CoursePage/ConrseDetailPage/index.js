import React, { Component } from 'react'
import Appfooter from '../../../component/Appfooter'
import { connect } from 'react-redux'
import { getCourseOnePublish } from '../../../actions/course'
import { Link } from 'react-router-dom'
import CardSection from '../../../component/Card/cardSectionPublish'
import renderHTML from 'react-render-html'
import { addItem } from '../../../actions/cart'
import LoadingIn from '../../../component/LoadingIn'
import Swal from 'sweetalert2'
import { push } from 'connected-react-router'

class CourseDetailPage extends Component {
  constructor(props) {
    super(props)
    this.state = {
      id: null
    }
  }
  componentDidMount() {
    const { match, dispatch } = this.props
    const { course } = match.params

    this.setState({ id: course })
    dispatch(getCourseOnePublish(course))
  }

  renderCard(list) {
    return (
      list &&
      list.map((course, index) => {
        return (
          <CardSection
            key={index}
            name={course.name}
            idsec={course.id}
            lectures={course.lectures}
            idcourse={this.state.id}
          />
        )
      })
    )
  }

  onHandleAddItem(course) {
    const { dispatch, currentUser, isAuthenticated } = this.props
    if (isAuthenticated) {
      const data = {
        courseid: course.id,
        name: course.name,
        price: parseInt(course.price, 10),
        img: course.thumbnail
      }
      dispatch(addItem(currentUser.ID, data))
    } else {
      Swal.fire({
        title: 'Please login.',
        text: 'กรุณา login เพื่อเข้าสู่ระบบ',
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'login'
      }).then(result => {
        if (result.value) {
          dispatch(push('/login'))
        }
      })
    }
  }

  render() {
    const { isFetching, course, cart } = this.props
    let haveItem = false
    haveItem =
      cart.filter(item => {
        return item.courseid === course.id
      }).length > 0

    if (isFetching) {
      return (
        <>
          <div className="hero is-light is-medium">
            <div className="hero-body">
              <div className="container has-text-centered">
                <h1 className="animated flipInX is-size-1">Course Details</h1>
              </div>
            </div>
          </div>
          <div className="container my-5">
            <LoadingIn />
          </div>
        </>
      )
    } else {
      return (
        <React.Fragment>
          <div className="hero is-light is-medium">
            <div className="hero-body">
              <div className="container has-text-centered">
                <h1 className="animated flipInX is-size-1">Course Details</h1>
              </div>
            </div>
          </div>
          <div className="container">
            <nav className="level">
              <div className="level-left">
                <Link to="/course" className="button is-text my-4">
                  <i className="fas fa-chevron-left" />
                  Back
                </Link>
              </div>
            </nav>

            <div className="columns is-multiline">
              <div className="column is-half has-text-centered">
                <figure className="image">
                  <img src={course.thumbnail} alt="img course" />
                </figure>
              </div>
              <div className="column is-half py-0">
                <div className="is-size-1 has-text-centered mb-2">
                  {course.name}
                </div>

                <div>
                  {course.detail !== undefined
                    ? renderHTML(course.detail)
                    : null}
                </div>
              </div>
              <div className="column is-half">
                <br />
                <nav className="level">
                  <div className="level-item has-text-centered">
                    <div>
                      <p className="heading">Section</p>
                      <p className="title">{course.sec}</p>
                    </div>
                  </div>
                  <div className="level-item has-text-centered">
                    <div>
                      <p className="heading">Lectures</p>
                      <p className="title">{course.lec}</p>
                    </div>
                  </div>
                  <div className="level-item has-text-centered">
                    <div>
                      <p className="heading">Days</p>
                      <p className="title">{course.hour}</p>
                    </div>
                  </div>
                  <div className="level-item has-text-centered">
                    <div>
                      <p className="heading">Price</p>
                      <p className="title">{course.price}฿</p>
                    </div>
                  </div>
                </nav>
              </div>
              <div className="column is-half">
                {haveItem ? (
                  <Link
                    to="/cart"
                    className="button mx-3 mt-3 is-large is-info is-fullwidth"
                  >
                    ไปที่รถเข็น
                  </Link>
                ) : (
                  <button
                    className="button mx-lg-3 mt-4 is-large is-primary is-fullwidth"
                    onClick={() => this.onHandleAddItem(course)}
                  >
                    เพิ่มลงรถเข็น
                  </button>
                )}
              </div>
            </div>
          </div>
          <div className="container my-5">
            {course.section && this.renderCard(course.section)}
          </div>
          <Appfooter />
        </React.Fragment>
      )
    }
  }
}

const mapStateToProps = state => ({
  course: state.course.course,
  isFetching: state.course.isFetching,
  currentUser: state.auth.currentUser,
  cart: state.cart.cart,
  isAuthenticated: state.auth.isAuthenticated
})

export default connect(mapStateToProps)(CourseDetailPage)
