import React, { Component } from 'react'
import Card from '../../component/Card'
import Appfooter from '../../component/Appfooter'
import { listCourse } from '../../actions/course'
import { connect } from 'react-redux'
import CardBundlePubish from '../../component/Card/cardBundlePubish'
import { listBundlePubish } from '../../actions/bundle'
import LoadingIn from '../../component/LoadingIn'
import { isMobile } from 'react-device-detect'

class CoursePage extends Component {
  componentDidMount() {
    const { dispatch } = this.props
    window.scrollTo(0, 0)

    dispatch(listCourse())
    dispatch(listBundlePubish())
  }

  renderCard(list) {
    return (
      list &&
      list.map((course, index) => {
        console.log(course, index)
        return (
          <Card
            key={index}
            id={course.id}
            name={course.name}
            detail={course.detail}
            img={course.thumbnail}
            price={course.price}
            time={course.hour}
          />
        )
      })
    )
  }

  renderCardBundle(list) {
    return (
      list &&
      list.map((bundle, index) => {
        console.log(bundle, index)
        return (
          <CardBundlePubish
            key={index}
            id={bundle.id}
            name={bundle.name}
            detail={bundle.detail}
            img={bundle.thumbnail}
            price={bundle.price}
            time={bundle.hour}
            priceold={bundle.priceold}
          />
        )
      })
    )
  }

  render() {
    const { isFetching, list, bundle } = this.props

    if (isFetching) {
      return (
        <>
          {isMobile ? (
            <div className="hero is-light is-medium">
              <div className="hero-body">
                <div className="container has-text-centered">
                  <h2 className="animated flipInX is-size-1">
                    Select Your Course
                  </h2>
                  {/* <h3 className="animated flipInX ">
                    คอร์สเรียนที่จะช่วยให้คุณใช้ภาษาอังกฤษได้มั่นใจมากขึ้น
                  </h3> */}
                </div>
              </div>
            </div>
          ) : (
            <div className="hero is-light is-medium">
              <div className="hero-body">
                <div className="container has-text-centered">
                  <h1 className="animated flipInX  is-size-1	">
                    Select Your Course
                  </h1>
                  <h2 className="animated flipInX ">
                    คอร์สเรียนที่จะช่วยให้คุณใช้ภาษาอังกฤษได้มั่นใจมากขึ้น
                  </h2>
                </div>
              </div>
            </div>
          )}
          <div className="container my-5">
            <LoadingIn />
          </div>
        </>
      )
    } else {
      return (
        <React.Fragment>
          <div className="hero is-light is-medium">
            <div className="hero-body">
              <div className="container has-text-centered">
                <h1 className="animated flipInX is-size-1">
                  Select Your Course
                </h1>
                <h2 className="animated flipInX ">
                  คอร์สเรียนที่จะช่วยให้คุณใช้ภาษาอังกฤษได้มั่นใจมากขึ้น
                </h2>
              </div>
            </div>
          </div>

          <div className="container">
            <p className="is-size-2	my-4">Online Courses</p>
            {list ? (
              <div className="columns is-multiline">
                {list.length > 0 && this.renderCard(list)}
              </div>
            ) : (
              <div className="columns is-vcentered">
                <div className="column is-12 has-text-centered">
                  <p>
                    <i className="fas fa-history fa-5x mb-4" />
                  </p>
                  <p className="is-size-3">Coming Soon</p>
                </div>
              </div>
            )}
            {bundle ? (
              <>
                <hr />
                {bundle.length > 0 && (
                  <p className="is-size-2	my-4">Course Bundles</p>
                )}
                <div className="columns is-multiline mb-4">
                  {bundle.length > 0 && this.renderCardBundle(bundle)}
                </div>{' '}
              </>
            ) : (
              <br />
            )}
          </div>
          <Appfooter />
        </React.Fragment>
      )
    }
  }
}

const mapStateToProps = state => ({
  list: state.course.courses,
  bundle: state.bundle.data,
  isFetching: state.course.isFetching
})

export default connect(mapStateToProps)(CoursePage)
