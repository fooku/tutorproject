import React, { Component } from 'react'
import { connect } from 'react-redux'
import styled from 'styled-components'
import { updateUser } from '../../actions/auth'
import AuthImg from '../../img/auth.jpg'

class ProfilePage extends Component {
  constructor(props) {
    super(props)
    const Img = styled.abbr`
      background: url(${AuthImg}) center;
      background-size: cover;
      height: 100vh;
      display: flex;
    `

    this.state = {
      editProfile: false,
      email: '',
      name: '',
      telephonenumber: '',
      Img,
      validateEmail: false
    }
  }

  componentDidMount() {
    window.scrollTo(0, 0)
  }

  componentWillReceiveProps() {
    this.setState({ editProfile: false })
  }
  edit() {
    const { list } = this.props
    this.setState({
      editProfile: !this.state.editProfile,
      email: list.email,
      name: list.name,
      telephonenumber: list.telephonenumber,
      validateEmail: false
    })
  }

  updateUser(id) {
    const { dispatch } = this.props
    const { email, name, telephonenumber } = this.state

    if (/^[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[A-Za-z]+$/.test(email)) {
      const user = {
        email,
        name,
        telephonenumber
      }
      dispatch(updateUser(id, user))
    } else {
      this.setState({
        validateEmail: true
      })
    }
  }

  onChangeInput(name, even) {
    if (name === 'email') {
      this.setState({
        email: even.target.value
      })
    } else if (name === 'name') {
      this.setState({
        name: even.target.value
      })
    } else if (name === 'tell') {
      this.setState({
        telephonenumber: even.target.value
      })
    }
  }

  render() {
    const { list } = this.props
    const { email, name, telephonenumber, Img, validateEmail } = this.state
    const screenSize = window.screen.width < 1275
    const dis =
      list.email === email &&
      list.name === name &&
      list.telephonenumber === telephonenumber
    return screenSize ? (
      <>
        <div className="hero is-light is-medium mb-4">
          <div className="hero-body">
            <div className="container has-text-centered	">
              <h1 className="animated flipInX is-size-1">Profile</h1>
              <h2 className="animated flipInX ">ข้อมูลส่วนตัว</h2>
            </div>
          </div>
        </div>
        <div className="container">
          <div className="columns">
            <div className="column has-text-grey-dark">
              <div className="margin-pro">
                {this.state.editProfile ? (
                  <React.Fragment>
                    <p className="is-size-1 mb-4">Edit Profile</p>

                    <div className="field">
                      <p className="control has-icons-left has-icons-right">
                        <input
                          className={
                            validateEmail ? 'input is-danger' : 'input'
                          }
                          type="email"
                          placeholder="Email"
                          value={email}
                          onChange={e => this.onChangeInput('email', e)}
                        />
                        <span className="icon is-small is-left">
                          <i className="fas fa-envelope" />
                        </span>
                      </p>
                    </div>
                    <div className="field">
                      <p className="control has-icons-left has-icons-right">
                        <input
                          className="input"
                          type="text"
                          placeholder="Username"
                          value={name}
                          onChange={e => this.onChangeInput('name', e)}
                        />
                        <span className="icon is-small is-left">
                          <i className="fas fa-user" />
                        </span>
                      </p>
                    </div>

                    <div className="field">
                      <p className="control has-icons-left has-icons-right">
                        <input
                          className="input"
                          type="text"
                          placeholder="Telephone Number"
                          value={telephonenumber}
                          onChange={e => this.onChangeInput('tell', e)}
                        />
                        <span className="icon is-small is-left">
                          <i className="fas fa-mobile-alt" />
                        </span>
                      </p>
                    </div>

                    <div className="buttons has-addons is-centered mt-4">
                      <button
                        className="button is-medium is-warning"
                        onClick={() => this.edit()}
                      >
                        ยกเลิก
                      </button>
                      <button
                        className={'button is-medium is-success'}
                        disabled={dis}
                        onClick={() => this.updateUser(list.ID)}
                      >
                        ตกลง
                      </button>
                    </div>
                  </React.Fragment>
                ) : (
                  <React.Fragment>
                    <button
                      className="button is-warning is-medium my-4"
                      onClick={() => this.edit()}
                    >
                      EditProfile
                    </button>

                    <p className=" is-size-4">Email : {list.email}</p>
                    <p className=" is-size-4">Name : {list.name}</p>
                    <p className=" is-size-4">
                      Telephone : {list.telephonenumber}
                    </p>
                    <p className=" is-size-4 mb-3">
                      UserType : {list.usertype}
                    </p>
                  </React.Fragment>
                )}
              </div>
            </div>
          </div>
        </div>
      </>
    ) : (
      <React.Fragment>
        <div className="hero is-primary is-fullheight">
          <Img>
            <div className="container">
              <div className="columns mt padding_10 animated pulse">
                <div className="column profile has-text-grey-dark">
                  <div className="margin-pro">
                    {this.state.editProfile ? (
                      <React.Fragment>
                        <p className="is-size-1 mb-4">Edit Profile</p>

                        <div className="field">
                          <p className="control has-icons-left has-icons-right">
                            <input
                              className={
                                validateEmail ? 'input is-danger' : 'input'
                              }
                              type="email"
                              placeholder="Email"
                              value={email}
                              onChange={e => this.onChangeInput('email', e)}
                            />
                            <span className="icon is-small is-left">
                              <i className="fas fa-envelope" />
                            </span>
                          </p>
                        </div>
                        <div className="field">
                          <p className="control has-icons-left has-icons-right">
                            <input
                              className="input"
                              type="text"
                              placeholder="Username"
                              value={name}
                              onChange={e => this.onChangeInput('name', e)}
                            />
                            <span className="icon is-small is-left">
                              <i className="fas fa-user" />
                            </span>
                          </p>
                        </div>

                        <div className="field">
                          <p className="control has-icons-left has-icons-right">
                            <input
                              className="input"
                              type="text"
                              placeholder="Telephone Number"
                              value={telephonenumber}
                              onChange={e => this.onChangeInput('tell', e)}
                            />
                            <span className="icon is-small is-left">
                              <i className="fas fa-mobile-alt" />
                            </span>
                          </p>
                        </div>

                        <div className="buttons has-addons is-centered mt-4">
                          <button
                            className="button is-medium is-warning"
                            onClick={() => this.edit()}
                          >
                            ยกเลิก
                          </button>
                          <button
                            className={'button is-medium is-success'}
                            disabled={dis}
                            onClick={() => this.updateUser(list.ID)}
                          >
                            ตกลง
                          </button>
                        </div>
                      </React.Fragment>
                    ) : (
                      <React.Fragment>
                        <nav className="level">
                          <div className="level-left">
                            <p className="is-size-1 mb-2">Profile </p>
                          </div>

                          <div className="level-right">
                            <button
                              className="button is-warning is-medium"
                              onClick={() => this.edit()}
                            >
                              EditProfile
                            </button>
                          </div>
                        </nav>

                        <p className=" is-size-4">Email : {list.email}</p>
                        <p className=" is-size-4">Name : {list.name}</p>
                        <p className=" is-size-4">
                          Telephone : {list.telephonenumber}
                        </p>
                        <p className=" is-size-4 mb-3">
                          Status : {list.usertype}
                        </p>
                      </React.Fragment>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </Img>
        </div>
      </React.Fragment>
    )
  }
}

const mapStateToProps = state => ({
  list: state.auth.currentUser,
  isFetching: state.users.isFetching
})

export default connect(mapStateToProps)(ProfilePage)
