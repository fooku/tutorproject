import React, { Component } from 'react'
import Appfooter from '../../component/Appfooter'
import { listSchedule } from '../../actions/schedule'
import { connect } from 'react-redux'
import Table from '../Manage/ScheduleMagenePage/Table'
import { Link } from 'react-router-dom'
import Line from '../../img/lineqrcode.jpg'

class TablePage extends Component {
  componentDidMount() {
    window.scrollTo(0, 0)

    document.addEventListener('mousedown', this.handleClickOutside)
    const { dispatch } = this.props

    dispatch(listSchedule())
  }

  constructor(props) {
    super(props)

    this.setWrapperRef = this.setWrapperRef.bind(this)
    this.handleClickOutside = this.handleClickOutside.bind(this)
    this.state = {
      activeModel: false
    }
  }

  componentWillUnmount() {
    document.removeEventListener('mousedown', this.handleClickOutside)
  }

  /**
   * Set the wrapper ref
   */
  setWrapperRef(node) {
    this.wrapperRef = node
  }

  /**
   * Alert if clicked on outside of element
   */
  handleClickOutside(event) {
    if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      this.setState({
        activeModel: false
      })
    }
  }

  render() {
    const { schedule } = this.props
    return (
      <React.Fragment>
        <div className="hero is-light is-medium">
          <div className="hero-body">
            <div className="container has-text-centered	">
              <h1 className="animated flipInX is-size-1">ตารางเรียน</h1>
              <h2 className="animated flipInX ">ตารางเวลาเรียนคอร์ส สอนสด</h2>
            </div>
          </div>
        </div>
        <div className="container is-fluid my-5">
          <nav className="breadcrumb" aria-label="breadcrumbs">
            <ul>
              <li>
                <Link to={'/offlinecourse'}>
                  <span className="icon is-small">
                    <i className="fas fa-pager" />
                  </span>
                  <span>Private course</span>
                </Link>
              </li>
              <li className="is-active">
                {' '}
                <Link to={'/schedule'} className="is-primary">
                  <span className="icon is-small">
                    <i className="fas fa-calendar-week" />
                  </span>
                  <span>Schedule</span>
                </Link>
              </li>
            </ul>
          </nav>
          <div className="table-container">
            <Table plan={schedule} />
          </div>
          <div
            style={{
              padding: '24px',
              borderRadius: '8px',
              boxShadow: '0 4px 12px 0 rgba(0, 0, 0, 0.05)',
              border: 'solid 1px #cdcdcd',
              backgroundColor: '#ffffff',
              width: '18rem'
            }}
          >
            <p className="mb-2 ff3 has-text-weight-bold is-size-3">
              แจ้งยืนยันกับผู้สอน
            </p>
            <p>ผ่านทาง</p>
            <p className="mb-2">
              <i
                className="fab fa-facebook-square mr-2"
                style={{ color: '#3b5998' }}
              />
              <a
                target="_blank"
                href="https://www.facebook.com/wellbalancedKORAT"
                rel="noopener noreferrer"
                style={{ color: '#4a4a4a' }}
              >
                Well-Balanced English
              </a>
            </p>
            <div
              className="mb-2"
              ref={this.setWrapperRef}
              onClick={() => this.setState({ activeModel: true })}
            >
              <i className="fab fa-line mr-2" style={{ color: '#28B463' }} />
              <span style={{ cursor: 'pointer' }}>line QR Code</span>
            </div>
            <p>
              หรือโทร
              <i
                className="fas fa-phone-square-alt mx-2"
                style={{ color: '#E74C3C' }}
              />
              <span>082-953-6564</span>
            </p>
          </div>
          <div className={this.state.activeModel ? 'modal is-active' : 'modal'}>
            <div className="modal-background" />
            <div
              className="modal-content"
              style={{ backgroundColor: '#fff', width: '400px' }}
              ref={this.setWrapperRef}
            >
              <p
                className="image"
                style={{
                  display: 'block',
                  marginLeft: 'auto',
                  marginRight: 'auto',
                  marginTop: '1rem',
                  marginBottom: '1rem',
                  width: '90%'
                }}
              >
                <img style={{ width: '400px' }} src={Line} alt="line qr code" />
              </p>
            </div>
          </div>
        </div>
        <Appfooter />
      </React.Fragment>
    )
  }
}

const mapStateToProps = state => ({
  schedule: state.schedule.data
})
export default connect(mapStateToProps)(TablePage)
