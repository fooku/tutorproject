import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'connected-react-router'
import store, { history } from './stores'

import * as serviceWorker from './serviceWorker'

const renderMethod = module.hot ? ReactDOM.render : ReactDOM.hydrate
renderMethod(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <App />
    </ConnectedRouter>
  </Provider>,
  document.getElementById('root')
)
serviceWorker.unregister()
