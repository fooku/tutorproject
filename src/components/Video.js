import React from 'react'
import ReactPlayer from 'react-player'
import StyledVideoWrapper from './styles/StyledVideoWrapper'
import StyledVideo from './styles/StyledVideo'
import baseurl from '../services/baseurl'

const Video = ({ active, autoplay, endCallback, progressCallback, img }) => (
  <StyledVideo>
    <StyledVideoWrapper>
      <ReactPlayer
        width="100%"
        height="100%"
        style={{ position: 'absolute', top: '0' }}
        playing={autoplay}
        controls={true}
        url={active.link && baseurl + active.link}
        // url="https://www.wellbalancedenglishapi.xyz/video/5d283ec25458d004e9e16c31-1080.mp4"
        onEnded={endCallback}
        onProgress={progressCallback}
        config={{ file: { attributes: { controlsList: 'nodownload' } } }}
        fileConfig={{
          attributes: {
            poster: img
          }
        }}
      />
    </StyledVideoWrapper>
  </StyledVideo>
)

export default Video
