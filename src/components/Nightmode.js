import React from 'react'
import StyledNightmode from './styles/StyledNightmode'
// import { Link } from 'react-router-dom'

const NightMode = ({ name, doc, nightModeCallback, nightMode }) => (
  <StyledNightmode>
    <div className="columns">
      <div className="column">
        {' '}
        <span>{name && name} </span>
      </div>
      <div className="column is-one-quarter">
        <nav className="level">
          <div className="level-left" />

          <div className="level-right">
            <a
              className="b"
              target="_blank"
              rel="noopener noreferrer"
              href={doc}
            >
              <span>เอกสาร</span>
              <i className="far fa-file-pdf mx-2 fa-lg" />
            </a>
          </div>
        </nav>
      </div>
    </div>

    {/* <label className="switch">
      <input type="checkbox" checked={nightMode} onChange={nightModeCallback} />
      <span className="slider round" />
    </label> */}
  </StyledNightmode>
)

export default NightMode
