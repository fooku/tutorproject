import styled from 'styled-components'

const StyledPlaylistitems = styled.div`
  padding: 0 20px;
  overflow-y: auto;
  height: auto;
`

export default StyledPlaylistitems
