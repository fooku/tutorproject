import styled from 'styled-components'

const StyledPlaylistitems = styled.div`
  padding: 0 0px;
  overflow-y: auto;
  height: auto;
  max-height: 27rem;
  border-top-style: solid;
  border-color: #474747;

  @media screen and (min-device-width: 1440px) and (max-device-width: 1499px) {
    max-height: 29rem;
  }
  @media screen and (min-device-width: 1300px) and (max-device-width: 1599px) {
    max-height: 32rem;
  }
  @media screen and (min-width: 1600px) {
    max-height: 34rem;
  }

  ::-webkit-scrollbar {
    width: 5px;
  }

  ::-webkit-scrollbar-track {
    background: transparent;
  }

  ::-webkit-scrollbar-thumb {
    background: #888;
    border-radius: 2px;
  }

  ::-webkit-scrollbar-thumb:hover {
    background: #555;
  }
`

export default StyledPlaylistitems
