import React from 'react'
import StyledPlaylistItem from './styles/StyledPlaylistItem'
import { Link } from 'react-router-dom'

const PlaylistItem = ({ num, video, active, played, idC }) => (
  <Link to={`/mycourse/${idC}/${video.id}`}>
    <StyledPlaylistItem active={active} played={played}>
      <div className="is-size-6 wbn-player__video-nr">{num}</div>
      <div className="is-size-6 wbn-player__video-name">
        {video && video.name}
      </div>
      {video.duration === undefined ? null : (
        <div className="is-size-6 wbn-player__video-time">
          {video && video.duration}
        </div>
      )}
    </StyledPlaylistItem>
  </Link>
)

export default PlaylistItem
