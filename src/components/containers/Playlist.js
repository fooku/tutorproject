import React, { Component } from 'react'
import PlaylistHeader from '../PlaylistHeader'
import PlaylistItems from '../containers/PlaylistItems'
import NightMode from '../Nightmode'
import StyledPlaylist from '../styles/StyledPlaylist'
import StyledPlaylistitems from '../styles/StyledPlaylistitems'

class Playlist extends Component {
  renderCard(list, active, idC) {
    return (
      list &&
      list.map((sec, index) => {
        console.log(sec.lectures)
        return (
          <React.Fragment key={index}>
            <PlaylistHeader
              key={'PlaylistHeader_' + index}
              active={sec}
              total={
                sec.lectures && sec.lectures !== undefined
                  ? sec.lectures.length
                  : 0
              }
            />
            <PlaylistItems
              key={'PlaylistItems_' + index}
              videos={sec.lectures}
              active={active}
              idC={idC}
            />
          </React.Fragment>
        )
      })
    )
  }

  render() {
    const { videos, active, idC, doc } = this.props
    console.log(active)

    return (
      <StyledPlaylist>
        <NightMode name={active.name} doc={doc} />
        <StyledPlaylistitems>
          {videos.length > 0 && this.renderCard(videos, active, idC)}
        </StyledPlaylistitems>
      </StyledPlaylist>
    )
  }
}

export default Playlist
