import React from 'react'
import PlaylistItem from '../PlaylistItem'
// import withLink from '../hoc/withLink'
import StyledPlaylistitemsTwo from '../styles/StyledPlaylistitemsTwo'

// const PlaylistItemWithLink = withLink(PlaylistItem)

const Playlistitems = ({ videos, active, idC }) => (
  <>
    {videos &&
      videos.map((video, index) => {
        return (
          <StyledPlaylistitemsTwo key={index}>
            <PlaylistItem
              key={video.id}
              num={index + 1}
              video={video}
              active={video.id === active.id}
              played={video.played}
              idC={idC}
            />
          </StyledPlaylistitemsTwo>
        )
      })}
  </>
)

export default Playlistitems
