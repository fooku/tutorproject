import React from 'react'
import StyledPlaylistHeader from './styles/StyledPlaylistHeader'
// import StyledJourney from './styles/StyledJourney'

const PlaylistHeader = ({ active, total }) => (
  <StyledPlaylistHeader>
    <p className="is-size-5">{active && active.name}</p>
    <span className="is-size-5">{total && total + ' lectures'}</span>
    {/* <StyledJourney>
      {active && active.num} / {total}
    </StyledJourney> */}
  </StyledPlaylistHeader>
)

export default PlaylistHeader
