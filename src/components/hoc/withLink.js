import React from 'react'
import { Link } from 'react-router-dom'

const withLink = WrappedComponent => props => {
  const newProps = {
    ...props,
    video: {
      ...props.video,
      title: (
        <Link to={`/mycourse/${props.idC}/${props.video.id}`}>
          {props.video.name}
        </Link>
      )
    }
  }

  return <WrappedComponent {...newProps} />
}

export default withLink
