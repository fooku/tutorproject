import BASE_URL from './baseurl'
import axios from 'axios'

const listStatusOrder = async () => {
  const URL = BASE_URL + '/restricted/status'

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  return axios
    .get(URL, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

export default {
  listStatusOrder
}
