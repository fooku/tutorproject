import BASE_URL from './baseurl'
import axios from 'axios'

const listOrder = async () => {
  const URL = BASE_URL + '/restricted/userorder'

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  return axios
    .get(URL, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => console.log(err))
}

const listOrders = async () => {
  const URL = BASE_URL + '/restricted/orders'

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  return axios
    .get(URL, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const getOrderOne = async id => {
  const URL = BASE_URL + '/restricted/orderone?idorder=' + id

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  return axios
    .get(URL, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const payment = async (id, data) => {
  const URL = BASE_URL + '/restricted/payment?idorder=' + id
  const URL2 = BASE_URL + '/restricted/orderone?idorder=' + id

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  await axios
    .post(URL, data, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))

  return axios
    .get(URL2, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const confirmPayment = async (id, book) => {
  const URL = BASE_URL + '/restricted/payment/confirm?idorder=' + id
  const URL2 = BASE_URL + '/restricted/orderone?idorder=' + id

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  await axios
    .post(URL, book, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))

  return axios
    .get(URL2, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

export default {
  listOrder,
  listOrders,
  getOrderOne,
  payment,
  confirmPayment
}
