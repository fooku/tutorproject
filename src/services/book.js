import axios from 'axios'
import Swal from 'sweetalert2'
import BASE_URL from './baseurl'

const addBook = async course => {
  console.log(course)
  const URL = BASE_URL + '/restricted/book'
  const URL2 = BASE_URL + '/restricted/books'

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  await axios
    .post(URL, course, { headers: { Authorization: token } })
    .then(Swal('เพิ่มเรียบร้อยแล้ว', 'success'))
    .catch(err => Swal('error!', '' + err, 'error'))

  return axios
    .get(URL2, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const listBooks = async () => {
  const URL = BASE_URL + '/restricted/books'

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }

  return axios
    .get(URL, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const deleteBook = async id => {
  const URL = BASE_URL + '/restricted/book?book_id=' + id
  const URL2 = BASE_URL + '/restricted/books'

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  await axios
    .delete(URL, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))

  return axios
    .get(URL2, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const getBook = async id => {
  const URL = BASE_URL + '/restricted/book?book_id=' + id

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }

  return axios
    .get(URL, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const bookUpdatePublish = async (id, isPublish) => {
  const URL =
    BASE_URL + '/restricted/book/publish?id=' + id + '&isPublish=' + isPublish
  const URL2 = BASE_URL + '/restricted/book?book_id=' + id

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  await axios
    .put(URL, null, { headers: { Authorization: token } })
    .then(
      isPublish
        ? Swal.fire({
            title: 'คอร์สนี้ได้เผยแพร่เรียบร้อยแล้ว',
            type: 'success'
          })
        : Swal.fire({
            title: 'คอร์สนี้ได้ถูกซ่อนเรียบร้อยแล้ว',
            type: 'success'
          })
    )
    .catch(err => Swal('error!', '' + err.response.data.message, 'error'))

  return axios
    .get(URL2, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const bookUpdate = async (id, book) => {
  const URL = BASE_URL + '/restricted/book?book_id=' + id
  const URL2 = BASE_URL + '/restricted/book?book_id=' + id

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  await axios
    .put(URL, book, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Swal('error!', '' + err.response.data.message, 'error'))

  return axios
    .get(URL2, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const listBooksPublished = async () => {
  const URL = BASE_URL + '/books'

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }

  return axios
    .get(URL, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const getBookPublished = async id => {
  const URL = BASE_URL + '/book?book_id=' + id

  return axios
    .get(URL)
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const getTracingBook = async id => {
  const URL = BASE_URL + '/restricted/book/purchase'

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }

  return axios
    .get(URL, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const listMyBook = async () => {
  const URL = BASE_URL + '/restricted/mybook'

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  return axios
    .get(URL, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const bookLinkUpdate = async (id, link) => {
  const URL = BASE_URL + '/restricted/book/link?id=' + id + '&link=' + link
  const URL2 = BASE_URL + '/restricted/book/purchase'

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  await axios
    .patch(URL, link, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Swal('error!', '' + err.response.data.message, 'error'))

  return axios
    .get(URL2, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

export default {
  addBook,
  listBooks,
  deleteBook,
  getBook,
  bookUpdatePublish,
  bookUpdate,
  listBooksPublished,
  getBookPublished,
  getTracingBook,
  listMyBook,
  bookLinkUpdate
}
