import axios from 'axios'
import Swal from 'sweetalert2'
import BASE_URL from './baseurl'

const login = user => {
  const URL = BASE_URL + '/login'

  return axios
    .post(URL, user)
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const auth = async () => {
  const URL = BASE_URL + '/restricted/auth'

  const token = await localStorage.getItem('token')
  console.log(token)
  if (token === '') {
    return null
  }
  return (
    token &&
    axios
      .get(URL, { headers: { Authorization: token } })
      .then(res => res.data)
      .catch(err => Promise.reject(err))
  )
}

const register = user => {
  const URL = BASE_URL + '/register'

  return axios
    .post(URL, user)
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const listMember = async () => {
  const URL = BASE_URL + '/restricted/member'

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  return axios
    .get(URL, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const deleteMember = async id => {
  const URL = BASE_URL + '/restricted/member?id=' + id
  const URLLIST = BASE_URL + '/restricted/member'

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  await axios
    .delete(URL, { headers: { Authorization: token } })
    .then(Swal('ลบเรียบร้อยแล้วจร้า', 'success'))
    .catch(err => Swal('error!', err, 'success'))

  return axios
    .get(URLLIST, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const updateUserType = async (id, user) => {
  console.log(id, user)
  const URL = BASE_URL + '/restricted/member/usertype?id=' + id
  const URLLIST = BASE_URL + '/restricted/member'

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  await axios
    .put(URL, user, { headers: { Authorization: token } })
    .then(Swal('แก้ไขเรียบร้อยแล้วจร้า', 'success'))
    .catch(err => Swal('error!', '' + err, 'error'))

  return axios
    .get(URLLIST, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const updateUser = async (id, user) => {
  const URL = BASE_URL + '/restricted/member?id=' + id
  const URL2 = BASE_URL + '/restricted/auth'

  const token = await localStorage.getItem('token')
  console.log(token)
  if (token === '') {
    return null
  }
  await axios
    .put(URL, user, { headers: { Authorization: token } })
    .then(Swal('แก้ไขเรียบร้อยแล้วจร้า', 'success'))
    .catch(err => Swal('error!', '' + err, 'error'))

  return (
    token &&
    axios
      .get(URL2, { headers: { Authorization: token } })
      .then(res => res.data)
      .catch(err => Promise.reject(err))
  )
}

const changePass = async data => {
  const URL = BASE_URL + '/restricted/pass'

  const token = await localStorage.getItem('token')
  console.log(token)
  if (token === '') {
    return null
  }
  return (
    token &&
    axios
      .put(URL, data, { headers: { Authorization: token } })
      .then(res => res.data)
      .catch(err => Promise.reject(err))
  )
}

export default {
  login,
  auth,
  register,
  listMember,
  deleteMember,
  updateUserType,
  updateUser,
  changePass
}
