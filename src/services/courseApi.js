import axios from 'axios'
import Swal from 'sweetalert2'
import BASE_URL from './baseurl'

const addCourse = async course => {
  console.log(course)
  const URL = BASE_URL + '/restricted/course'
  const URL2 = BASE_URL + '/restricted/courseall'

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  await axios
    .post(URL, course, { headers: { Authorization: token } })
    .then(Swal('เพิ่มเรียบร้อยแล้วจร้า', 'success'))
    .catch(err => Swal('error!', '' + err, 'error'))

  return axios
    .get(URL2, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const listCourse = async () => {
  const URL = BASE_URL + '/course'

  return axios
    .get(URL)
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const listCourseAll = async () => {
  const URL = BASE_URL + '/restricted/courseall'

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  return axios
    .get(URL, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const GetCourseOne = async id => {
  const URL = BASE_URL + '/restricted/courseone?id=' + id

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  return axios
    .get(URL, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const deleteCourse = async id => {
  const URL = BASE_URL + '/restricted/course?id=' + id
  const URLLIST = BASE_URL + '/course'

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  await axios
    .delete(URL, { headers: { Authorization: token } })
    .then(Swal('ลบเรียบร้อยแล้วจร้า', 'success'))
    .catch(err => Swal('error!', err, 'success'))

  return axios
    .get(URLLIST)
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const addSection = async (section, id) => {
  console.log(section)
  const URL = BASE_URL + '/restricted/section?id=' + id
  const URL2 = BASE_URL + '/restricted/courseone?id=' + id

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  await axios
    .post(URL, section, { headers: { Authorization: token } })
    .then(Swal('เพิ่มเรียบร้อยแล้วจร้า', 'success'))
    .catch(err => Swal('error!', '' + err, 'error'))

  return axios
    .get(URL2, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const deleteSection = async (idcourse, idsec) => {
  const URL =
    BASE_URL + '/restricted/section?idsec=' + idsec + '&idcourse=' + idcourse
  const URL2 = BASE_URL + '/restricted/courseone?id=' + idcourse

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  await axios
    .delete(URL, { headers: { Authorization: token } })
    .then(Swal('ลบเรียบร้อยแล้วจร้า', 'success'))
    .catch(err => console.log('err.>', err))

  return axios
    .get(URL2, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const deleteVideo = async (idlec, idsec, idcourse) => {
  console.log('check', idlec, idsec, idcourse)
  const URL =
    BASE_URL + '/restricted/lectures?idsec=' + idsec + '&idlec=' + idlec
  const URL2 = BASE_URL + '/restricted/courseone?id=' + idcourse

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  await axios
    .delete(URL, { headers: { Authorization: token } })
    .then(Swal('ลบเรียบร้อยแล้วจร้า', 'success'))
    .catch(err => Swal('error!', '' + err.response.data.message, 'error'))

  return axios
    .get(URL2, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const updateCourse = async (id, course) => {
  console.log('axios updateclourse', id, course)
  const URL = BASE_URL + '/restricted/course?id=' + id
  const URL2 = BASE_URL + '/restricted/courseone?id=' + id

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  await axios
    .put(URL, course, { headers: { Authorization: token } })
    .then(Swal('แก้ไขเรียบร้อยแล้วจร้า', 'success'))
    .catch(err => Swal('error!', '' + err, 'error'))

  return axios
    .get(URL2, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const updateSection = async (id, idsec, name) => {
  console.log('axios update name sec', id, name)
  const URL = BASE_URL + '/restricted/section?id=' + idsec
  const URL2 = BASE_URL + '/restricted/courseone?id=' + id

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  await axios
    .put(URL, name, { headers: { Authorization: token } })
    .then(Swal('แก้ไขเรียบร้อยแล้วจร้า', 'success'))
    .catch(err => Swal('error!', '' + err, 'error'))

  return axios
    .get(URL2, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const updateLec = async (id, idlec, name) => {
  console.log('axios update name lec', idlec, name)
  const URL = BASE_URL + '/restricted/lectures?id=' + idlec
  const URL2 = BASE_URL + '/restricted/courseone?id=' + id

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  await axios
    .put(URL, name, { headers: { Authorization: token } })
    .then(Swal('แก้ไขเรียบร้อยแล้วจร้า', 'success'))
    .catch(err => Swal('error!', '' + err.response.data, 'error'))

  return axios
    .get(URL2, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const GetCourseOnePublish = async id => {
  const URL = BASE_URL + '/courseone?id=' + id

  return axios
    .get(URL)
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const EditPublish = async (id, p) => {
  const URL = BASE_URL + '/restricted/course/publish?id=' + id + '&p=' + p
  const URL2 = BASE_URL + '/restricted/courseone?id=' + id

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  await axios
    .get(URL, { headers: { Authorization: token } })
    .then(
      p
        ? Swal.fire({
            title: 'คอร์สนี้ได้เผยแพร่เรียบร้อยแล้ว',
            type: 'success'
          })
        : Swal.fire({
            title: 'คอร์สนี้ได้ถูกซ่อนเรียบร้อยแล้ว',
            type: 'success'
          })
    )
    .catch(err => Swal('error!', '' + err.response.data.message, 'error'))

  return axios
    .get(URL2, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const EditDoc = async (id, p) => {
  const URL = BASE_URL + '/restricted/editdoc?id=' + id + '&p=' + p
  const URL2 = BASE_URL + '/restricted/courseone?id=' + id

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  await axios
    .get(URL, { headers: { Authorization: token } })
    .then(
      Swal.fire({
        title: 'แก้ไขlink เรียบร้อยแล้ว',
        type: 'success'
      })
    )
    .catch(err => Swal('error!', '' + err.response.data.message, 'error'))

  return axios
    .get(URL2, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const listMyCourse = async () => {
  const URL = BASE_URL + '/restricted/listmycourse'

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  return axios
    .get(URL, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const getMyCourse = async id => {
  const URL = BASE_URL + '/restricted/mycourse?id=' + id

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  return axios
    .get(URL, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

// const listMyCourse = async id => {
//   const URL = BASE_URL + '/restricted/mycourse?id=' + id

//   const token = await localStorage.getItem('token')
//   if (token === '') {
//     return null
//   }
//   return axios
//     .get(URL, { headers: { Authorization: token } })
//     .then(res => res.data)
//     .catch(err => Promise.reject(err))
// }

const UpdateSequence = async (id, data) => {
  const URL = BASE_URL + '/restricted/sequence'
  const URL2 = BASE_URL + '/restricted/courseone?id=' + id

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  await axios
    .put(URL, data, { headers: { Authorization: token } })
    .then(Swal('แก้ไขเรียบร้อยแล้ว', 'success'))
    .catch(err => Swal('error!', '' + err.response.data, 'error'))

  return axios
    .get(URL2, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const ListBundle = async () => {
  const URL = BASE_URL + '/restricted/bundle'

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  return axios
    .get(URL, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const ListBundlePubish = async () => {
  const URL = BASE_URL + '/bundlepubish'

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  return axios
    .get(URL, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const addBundle = async () => {
  const URL = BASE_URL + '/restricted/bundle'
  const URL2 = BASE_URL + '/restricted/bundle'

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  await axios
    .delete(URL, { headers: { Authorization: token } })
    .then(Swal('เพิ่มเรียบร้อยแล้วจร้า', 'success'))
    .catch(err => Swal('error!', '' + err, 'error'))

  return axios
    .get(URL2, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const getBundle = async id => {
  const URL = BASE_URL + '/restricted/bundleone?id=' + id

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  return axios
    .get(URL, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const getBundlePublish = async id => {
  const URL = BASE_URL + '/bundleone/publish?id=' + id

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  return axios
    .get(URL, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const updateBundle = async (id, bundle) => {
  console.log('axios', id, bundle)
  const URL = BASE_URL + '/restricted/bundle?id=' + id
  const URL2 = BASE_URL + '/restricted/bundleone?id=' + id

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  await axios
    .put(URL, bundle, { headers: { Authorization: token } })
    .then(Swal('แก้ไขเรียบร้อยแล้วจร้า', 'success'))
    .catch(err => Swal('error!', '' + err, 'error'))

  return axios
    .get(URL2, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const deleteBundle = async id => {
  const URL = BASE_URL + '/restricted/removebundle?id=' + id
  const URL2 = BASE_URL + '/restricted/bundle'

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  await axios
    .delete(URL, { headers: { Authorization: token } })
    .then(Swal('ลบเรียบร้อยแล้วจร้า', 'success'))
    .catch(err => Swal('error!', '' + err, 'error'))

  return axios
    .get(URL2, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const EditPublishBundle = async (id, p) => {
  const URL = BASE_URL + '/restricted/bundlepublish?id=' + id + '&p=' + p
  const URL2 = BASE_URL + '/restricted/bundleone?id=' + id

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  await axios
    .get(URL, { headers: { Authorization: token } })
    .then(
      p
        ? Swal.fire({
            title: 'คอร์สนี้ได้เผยแพร่เรียบร้อยแล้ว',
            type: 'success'
          })
        : Swal.fire({
            title: 'คอร์สนี้ได้ถูกซ่อนเรียบร้อยแล้ว',
            type: 'success'
          })
    )
    .catch(err => Swal('error!', '' + err.response.data.message, 'error'))

  return axios
    .get(URL2, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

export default {
  addCourse,
  listCourse,
  GetCourseOne,
  deleteCourse,
  addSection,
  deleteSection,
  deleteVideo,
  updateCourse,
  updateSection,
  updateLec,
  GetCourseOnePublish,
  EditPublish,
  listMyCourse,
  getMyCourse,
  UpdateSequence,
  listCourseAll,
  addBundle,
  ListBundle,
  getBundle,
  updateBundle,
  deleteBundle,
  ListBundlePubish,
  EditPublishBundle,
  getBundlePublish,
  EditDoc
}
