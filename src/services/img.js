import BASE_URL from './baseurl'
import axios from 'axios'

const listImg = async () => {
  const URL = BASE_URL + '/restricted/img'

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  return axios
    .get(URL, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const deleteImg = async id => {
  const URL = BASE_URL + '/restricted/img?idimg=' + id
  const URL2 = BASE_URL + '/restricted/img'

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  await axios
    .delete(URL, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))

  return axios
    .get(URL2, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const updateNameImg = async (id, name) => {
  const URL = BASE_URL + '/restricted/image?idimg=' + id
  const URL2 = BASE_URL + '/restricted/img'

  const data = {
    name: name
  }

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  await axios
    .put(URL, data, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))

  return axios
    .get(URL2, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

export default {
  listImg,
  deleteImg,
  updateNameImg
}
