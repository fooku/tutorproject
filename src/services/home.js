import BASE_URL from './baseurl'
import axios from 'axios'

const listHome = async () => {
  const URL = BASE_URL

  return axios
    .get(URL)
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

export default {
  listHome
}
