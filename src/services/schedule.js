import BASE_URL from './baseurl'
import axios from 'axios'
import Swal from 'sweetalert2'

const listSchedule = async () => {
  const URL = BASE_URL + '/schedule'

  return axios
    .get(URL)
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const UpdateSchedule = async (data, name) => {
  const URL = BASE_URL + '/restricted/schedule'
  const URL2 = BASE_URL + '/schedule'

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  await axios
    .put(URL, data, { headers: { Authorization: token } })
    .then(Swal(name, 'success'))
    .catch(err => Swal('error!', '' + err.response.data, 'error'))

  return axios
    .get(URL2, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

export default {
  listSchedule,
  UpdateSchedule
}
