import BASE_URL from './baseurl'
import axios from 'axios'
import Swal from 'sweetalert2'

const Toast = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
})

const listCart = async id => {
  const URL = BASE_URL + '/restricted/cart?iduser=' + id

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  return axios
    .get(URL, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const deleteItem = async (id, index) => {
  const URL = BASE_URL + '/restricted/oo?iduser=' + id
  const URL2 = BASE_URL + '/restricted/cart?iduser=' + id
  const data = {
    index
  }
  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  await axios
    .post(URL, data, { headers: { Authorization: token } })
    .then(res => {
      Toast.fire({
        type: 'success',
        title: 'สินค้าถูกลบเรียบร้อยแล้ว'
      })
      return res.data
    })
    .catch(err => Promise.reject(err))

  return axios
    .get(URL2, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const addItem = async (id, data) => {
  const URL = BASE_URL + '/restricted/cart?iduser=' + id

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  await axios
    .post(URL, data, { headers: { Authorization: token } })
    .then(res => {
      Toast.fire({
        type: 'success',
        title: 'สินค้าถูกเพิ่มในรถเข็นเรียบร้อยแล้ว'
      })
      return res.data
    })
    .catch(err => Promise.reject(err))

  return axios
    .get(URL, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const createOrder = async (id, address) => {
  const URL = BASE_URL + '/restricted/order?iduser=' + id
  const URL2 = BASE_URL + '/restricted/cart?iduser=' + id

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  await axios
    .post(URL, address, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))

  return axios
    .get(URL2, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

const getAlert = async id => {
  const URL = BASE_URL + '/restricted/useralert?id=' + id

  const token = await localStorage.getItem('token')
  if (token === '') {
    return null
  }
  return axios
    .get(URL, { headers: { Authorization: token } })
    .then(res => res.data)
    .catch(err => Promise.reject(err))
}

export default {
  listCart,
  addItem,
  deleteItem,
  createOrder,
  getAlert
}
