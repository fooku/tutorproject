import { createActionSet } from '../../helpers'
import api from '../../services/api'

export const LIST_SESSION = createActionSet('LIST_SESSION')
export const DELETE_MEMBER = createActionSet('DELETE_MEMBER')
export const UPDATE_MEMBER = createActionSet('UPDATE_MEMBER')

export const listMember = () => async dispatch => {
  dispatch({
    type: LIST_SESSION.PENDING,
    isFetching: true
  })

  try {
    const response = await api.listMember()
    console.log(response)

    dispatch({
      type: LIST_SESSION.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    console.log('error > ', error)
    dispatch({
      type: LIST_SESSION.FAILED,
      error: error
    })
  }
}

export const deleteMember = id => async dispatch => {
  dispatch({
    type: DELETE_MEMBER.PENDING,
    isFetching: true
  })

  try {
    const response = await api.deleteMember(id)
    console.log(response)

    dispatch({
      type: DELETE_MEMBER.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    console.log('error > ', error.response.data.message)
    dispatch({
      type: DELETE_MEMBER.FAILED,
      error: error.response.data.message
    })
  }
}

export const updateUsersType = (id, user) => async dispatch => {
  dispatch({
    type: UPDATE_MEMBER.PENDING,
    isFetching: true
  })

  try {
    const response = await api.updateUserType(id, user)
    console.log(response)

    dispatch({
      type: UPDATE_MEMBER.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    console.log('error > ', error.response.data.message)
    dispatch({
      type: UPDATE_MEMBER.FAILED,
      error: error.response.data.message
    })
  }
}
