import { createActionSet } from '../../helpers'
import api from '../../services/cart'

export const CART_SESSION = createActionSet('CART_SESSION')
const ALERT_SESSION = createActionSet('ALERT_SESSION')

export const listCart = () => async dispatch => {
  dispatch({
    type: CART_SESSION.PENDING,
    isFetching: true
  })

  try {
    const response = await api.listCart()

    dispatch({
      type: CART_SESSION.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    dispatch({
      type: CART_SESSION.FAILED,
      error: error
    })
  }
}

export const addItem = (id, data) => async dispatch => {
  dispatch({
    type: CART_SESSION.PENDING,
    isFetching: true
  })

  try {
    const response = await api.addItem(id, data)

    dispatch({
      type: CART_SESSION.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    dispatch({
      type: CART_SESSION.FAILED,
      error: error
    })
  }
}

export const deleteItem = (id, idex) => async dispatch => {
  console.log(id, idex)

  dispatch({
    type: CART_SESSION.PENDING,
    isFetching: true
  })

  try {
    const response = await api.deleteItem(id, idex)

    dispatch({
      type: CART_SESSION.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    dispatch({
      type: CART_SESSION.FAILED,
      error: error
    })
  }
}

export const createOrder = (id, address) => async dispatch => {
  dispatch({
    type: CART_SESSION.PENDING,
    isFetching: true
  })

  try {
    const response = await api.createOrder(id, address)

    dispatch({
      type: CART_SESSION.SUCCESS,
      payload: response,
      error: ''
    })

    const userAlert = await api.getAlert(id)
    dispatch({
      type: ALERT_SESSION.SUCCESS,
      payload: userAlert,
      error: ''
    })
  } catch (error) {
    dispatch({
      type: CART_SESSION.FAILED,
      error: error
    })
  }
}
