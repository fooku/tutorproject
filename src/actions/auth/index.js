import { push } from 'connected-react-router'

import { createActionSet } from '../../helpers'
import api from '../../services/api'
import api2 from '../../services/cart'

export const USER_SESSION = createActionSet('USER_SESSION')
export const USER_LOGOUT = createActionSet('USER_LOGOUT')
export const USER_AUTH = createActionSet('USER_AUTH')
export const USER_REGISTER = createActionSet('USER_REGISTER')
export const CART_SESSION = createActionSet('CART_SESSION')
export const ORDERS_SESSION = createActionSet('ORDERS_SESSION')
export const ALERT_SESSION = createActionSet('ALERT_SESSION')

export const login = user => async dispatch => {
  dispatch({
    type: USER_SESSION.PENDING,
    isFetching: true
  })

  try {
    const response = await api.login(user)
    console.log(response)
    const token = `Bearer ${response.token}`

    dispatch({
      type: USER_SESSION.SUCCESS,
      payload: response.user,
      error: ''
    })

    localStorage.setItem('token', token)

    dispatch(push('/'))
  } catch (error) {
    let err = { login }
    console.log('error > ', error.response)
    err.login = error.response.data.message
    dispatch({
      type: USER_SESSION.FAILED,
      error: err
    })
  }
}

export const logout = () => dispatch => {
  dispatch({
    type: USER_LOGOUT.PENDING
  })

  try {
    dispatch({
      type: USER_LOGOUT.SUCCESS,
      error: ''
    })

    localStorage.removeItem('token')

    dispatch(push('/'))
  } catch (error) {
    dispatch({
      type: USER_LOGOUT.FAILED,
      error
    })
  }
}

export const verifyauth = () => async dispatch => {
  dispatch({
    type: USER_AUTH.PENDING,
    isFetching: true
  })

  try {
    const response = await api.auth()
    console.log(response)

    if (response === null) {
      dispatch({
        type: USER_AUTH.FAILED,
        error: null
      })
    } else {
      dispatch({
        type: USER_AUTH.SUCCESS,
        payload: response,
        error: ''
      })
      try {
        dispatch({
          type: CART_SESSION.PENDING,
          isFetching: true
        })

        const cart = await api2.listCart(response.ID)

        dispatch({
          type: CART_SESSION.SUCCESS,
          payload: cart,
          error: ''
        })

        const userAlert = await api2.getAlert(response.ID)

        dispatch({
          type: ALERT_SESSION.SUCCESS,
          payload: userAlert,
          error: ''
        })
      } catch (error) {
        dispatch({
          type: CART_SESSION.FAILED,
          error: error
        })
      }
    }

    // localStorage.setItem('token', response.token)
  } catch (error) {
    console.log('error > ', error)

    dispatch({
      type: USER_AUTH.FAILED,
      error: error
    })
  }
}

export const register = user => async dispatch => {
  dispatch({
    type: USER_REGISTER.PENDING,
    isFetching: true
  })

  try {
    const response = await api.register(user)
    console.log(response)

    console.log(response.Message)
    if (response.Message === 'Succeed') {
      const userlogin = {
        email: user.email,
        password: user.password
      }
      console.log(userlogin)
      const response = await api.login(userlogin)
      console.log(response)
      const token = `Bearer ${response.token}`

      dispatch({
        type: USER_SESSION.SUCCESS,
        payload: response.user,
        error: ''
      })
      localStorage.setItem('token', token)
    }

    dispatch(push('/'))
  } catch (error) {
    let err = { register }
    console.log('error > ', error.response.data)
    const er = error.response.data.Err
    if (error.response.data.Code === 11000) {
      const e = er.split('"')
      console.log(er.localeCompare('telephonenumber'))
      console.log(er.localeCompare('email'))
      if (er.includes('telephonenumber')) {
        err.register = 'เบอร์ ' + e[1] + ' ได้ถูกใช้ไปแล้ว'
      } else if (er.includes('email')) {
        err.register = 'อีเมล ' + e[1] + ' ได้ถูกใช้ไปแล้ว'
      }
    }
    dispatch({
      type: USER_REGISTER.FAILED,
      error: err
    })
  }
}

export const errorRegister = error => dispatch => {
  dispatch({
    type: USER_REGISTER.PENDING
  })
  let err = { register: error }
  dispatch({
    type: USER_REGISTER.FAILED,
    error: err
  })
}

export const updateUser = (id, user) => async dispatch => {
  dispatch({
    type: USER_AUTH.PENDING,
    isFetching: true
  })

  try {
    const response = await api.updateUser(id, user)
    console.log(response)

    if (response === null) {
      dispatch({
        type: USER_AUTH.FAILED,
        error: null
      })
    } else {
      dispatch({
        type: USER_AUTH.SUCCESS,
        payload: response,
        error: ''
      })
    }

    // localStorage.setItem('token', response.token)
  } catch (error) {
    console.log('error > ', error)

    dispatch({
      type: USER_AUTH.FAILED,
      error: error
    })
  }
}
