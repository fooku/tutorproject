import { createActionSet } from '../../helpers'
import api from '../../services/status'

export const STATUS_SESSION = createActionSet('STATUS_SESSION')

export const listStatusOrder = () => async dispatch => {
  dispatch({
    type: STATUS_SESSION.PENDING,
    isFetching: true
  })

  try {
    const response = await api.listStatusOrder()

    dispatch({
      type: STATUS_SESSION.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    dispatch({
      type: STATUS_SESSION.FAILED,
      error: error
    })
  }
}
