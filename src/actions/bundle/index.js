import { createActionSet } from '../../helpers'
import api from '../../services/courseApi'

export const BUNDLE_SESSION = createActionSet('BUNDLE_SESSION')
export const BUNDLE_ONE_SESSION = createActionSet('BUNDLE_ONE_SESSION')

export const listBundle = () => async dispatch => {
  dispatch({
    type: BUNDLE_SESSION.PENDING,
    isFetching: true
  })

  try {
    const response = await api.ListBundle()
    console.log(response)

    dispatch({
      type: BUNDLE_SESSION.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    console.log('error > ', error)
    dispatch({
      type: BUNDLE_SESSION.FAILED,
      error: error
    })
  }
}

export const getBundle = id => async dispatch => {
  dispatch({
    type: BUNDLE_SESSION.PENDING,
    isFetching: true
  })

  try {
    const response = await api.getBundle(id)
    console.log('BUNDLE_ONE_SESSION', response)

    dispatch({
      type: BUNDLE_ONE_SESSION.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    console.log('error > ', error)
    dispatch({
      type: BUNDLE_SESSION.FAILED,
      error: error
    })
  }
}

export const getBundlePublish = id => async dispatch => {
  dispatch({
    type: BUNDLE_SESSION.PENDING,
    isFetching: true
  })

  try {
    const response = await api.getBundlePublish(id)
    console.log('BUNDLE_ONE_SESSION', response)

    dispatch({
      type: BUNDLE_ONE_SESSION.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    console.log('error > ', error)
    dispatch({
      type: BUNDLE_SESSION.FAILED,
      error: error
    })
  }
}

export const updateBundle = (id, bundle) => async dispatch => {
  dispatch({
    type: BUNDLE_SESSION.PENDING,
    isFetching: true
  })

  try {
    const response = await api.updateBundle(id, bundle)
    console.log('BUNDLE_ONE_SESSION', response)

    dispatch({
      type: BUNDLE_ONE_SESSION.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    console.log('error > ', error)
    dispatch({
      type: BUNDLE_SESSION.FAILED,
      error: error
    })
  }
}

export const deleteBundle = id => async dispatch => {
  dispatch({
    type: BUNDLE_SESSION.PENDING,
    isFetching: true
  })

  try {
    const response = await api.deleteBundle(id)
    console.log('BUNDLE_ONE_SESSION', response)

    dispatch({
      type: BUNDLE_SESSION.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    console.log('error > ', error)
    dispatch({
      type: BUNDLE_SESSION.FAILED,
      error: error
    })
  }
}

export const addBundle = () => async dispatch => {
  dispatch({
    type: BUNDLE_SESSION.PENDING,
    isFetching: true
  })

  try {
    const response = await api.addBundle()
    console.log(response)

    dispatch({
      type: BUNDLE_SESSION.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    console.log('error > ', error)
    dispatch({
      type: BUNDLE_SESSION.FAILED,
      error: error
    })
  }
}

export const listBundlePubish = () => async dispatch => {
  dispatch({
    type: BUNDLE_SESSION.PENDING,
    isFetching: true
  })

  try {
    const response = await api.ListBundlePubish()
    console.log(response)

    dispatch({
      type: BUNDLE_SESSION.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    console.log('error > ', error)
    dispatch({
      type: BUNDLE_SESSION.FAILED,
      error: error
    })
  }
}

export const editPublishBundle = (id, p) => async dispatch => {
  dispatch({
    type: BUNDLE_SESSION.PENDING,
    isFetching: true
  })

  try {
    const response = await api.EditPublishBundle(id, p)
    console.log('xx', response)

    dispatch({
      type: BUNDLE_ONE_SESSION.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    console.log('error > ', error.response)
    dispatch({
      type: BUNDLE_SESSION.FAILED,
      error: error.response
    })
  }
}
