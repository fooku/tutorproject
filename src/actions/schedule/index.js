import { createActionSet } from '../../helpers'
import api from '../../services/schedule'

export const SCHEDULE_SESSION = createActionSet('SCHEDULE_SESSION')

export const listSchedule = () => async dispatch => {
  dispatch({
    type: SCHEDULE_SESSION.PENDING,
    isFetching: true
  })

  try {
    const response = await api.listSchedule()

    dispatch({
      type: SCHEDULE_SESSION.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    dispatch({
      type: SCHEDULE_SESSION.FAILED,
      error: error
    })
  }
}

// export const deleteImg = id => async dispatch => {
//   dispatch({
//     type: SCHEDULE_SESSION.PENDING,
//     isFetching: true
//   })

//   try {
//     const response = await api.deleteImg(id)

//     dispatch({
//       type: SCHEDULE_SESSION.SUCCESS,
//       payload: response,
//       error: ''
//     })
//   } catch (error) {
//     dispatch({
//       type: SCHEDULE_SESSION.FAILED,
//       error: error
//     })
//   }
// }

export const UpdateSchedule = (data, name) => async dispatch => {
  dispatch({
    type: SCHEDULE_SESSION.PENDING,
    isFetching: true
  })

  try {
    const response = await api.UpdateSchedule(data, name)

    dispatch({
      type: SCHEDULE_SESSION.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    dispatch({
      type: SCHEDULE_SESSION.FAILED,
      error: error
    })
  }
}
