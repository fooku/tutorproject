import { createActionSet } from '../../helpers'
import api from '../../services/order'
import api2 from '../../services/cart'

export const ORDERS_SESSION = createActionSet('ORDERS_SESSION')
export const ORDER_SESSION = createActionSet('ORDER_SESSION')
const ALERT_SESSION = createActionSet('ALERT_SESSION')

export const listOrder = id => async dispatch => {
  dispatch({
    type: ORDERS_SESSION.PENDING,
    isFetching: true
  })

  try {
    const response = await api.listOrder()
    response.reverse()

    dispatch({
      type: ORDERS_SESSION.SUCCESS,
      payload: response,
      error: ''
    })

    if (id !== 'no') {
      const userAlert = await api2.getAlert(id)
      dispatch({
        type: ALERT_SESSION.SUCCESS,
        payload: userAlert,
        error: ''
      })
    }
  } catch (error) {
    console.log('err', error)
    dispatch({
      type: ORDERS_SESSION.FAILED,
      error: error
    })
  }
}

export const listOrders = () => async dispatch => {
  dispatch({
    type: ORDERS_SESSION.PENDING,
    isFetching: true
  })

  try {
    const response = await api.listOrders()
    console.log(response)

    dispatch({
      type: ORDERS_SESSION.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    dispatch({
      type: ORDERS_SESSION.FAILED,
      error: error
    })
  }
}

export const getOrderOne = id => async dispatch => {
  dispatch({
    type: ORDER_SESSION.PENDING,
    isFetching: true
  })

  try {
    const response = await api.getOrderOne(id)
    console.log(response)

    dispatch({
      type: ORDER_SESSION.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    dispatch({
      type: ORDER_SESSION.FAILED,
      error: error
    })
  }
}

export const payment = (id, data) => async dispatch => {
  dispatch({
    type: ORDER_SESSION.PENDING,
    isFetching: true
  })

  try {
    const response = await api.payment(id, data)
    console.log(response)

    dispatch({
      type: ORDER_SESSION.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    dispatch({
      type: ORDER_SESSION.FAILED,
      error: error
    })
  }
}

export const confirmPayment = (id, book) => async dispatch => {
  dispatch({
    type: ORDER_SESSION.PENDING,
    isFetching: true
  })

  try {
    const response = await api.confirmPayment(id, book)
    console.log(response)

    dispatch({
      type: ORDER_SESSION.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    dispatch({
      type: ORDER_SESSION.FAILED,
      error: error
    })
  }
}
