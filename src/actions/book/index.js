import { createActionSet } from '../../helpers'
import api from '../../services/book'
import api2 from '../../services/cart'

export const BOOK_ADD = createActionSet('BOOK_ADD')
export const BOOK_DETAIL = createActionSet('BOOK_DETAIL')
export const BOOK_TRACING = createActionSet('BOOK_Tracing')
export const ALERT_SESSION = createActionSet('ALERT_SESSION')
export const LIST_MY_BOOKS = createActionSet('LIST_MY_BOOKS')

export const addBook = book => async dispatch => {
  dispatch({
    type: BOOK_ADD.PENDING,
    isFetching: true
  })

  try {
    const response = await api.addBook(book)
    console.log('xx', response)

    dispatch({
      type: BOOK_ADD.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    console.log('error > ', error.response)
    dispatch({
      type: BOOK_ADD.FAILED,
      error: error.response
    })
  }
}

export const listBooks = () => async dispatch => {
  dispatch({
    type: BOOK_ADD.PENDING,
    isFetching: true
  })

  try {
    const response = await api.listBooks()
    console.log('xx', response)

    dispatch({
      type: BOOK_ADD.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    console.log('error > ', error.response)
    dispatch({
      type: BOOK_ADD.FAILED,
      error: error.response
    })
  }
}

export const deleteBook = id => async dispatch => {
  dispatch({
    type: BOOK_ADD.PENDING,
    isFetching: true
  })

  try {
    console.log('id', id)
    const response = await api.deleteBook(id)
    console.log('xx', response)

    dispatch({
      type: BOOK_ADD.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    console.log('error > ', error.response)
    dispatch({
      type: BOOK_ADD.FAILED,
      error: error.response
    })
  }
}

export const getBook = id => async dispatch => {
  dispatch({
    type: BOOK_ADD.PENDING,
    isFetching: true
  })

  try {
    console.log('id', id)
    const response = await api.getBook(id)
    console.log('xx', response)

    dispatch({
      type: BOOK_DETAIL.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    console.log('error > ', error.response)
    dispatch({
      type: BOOK_ADD.FAILED,
      error: error.response
    })
  }
}

export const updateBookPublish = (id, isPublish) => async dispatch => {
  dispatch({
    type: BOOK_ADD.PENDING,
    isFetching: true
  })

  try {
    console.log('id', id)
    const response = await api.bookUpdatePublish(id, isPublish)
    console.log('xx', response)

    dispatch({
      type: BOOK_DETAIL.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    console.log('error > ', error.response)
    dispatch({
      type: BOOK_ADD.FAILED,
      error: error.response
    })
  }
}

export const updateBook = (idBook, book) => async dispatch => {
  dispatch({
    type: BOOK_ADD.PENDING,
    isFetching: true
  })

  try {
    const response = await api.bookUpdate(idBook, book)

    dispatch({
      type: BOOK_DETAIL.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    console.log('error > ', error.response)
    dispatch({
      type: BOOK_ADD.FAILED,
      error: error.response
    })
  }
}

export const listBooksPublished = () => async dispatch => {
  dispatch({
    type: BOOK_ADD.PENDING,
    isFetching: true
  })

  try {
    const response = await api.listBooksPublished()
    console.log('xx', response)

    dispatch({
      type: BOOK_ADD.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    console.log('error > ', error.response)
    dispatch({
      type: BOOK_ADD.FAILED,
      error: error.response
    })
  }
}

export const getBookPublished = id => async dispatch => {
  dispatch({
    type: BOOK_ADD.PENDING,
    isFetching: true
  })

  try {
    console.log('id', id)
    const response = await api.getBookPublished(id)
    console.log('xx', response)

    dispatch({
      type: BOOK_DETAIL.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    console.log('error > ', error.response)
    dispatch({
      type: BOOK_ADD.FAILED,
      error: error.response
    })
  }
}

export const listTraceBooks = () => async dispatch => {
  dispatch({
    type: BOOK_ADD.PENDING,
    isFetching: true
  })

  try {
    const response = await api.getTracingBook()
    console.log('xx', response)

    dispatch({
      type: BOOK_TRACING.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    console.log('error > ', error.response)
    dispatch({
      type: BOOK_ADD.FAILED,
      error: error.response
    })
  }
}

export const listMyBook = id => async dispatch => {
  dispatch({
    type: LIST_MY_BOOKS.PENDING,
    isFetching: true
  })

  try {
    const response = await api.listMyBook()

    dispatch({
      type: LIST_MY_BOOKS.SUCCESS,
      payload: response.reverse(),
      error: ''
    })

    if (id !== 'no') {
      const userAlert = await api2.getAlert(id)

      dispatch({
        type: ALERT_SESSION.SUCCESS,
        payload: userAlert,
        error: ''
      })
    }
  } catch (error) {
    console.log('error > ', error.response)
    dispatch({
      type: LIST_MY_BOOKS.FAILED,
      error: error.response
    })
  }
}

export const updateLinkBooks = (id, book) => async dispatch => {
  dispatch({
    type: BOOK_ADD.PENDING,
    isFetching: true
  })

  try {
    const response = await api.bookLinkUpdate(id, book)
    console.log('xx', response)

    dispatch({
      type: BOOK_TRACING.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    console.log('error > ', error.response)
    dispatch({
      type: BOOK_ADD.FAILED,
      error: error.response
    })
  }
}
