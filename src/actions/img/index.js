import { createActionSet } from '../../helpers'
import api from '../../services/img'

export const IMG_SESSION = createActionSet('IMG_SESSION')

export const listImg = () => async dispatch => {
  dispatch({
    type: IMG_SESSION.PENDING,
    isFetching: true
  })

  try {
    const response = await api.listImg()

    dispatch({
      type: IMG_SESSION.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    dispatch({
      type: IMG_SESSION.FAILED,
      error: error
    })
  }
}

export const deleteImg = id => async dispatch => {
  dispatch({
    type: IMG_SESSION.PENDING,
    isFetching: true
  })

  try {
    const response = await api.deleteImg(id)

    dispatch({
      type: IMG_SESSION.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    dispatch({
      type: IMG_SESSION.FAILED,
      error: error
    })
  }
}

export const updateNameImg = (id, name) => async dispatch => {
  dispatch({
    type: IMG_SESSION.PENDING,
    isFetching: true
  })

  try {
    const response = await api.updateNameImg(id, name)

    dispatch({
      type: IMG_SESSION.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    dispatch({
      type: IMG_SESSION.FAILED,
      error: error
    })
  }
}
