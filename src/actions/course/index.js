import { createActionSet } from '../../helpers'
import api from '../../services/courseApi'
import api2 from '../../services/cart'
import Swal from 'sweetalert2'
import { push } from 'connected-react-router'

export const LIST_COURSES = createActionSet('LIST_COURSES')
export const GET_COURSE = createActionSet('GET_COURSE')
export const LIST_MY_COURSES = createActionSet('LIST_MY_COURSES')
export const LIST_MY_COURSES2 = createActionSet('LIST_MY_COURSES2')
export const GET_MY_COURSES = createActionSet('GET_MY_COURSES')
export const ALERT_SESSION = createActionSet('ALERT_SESSION')

export const addCourse = course => async dispatch => {
  dispatch({
    type: LIST_COURSES.PENDING,
    isFetching: true
  })

  try {
    const response = await api.addCourse(course)
    console.log('xx', response)

    dispatch({
      type: LIST_COURSES.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    console.log('error > ', error.response)
    dispatch({
      type: LIST_COURSES.FAILED,
      error: error.response
    })
  }
}

export const listCourse = () => async dispatch => {
  dispatch({
    type: LIST_COURSES.PENDING,
    isFetching: true
  })

  try {
    const response = await api.listCourse()
    console.log('xx', response)

    dispatch({
      type: LIST_COURSES.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    console.log('error > ', error.response)
    dispatch({
      type: LIST_COURSES.FAILED,
      error: error.response
    })
  }
}

export const listCourseAll = () => async dispatch => {
  dispatch({
    type: LIST_COURSES.PENDING,
    isFetching: true
  })

  try {
    const response = await api.listCourseAll()
    console.log('xx', response)

    dispatch({
      type: LIST_COURSES.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    console.log('error > ', error.response)
    dispatch({
      type: LIST_COURSES.FAILED,
      error: error.response
    })
  }
}

export const getCourseOne = id => async dispatch => {
  dispatch({
    type: GET_COURSE.PENDING,
    isFetching: true
  })

  try {
    const response = await api.GetCourseOne(id)
    console.log('xx', response)

    dispatch({
      type: GET_COURSE.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    console.log('error > ', error.response)
    dispatch({
      type: GET_COURSE.FAILED,
      error: error.response
    })
  }
}

export const deleteCourse = id => async dispatch => {
  dispatch({
    type: LIST_COURSES.PENDING,
    isFetching: true
  })

  try {
    const response = await api.deleteCourse(id)
    console.log('xx', response)

    dispatch({
      type: LIST_COURSES.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    console.log('error > ', error.response)
    dispatch({
      type: LIST_COURSES.FAILED,
      error: error.response
    })
  }
}

export const addSection = (section, id) => async dispatch => {
  dispatch({
    type: GET_COURSE.PENDING,
    isFetching: true
  })

  try {
    const response = await api.addSection(section, id)
    console.log('xx', response)

    dispatch({
      type: GET_COURSE.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    console.log('error > ', error.response)
    dispatch({
      type: GET_COURSE.FAILED,
      error: error.response
    })
  }
}

export const deleteSection = (idcourse, idsec) => async dispatch => {
  dispatch({
    type: GET_COURSE.PENDING,
    isFetching: true
  })

  try {
    const response = await api.deleteSection(idcourse, idsec)
    console.log('xx', response)

    dispatch({
      type: GET_COURSE.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    console.log('error > ', error.response)
    dispatch({
      type: GET_COURSE.FAILED,
      error: error.response
    })
  }
}

export const deleteLec = (idlec, idsec, idcourse) => async dispatch => {
  console.log('asdfasdf >> ', idlec, idsec, idcourse)
  dispatch({
    type: GET_COURSE.PENDING,
    isFetching: true
  })

  try {
    const response = await api.deleteVideo(idlec, idsec, idcourse)
    console.log('xx', response)

    dispatch({
      type: GET_COURSE.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    console.log('error > ', error.response)
    dispatch({
      type: GET_COURSE.FAILED,
      error: error.response
    })
  }
}

export const updateCourse = (idcourse, course) => async dispatch => {
  dispatch({
    type: GET_COURSE.PENDING,
    isFetching: true
  })

  try {
    const response = await api.updateCourse(idcourse, course)

    dispatch({
      type: GET_COURSE.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    console.log('error > ', error.response)
    dispatch({
      type: GET_COURSE.FAILED,
      error: error.response
    })
  }
}

export const updateSection = (idcourse, idsec, name) => async dispatch => {
  dispatch({
    type: GET_COURSE.PENDING,
    isFetching: true
  })

  try {
    const response = await api.updateSection(idcourse, idsec, name)

    dispatch({
      type: GET_COURSE.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    console.log('error > ', error.response)
    dispatch({
      type: GET_COURSE.FAILED,
      error: error.response
    })
  }
}

export const updateLec = (idcourse, idlec, name) => async dispatch => {
  dispatch({
    type: GET_COURSE.PENDING,
    isFetching: true
  })

  try {
    const response = await api.updateLec(idcourse, idlec, name)

    dispatch({
      type: GET_COURSE.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    console.log('error > ', error.response)
    dispatch({
      type: GET_COURSE.FAILED,
      error: error.response
    })
  }
}

export const getCourseOnePublish = id => async dispatch => {
  dispatch({
    type: GET_COURSE.PENDING,
    isFetching: true
  })

  try {
    const response = await api.GetCourseOnePublish(id)
    console.log('xx', response)

    dispatch({
      type: GET_COURSE.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    console.log('error > ', error.response)
    dispatch({
      type: GET_COURSE.FAILED,
      error: error.response
    })
  }
}

export const editPublish = (id, p) => async dispatch => {
  dispatch({
    type: GET_COURSE.PENDING,
    isFetching: true
  })

  try {
    const response = await api.EditPublish(id, p)
    console.log('xx', response)

    dispatch({
      type: GET_COURSE.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    console.log('error > ', error.response)
    dispatch({
      type: GET_COURSE.FAILED,
      error: error.response
    })
  }
}

export const editDoc = (id, p) => async dispatch => {
  dispatch({
    type: GET_COURSE.PENDING,
    isFetching: true
  })

  try {
    const response = await api.EditDoc(id, p)
    console.log('xx', response)

    dispatch({
      type: GET_COURSE.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    console.log('error > ', error.response)
    dispatch({
      type: GET_COURSE.FAILED,
      error: error.response
    })
  }
}

export const listMyCourse = id => async dispatch => {
  dispatch({
    type: LIST_MY_COURSES.PENDING,
    isFetching: true
  })

  try {
    const response = await api.listMyCourse()

    dispatch({
      type: LIST_MY_COURSES.SUCCESS,
      payload: response.reverse(),
      error: ''
    })

    if (id !== 'no') {
      const userAlert = await api2.getAlert(id)

      dispatch({
        type: ALERT_SESSION.SUCCESS,
        payload: userAlert,
        error: ''
      })
    }
  } catch (error) {
    console.log('error > ', error.response)
    dispatch({
      type: LIST_MY_COURSES.FAILED,
      error: error.response
    })
  }
}

export const getMyCourse = id => async dispatch => {
  dispatch({
    type: GET_MY_COURSES.PENDING,
    isFetching: true
  })

  try {
    const response = await api.getMyCourse(id)
    console.log('xx', response)

    if (response.section) {
      if (response.section[0] !== undefined && response.section[0]) {
        response.section[0].lectures.sort((a, b) =>
          a.sequence > b.sequence ? 1 : -1
        )
      }
    }

    dispatch({
      type: GET_MY_COURSES.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    if (
      error.response.data &&
      error.response.data !== undefined &&
      error.response.data.message &&
      error.response.data.message !== undefined &&
      error.response.data.message !== 'missing or malformed jwt'
    ) {
      Swal('error!', '' + error.response.data.message, 'error')
      dispatch(push('/'))
    }

    dispatch({
      type: GET_MY_COURSES.FAILED,
      error: error.response
    })
  }
}

export const UpdateSequence = (id, data) => async dispatch => {
  dispatch({
    type: GET_COURSE.PENDING,
    isFetching: true
  })

  try {
    const response = await api.UpdateSequence(id, data)
    console.log('xx', response)

    dispatch({
      type: GET_COURSE.SUCCESS,
      payload: response,
      error: ''
    })
  } catch (error) {
    console.log('error > ', error.response)
    dispatch({
      type: GET_COURSE.FAILED,
      error: error.response
    })
  }
}

// export const listMyCourse = id => async dispatch => {
//   dispatch({
//     type: LIST_MY_COURSES.PENDING,
//     isFetching: true
//   })

//   try {
//     const response = await api.listMyCourse(id)
//     console.log('xx', response)

//     dispatch({
//       type: LIST_MY_COURSES.SUCCESS,
//       payload: response,
//       error: ''
//     })
//   } catch (error) {
//     console.log('error > ', error.response)
//     dispatch({
//       type: LIST_MY_COURSES.FAILED,
//       error: error.response
//     })
//   }
// }
