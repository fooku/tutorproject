import React, { Component } from 'react'
import { Link } from 'react-router-dom'

class Appfooter extends Component {
  componentDidMount() {
    const script = document.createElement('script')

    script.src =
      'https://www.trustmarkthai.com/callbackData/initialize.js?t=6b4f4-37-5-5830ad4602ccd6190d11bda57b87aafe85981a3f'
    script.id = 'dbd-init'
    script.async = true

    document.body.appendChild(script)
  }

  render() {
    return (
      <footer className="footer footer-light-medium">
        <div className="container">
          <div className="columns">
            <div className="column is-4">
              <div className="mb-20 color-6">
                <img
                  className="small-footer-logo"
                  src="https://i.imgur.com/X7he3HJ.png"
                  alt=""
                />
                <div className="footer-description mt-2 color-6">
                  เรียนภาษาอังกฤษ "ฟัง-พูด ถูกต้อง | อ่าน-เขียน ได้ใจความ"
                  มัธยมปลาย | มหาวิทยาลัย | วัยทำงาน | ติวสอบแบบส่วนตัว
                  มาตรฐานเดียวกันทุกคลาส และปรับเนื้อหาตามพื้นฐานผู้เรียน.{' '}
                  <i className="fa fa-heart color-5" />
                </div>
              </div>
              <div>
                <div className="columns mt-1">
                  <div className="column is-4 pt-3">
                    <span id="Certificate-banners" />
                  </div>
                  <div className="column">
                    <div
                      className="social-links mt-2"
                      style={{ justifyContent: 'top' }}
                    >
                      <a
                        href="https://www.facebook.com/wellbalancedKORAT"
                        target="_blank"
                        className="mr-1"
                        rel="noopener noreferrer"
                      >
                        <span className="icon color-4">
                          <i className="fab fa-facebook-f" />
                        </span>
                      </a>
                      <a href="/" target="_blank">
                        <span className="icon color-4">
                          <i className="fab fa-twitter" />
                        </span>
                      </a>
                      <a href="/" target="_blank">
                        <span className="icon color-4">
                          <i className="fab fa-instagram" />
                        </span>
                      </a>
                      <a href="/" target="_blank">
                        <span className="icon color-4">
                          <i className="fab fa-pinterest-p" />
                        </span>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div className="column is-6 is-offset-2">
              <div className="columns">
                <div className="column">
                  <ul className="footer-column">
                    <li className="column-header">Links</li>
                    <li className="column-item">
                      <Link to="/">Home</Link>
                    </li>
                    <li className="column-item">
                      <Link to="/course">Course Online</Link>
                    </li>
                    <li className="column-item">
                      <Link to="/howtobuy">How to order</Link>
                    </li>{' '}
                    <li className="column-item">
                      <Link to="/policy">policy</Link>
                    </li>
                  </ul>
                </div>

                <div className="column">
                  <ul className="footer-column">
                    <li className="column-header">Purchase</li>
                    <li className="column-item">
                      <Link to="/cart">Cart</Link>
                    </li>
                    <li className="column-item">
                      <Link to="/order">Order</Link>
                    </li>
                    {/* <li className="column-item">
                    <Link to="/cart">Cart</Link>
                    </li> */}
                  </ul>
                </div>

                <div className="column">
                  <ul className="footer-column">
                    <li className="column-header">Contact</li>
                    <li className="column-item">
                      <a href="https://www.facebook.com/wellbalancedKORAT">
                        Facebook
                      </a>
                    </li>
                    <li className="column-item">
                      <a href="https://www.facebook.com/wellbalancedKORAT">
                        Line
                      </a>
                    </li>
                    <li className="column-item">
                      <a href="https://www.facebook.com/wellbalancedKORAT">
                        Twitter{' '}
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </footer>
    )
  }
}

export default Appfooter
