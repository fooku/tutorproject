import React, { Component } from 'react'
import { connect } from 'react-redux'

class CardSection extends Component {
  renderCard(list) {
    list.sort((a, b) => (a.sequence > b.sequence ? 1 : -1))

    return (
      list &&
      list.map((course, index) => {
        return (
          <header className="card-header" key={index}>
            <p className="card-header-title">{course.name}</p>
          </header>
        )
      })
    )
  }

  render() {
    const { name, lectures } = this.props
    return (
      <React.Fragment>
        <div className="card mt-4">
          <header className="card-header">
            <p className="card-header-title"> {name}</p>
          </header>
          <div className="card-content">
            <div className="card">{lectures && this.renderCard(lectures)}</div>
          </div>
        </div>
      </React.Fragment>
    )
  }
}

export default connect()(CardSection)
