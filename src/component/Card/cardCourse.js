import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { deleteCourse } from '../../actions/course'
import renderHTML from 'react-render-html'

class Card extends Component {
  render() {
    let { name, detail, img, id, dispatch } = this.props
    const url = '/coursemanage/' + id
    if (detail.length > 130) detail = detail.substring(0, 130) + '...'
    return (
      <React.Fragment>
        <div className="column is-4">
          <div className="card is-fixed-height">
            <Link to={url}>
              <div className="card-image">
                <figure className="image is-5by6 crop">
                  <img className="overlay" src={img} alt="Placeholder" />
                </figure>
                <div className="middle">
                  <div className="text has-text-weight-bold color-6">
                    <i className="fas fa-wrench fa-5x" />
                  </div>
                </div>
              </div>
            </Link>
            <div className="card-content">
              <div className="content">
                <h1 className="has-text-centered">{name}</h1>
                {renderHTML(detail)}
              </div>
            </div>

            <footer className="card-footer">
              {/* <Link to={url} className="card-footer-item a">
                View on{' '}
              </Link> */}
              <span
                className="card-footer-item b has-text-danger"
                onClick={() => dispatch(deleteCourse(id))}
              >
                Delete{' '}
              </span>
            </footer>
          </div>
        </div>
      </React.Fragment>
    )
  }
}

export default connect()(Card)
