import React, { Component } from 'react'
import { connect } from 'react-redux'
import baseurl from '../../services/baseurl'
import { deleteImg, updateNameImg } from '../../actions/img'
import Swal from 'sweetalert2'

class Card extends Component {
  constructor(props) {
    super(props)

    this.state = {
      canEdit: false,
      nameEdit: '',
      loaded: '',
      total: ''
    }
  }

  componentWillReceiveProps() {
    this.setState({
      canEdit: false
    })
  }

  onHandleEditName(name) {
    this.setState({
      canEdit: !this.state.canEdit,
      nameEdit: name
    })
  }

  onChangeInput(e) {
    this.setState({
      nameEdit: e.target.value
    })
  }

  onHandleDelete(id) {
    const { dispatch } = this.props
    Swal.fire({
      title: 'Are you sure?',
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then(result => {
      if (result.value) {
        dispatch(deleteImg(id))
      }
    })
  }

  render() {
    let { name, img, id, dispatch, isFetching } = this.props
    const { canEdit, nameEdit } = this.state
    return (
      <React.Fragment>
        <div className="column is-3">
          <div className="card is-fixed-height">
            {canEdit ? (
              <header className="card-header">
                <input
                  className="input m-2"
                  type="text"
                  placeholder="ชื่อรูปภาพ"
                  value={nameEdit}
                  onChange={e => this.onChangeInput(e)}
                />
              </header>
            ) : (
              <header className="card-header">
                <p className="card-header-title">
                  {name.length > 30 ? name.substring(0, 30) + '...' : name}
                </p>
                <span
                  className="card-header-icon"
                  aria-label="more options"
                  onClick={() => this.onHandleEditName(name)}
                >
                  <span className="icon has-text-warning">
                    <i className="fas fa-pen-square fa-lg" />
                  </span>
                </span>
              </header>
            )}

            <div className="card-image">
              <figure className="image is-5by6 crop2">
                <img
                  className="overlay"
                  src={baseurl + img}
                  alt="Placeholder"
                />
              </figure>
            </div>
            {/* <div className="card-content">
              <div className="content has-text-centered">
                <h1>{name}</h1>
                {detail}
              </div>
            </div> */}
            {canEdit ? (
              <footer className="card-footer">
                {isFetching ? (
                  <span className="card-footer-item has-text-danger">
                    loading{' '}
                  </span>
                ) : (
                  <React.Fragment>
                    <span
                      className="card-footer-item b has-text-success"
                      onClick={() => dispatch(updateNameImg(id, nameEdit))}
                    >
                      Save{' '}
                    </span>
                    <span
                      className="card-footer-item b has-text-danger"
                      onClick={() => this.onHandleEditName()}
                    >
                      Cancel{' '}
                    </span>
                  </React.Fragment>
                )}
              </footer>
            ) : (
              <footer className="card-footer">
                {isFetching ? (
                  <span className="card-footer-item has-text-danger">
                    loading{' '}
                  </span>
                ) : (
                  <span
                    className="card-footer-item b has-text-danger"
                    onClick={() => this.onHandleDelete(id)}
                  >
                    Delete{' '}
                  </span>
                )}
              </footer>
            )}
          </div>
        </div>
      </React.Fragment>
    )
  }
}

export default connect()(Card)
