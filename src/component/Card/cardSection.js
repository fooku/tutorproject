import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import {
  deleteSection,
  deleteLec,
  updateSection,
  updateLec,
  UpdateSequence
} from '../../actions/course'
import Modal from 'react-responsive-modal'
import AddVideoForm from '../../views/Manage/CourseManage/components/addVideoForm'
import baseurl from '../../services/baseurl'

class CardSection extends Component {
  constructor(props) {
    super(props)
    this.state = {
      open: false,
      editName: false,
      editSequence: false,
      editLec: false,
      name: this.props.name,
      open2: false,
      open3: false,
      link: '',
      nameLec: '',
      idLec: '',
      lecturesInstate: this.props.lectures
    }
  }

  handleSubmit = (values, dispatch) => {
    // this.setState({ open: false })
    console.log(values, dispatch)

    // dispatch(getCourseOne(this.props.idcourse))
  }

  onOpenModal = () => {
    this.setState({ open: true })
  }

  onCloseModal = () => {
    this.setState({ open: false })
  }

  onEdit = () => {
    this.setState({ editName: !this.state.editName })
  }

  onEditSequence = () => {
    console.log('99', this.props.lectures)

    this.setState({
      editSequence: !this.state.editSequence,
      lectures: this.props.lectures
    })
  }

  handleChangeName = event => {
    this.setState({ name: event.target.value })
  }

  handleChangeNameLec = event => {
    this.setState({ nameLec: event.target.value })
  }

  editName = () => {
    const { name } = this.state
    const { idsec, idcourse, dispatch } = this.props
    const n = {
      name: name
    }
    dispatch(updateSection(idcourse, idsec, n))
  }

  onOpenModal2 = () => {
    this.setState({ open2: !this.state.open2, nameLec: '' })
  }
  onView(p, n) {
    console.log(p)
    this.setState({ open2: !this.state.open2, link: p, nameLec: n })
  }

  onModal3 = () => {
    this.setState({ open3: !this.state.open3, nameLec: '', idLec: '' })
  }
  onOpenModal3(n, id) {
    this.setState({ open3: !this.state.open3, nameLec: n, idLec: id })
  }
  onEditLec = () => {
    const { nameLec, idLec } = this.state
    const { idcourse, dispatch } = this.props
    const n = {
      name: nameLec
    }
    dispatch(updateLec(idcourse, idLec, n))
  }

  up(sequence) {
    let { lecturesInstate } = this.state

    const lec = lecturesInstate.map(element => {
      if (element.sequence === sequence - 1) {
        element.sequence++
        return element
      } else if (element.sequence === sequence) {
        element.sequence--
        return element
      } else {
        return element
      }
    })
    this.setState({
      lecturesInstate: lec
    })
  }

  down(sequence) {
    let { lecturesInstate } = this.state

    const lec = lecturesInstate.map(element => {
      if (element.sequence === sequence + 1) {
        element.sequence--
        return element
      } else if (element.sequence === sequence) {
        element.sequence++
        return element
      } else {
        return element
      }
    })
    this.setState({
      lecturesInstate: lec
    })
  }

  Sequence() {
    console.log('enter')

    const { dispatch, idcourse } = this.props
    const { lecturesInstate } = this.state
    const data = {
      sequencelectures: lecturesInstate
    }

    dispatch(UpdateSequence(idcourse, data))
  }

  renderCard(list) {
    console.log('00', list.sort((a, b) => (a.sequence > b.sequence ? 1 : -1)))

    return (
      list &&
      list.map((course, index) => {
        return (
          <header className="card-header" key={index}>
            <p className="card-header-title">{course.name}</p>
            {this.state.editSequence ? (
              <>
                {index === 0 ? null : (
                  <Link
                    to="#"
                    className="card-header-icon"
                    aria-label="more options"
                    onClick={() => this.up(course.sequence)}
                  >
                    <span className="icon color-5">
                      <i className="fas fa-chevron-circle-up" />
                    </span>
                  </Link>
                )}
                {index === list.length - 1 ? (
                  <span className="ml-5"> </span>
                ) : (
                  <Link
                    to="#"
                    className="card-header-icon"
                    aria-label="more options"
                    onClick={() => this.down(course.sequence)}
                  >
                    <span className="icon color-6">
                      <i className="fas fa-chevron-circle-down" />
                    </span>
                  </Link>
                )}
              </>
            ) : (
              <React.Fragment>
                <Link
                  to="#"
                  className="card-header-icon"
                  aria-label="more options"
                  onClick={() => this.onOpenModal3(course.name, course.id)}
                >
                  <span className="icon color-5">
                    <i className="fas fa-marker" />
                  </span>
                </Link>
                <Link
                  to="#"
                  className="card-header-icon"
                  aria-label="more options"
                  onClick={() => this.onView(course.link, course.name)}
                >
                  <span className="icon color-6">
                    <i className="fas fa-video" />
                  </span>
                </Link>
                <Link
                  to="#"
                  className="card-header-icon"
                  aria-label="more options"
                  onClick={() =>
                    this.props.dispatch(
                      deleteLec(
                        course.id,
                        this.props.idsec,
                        this.props.idcourse
                      )
                    )
                  }
                >
                  <span className="icon">
                    <i className="fas fa-trash-alt" />
                  </span>
                </Link>
              </React.Fragment>
            )}
          </header>
        )
      })
    )
  }

  render() {
    const { name, idsec, idcourse, dispatch, lectures } = this.props
    const { lecturesInstate } = this.state
    return (
      <React.Fragment>
        <div className="card mt-4">
          <header className="card-header">
            {this.state.editName ? (
              <React.Fragment>
                <div className="field has-addons m-2">
                  <p className="control">
                    <button className="button is-static">name</button>
                  </p>
                  <p className="control">
                    <input
                      className="input"
                      type="text"
                      value={this.state.name}
                      onChange={this.handleChangeName}
                    />
                  </p>
                  <p className="control">
                    <button
                      className="button is-success"
                      onClick={this.editName}
                    >
                      Save
                    </button>
                  </p>
                  <p className="control">
                    <button className="button is-warning" onClick={this.onEdit}>
                      Cancel
                    </button>
                  </p>
                </div>
              </React.Fragment>
            ) : this.state.editSequence ? (
              <React.Fragment>
                <p className="card-header-title"> {name}</p>
                <button
                  className="button is-success mt-1 mr-3"
                  onClick={() => this.Sequence()}
                >
                  Save Sequence
                </button>
                <button
                  className="button is-danger mt-1 mr-3"
                  onClick={this.onEditSequence}
                >
                  Cancel
                </button>
              </React.Fragment>
            ) : (
              <React.Fragment>
                <p className="card-header-title"> {name}</p>
                <Link
                  to="#"
                  className="card-header-icon "
                  aria-label="more options"
                  onClick={this.onOpenModal}
                >
                  <span className="icon color-6">
                    <i className="fas fa-plus-square fa-2x " />
                  </span>
                </Link>
              </React.Fragment>
            )}
            <Modal
              open={this.state.open}
              onClose={this.onCloseModal}
              closeIconSize={52}
              center
            >
              <AddVideoForm
                onSubmit={this.handleSubmit}
                id={idcourse}
                idsec={idsec}
              />
            </Modal>
          </header>
          <div className="card-content">
            {this.state.editSequence ? (
              <div className="card">
                {lecturesInstate && this.renderCard(lecturesInstate)}
              </div>
            ) : (
              <div className="card">
                {lectures && this.renderCard(lectures)}
              </div>
            )}
          </div>
          <footer className="card-footer">
            <span className="card-footer-item a" onClick={this.onEdit}>
              Edit Name
            </span>
            <span className="card-footer-item a" onClick={this.onEditSequence}>
              Edit Sequence
            </span>
            <span
              className="card-footer-item b"
              onClick={() => dispatch(deleteSection(idcourse, idsec))}
            >
              Delete
            </span>
          </footer>
        </div>
        <Modal
          open={this.state.open2}
          onClose={this.onOpenModal2}
          closeIconSize={52}
          center
        >
          <span className="is-size-2 mb-3">{this.state.nameLec}</span>
          <video controls id="plyr-player">
            <source
              src={`${baseurl}${this.state.link}`}
              type="video/mp4"
              size="1080"
            />
          </video>
          {/* <Plyr link={this.state.link} /> */}
        </Modal>
        <Modal
          open={this.state.open3}
          onClose={this.onModal3}
          closeIconSize={50}
          center
        >
          <span className="is-size-2 mb-3 mr-5 pr-5">Edit Lectures</span>
          <div className="control">
            <input
              className="input my-1"
              type="text"
              value={this.state.nameLec}
              onChange={this.handleChangeNameLec}
            />
            <button
              className="button is-fullwidth is-success is-outlined"
              onClick={this.onEditLec}
            >
              save
            </button>
          </div>
        </Modal>
      </React.Fragment>
    )
  }
}

export default connect()(CardSection)
