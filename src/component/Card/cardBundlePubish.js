import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
// import renderHTML from 'react-render-html'

class Card extends Component {
  render() {
    let { name, detail, img, price, time, id, priceold } = this.props
    const url = '/bundle/' + id
    if (detail.length > 130) detail = detail.substring(0, 130) + '...'
    return (
      <React.Fragment>
        <div className="column is-4">
          <Link to={url}>
            <div className="card is-fixed-height">
              <div className="card-image">
                <figure className="image is-5by6 crop">
                  <img className="overlay" src={img} alt="Placeholder" />
                </figure>
                <div className="middle">
                  <div className="text has-text-weight-bold">
                    <i className="fas fa-info-circle fa-5x" />
                    {/* <i className="fas fa-sign-in-alt fa-5x" /> */}
                  </div>
                </div>
              </div>
              <div className="card-content">
                <div className="content">
                  <h1 className="has-text-centered">{name}</h1>
                  {/* {renderHTML(detail)} */}
                </div>
              </div>
              <footer className="card-footer">
                <span className="card-footer-item has-text-weight-bold	">
                  <small
                    className="mr-2"
                    style={{ textDecorationLine: 'line-through' }}
                  >
                    {priceold && priceold + '฿'}
                  </small>
                  <span className="has-text-danger">
                    {price && price + '฿'}
                  </span>
                </span>
                <span className="card-footer-item">
                  {time && time + ' Days'}
                </span>
              </footer>
            </div>
          </Link>
        </div>
      </React.Fragment>
    )
  }
}

export default connect()(Card)
