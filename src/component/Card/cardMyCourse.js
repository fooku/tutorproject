import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
// import renderHTML from 'react-render-html'

class Card extends Component {
  render() {
    let { name, detail, img, timeleft, idc } = this.props

    const url = '/mycourse/' + idc
    if (detail.length > 130) detail = detail.substring(0, 130) + '...'
    return (
      <React.Fragment>
        <div className="column is-4">
          <div className="card is-fixed-height">
            <Link to={url}>
              <div className="card-image">
                <figure className="image is-5by6 crop">
                  <img className="overlay" src={img} alt="Placeholder" />
                </figure>
                <div className="middle">
                  <div className="text has-text-weight-bold color-6">
                    <i className="fas fa-sign-in-alt fa-5x" />
                    {/* <i className="fas fa-sign-in-alt fa-5x" /> */}
                  </div>
                </div>
              </div>
              <div className="card-content">
                <div className="content">
                  <h1 className="has-text-centered">{name}</h1>
                  {/* {renderHTML(detail)} */}
                </div>
              </div>
            </Link>
            <footer className="card-footer">
              {timeleft > 0 ? (
                <span className="card-footer-item">
                  {'เวลาเรียนเหลือ ' + timeleft + ' วัน'}
                </span>
              ) : (
                <span className="card-footer-item has-text-danger">
                  {'เวลาเรียนหมดไปแล้ว ' + timeleft * -1 + ' วัน'}{' '}
                </span>
              )}
            </footer>
          </div>
        </div>
      </React.Fragment>
    )
  }
}

export default connect()(Card)
