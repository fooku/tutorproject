import React, { Component } from 'react'
import { NavLink, Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { logout } from '../../actions/auth'

class AppHeaderM extends Component {
  constructor(props) {
    super(props)
    this.setWrapperRef = this.setWrapperRef.bind(this)
    this.handleClickOutside = this.handleClickOutside.bind(this)

    this.state = {
      isActive: false
    }
  }

  componentDidMount() {
    document.addEventListener('click', this.handleClickOutside)
    document.addEventListener('touchstart', this.handleClickOutside)

    document
      .querySelector('.navbar-burger')
      .addEventListener('click', toggleNav)

    function toggleNav() {
      var nav = document.querySelector('.navbar-menu')
      if (nav.className === 'navbar-menu') {
        nav.className = 'navbar-menu is-active'
      } else {
        nav.className = 'navbar-menu'
      }
    }
  }
  closeBurger() {
    this.setState({
      isActive: false
    })

    var nav = document.querySelector('.navbar-menu')
    nav.className = 'navbar-menu'
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.handleClickOutside)
    document.removeEventListener('touchstart', this.handleClickOutside)
  }

  menu() {
    this.setState({
      isActive: !this.state.isActive
    })
  }

  setWrapperRef(node) {
    this.wrapperRef = node
  }

  handleClickOutside(event) {
    if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      this.setState({
        isActive: false
      })
      this.closeBurger()
    }
  }

  render() {
    const { isAuthenticated, dispatch, currentUser, cart } = this.props
    return (
      <>
        <nav
          id="navbar"
          className="navbar is-fixed-top"
          ref={this.setWrapperRef}
          style={{ backgroundColor: '#1C2833' }}
        >
          <div className="">
            <div className="navbar-brand">
              <NavLink className="navbar-item" to="/">
                {/* <img src="https://i.imgur.com/3quZjmv.png" alt="logo" /> */}
                <span
                  style={{ fontSize: '1.8rem', color: '#E5E7E9' }}
                  className="has-text-weight-bold	"
                >
                  Well-Balanced
                </span>
              </NavLink>
              <div
                className="navbar-burger burger"
                style={{ color: '#E5E7E9' }}
              >
                <span />
                <span />
                <span />
              </div>
            </div>
            <div id="navbarBasicExample" className="navbar-menu">
              <div className="navbar-start">
                <NavLink
                  exact
                  className="navbar-item"
                  to="/"
                  onClick={() => this.closeBurger()}
                >
                  Home
                </NavLink>
                <NavLink
                  className="navbar-item"
                  to="/course"
                  onClick={() => this.closeBurger()}
                >
                  Online Courses
                </NavLink>
                <NavLink
                  className="navbar-item"
                  to="/offlinecourse"
                  onClick={() => this.closeBurger()}
                >
                  Private Courses
                </NavLink>
                <NavLink
                  className="navbar-item s"
                  to="/book"
                  onClick={() => this.closeBurger()}
                >
                  eBooks
                </NavLink>
                <NavLink
                  className="navbar-item"
                  to="/howtobuy"
                  onClick={() => this.closeBurger()}
                >
                  How to order
                </NavLink>
              </div>

              <div className="navbar-end">
                {isAuthenticated ? (
                  <React.Fragment>
                    <NavLink
                      className="navbar-item s mr-1"
                      to="/mycourse"
                      onClick={() => this.closeBurger()}
                    >
                      My Course
                    </NavLink>
                    <NavLink
                      className="navbar-item s mr-1"
                      to="/order"
                      onClick={() => this.closeBurger()}
                    >
                      Order
                    </NavLink>
                    <NavLink
                      className="navbar-item s"
                      to="/cart"
                      onClick={() => this.closeBurger()}
                    >
                      <i className="fas fa-shopping-cart" />

                      {cart !== undefined && cart.length > 0 ? (
                        <span className="notify-badge-cart">
                          {' '}
                          {cart.length}
                        </span>
                      ) : null}
                    </NavLink>
                    <div className="navbar-item">
                      <div
                        className={
                          this.state.isActive
                            ? 'navbar-menu navbar-item has-dropdown is-active'
                            : 'navbar-menu navbar-item has-dropdown'
                        }
                        onClick={() => this.menu()}
                      >
                        <label className="navbar-link tt">
                          {currentUser && (
                            <p className="is-size-5">{currentUser.email}</p>
                          )}
                        </label>
                        <div className="navbar-dropdown is-boxed">
                          <Link
                            to="/profile"
                            className="navbar-item"
                            onClick={() => this.closeBurger()}
                          >
                            <span>Profile</span>
                          </Link>
                          <Link
                            to="/changepassword"
                            className="navbar-item"
                            onClick={() => this.closeBurger()}
                          >
                            <span>ChangePassword</span>
                          </Link>
                          {currentUser.usertype === 'admin' ? (
                            <React.Fragment>
                              <hr className="navbar-divider" />
                              <Link
                                to="/uploadimage"
                                className="navbar-item"
                                onClick={() => this.closeBurger()}
                              >
                                <span>Upload Image</span>
                              </Link>
                              <Link
                                to="/usersmanage"
                                className="navbar-item"
                                onClick={() => this.closeBurger()}
                              >
                                <span>Manage Users</span>
                              </Link>
                              <Link
                                to="/ordermanage"
                                className="navbar-item"
                                onClick={() => this.closeBurger()}
                              >
                                <span>Manage Order</span>
                              </Link>
                              <Link
                                to="/coursemanage"
                                className="navbar-item"
                                onClick={() => this.closeBurger()}
                              >
                                <span>Manage Course</span>
                              </Link>
                              <Link
                                to="/schedulemagage"
                                className="navbar-item"
                                onClick={() => this.closeBurger()}
                              >
                                <span>Manage Schedule</span>
                              </Link>
                              <Link
                                to="/bookmanage"
                                className="navbar-item"
                                onClick={() => this.closeBurger()}
                              >
                                <span>Manage Book</span>
                              </Link>
                            </React.Fragment>
                          ) : null}
                          <hr className="navbar-divider" />
                          <Link
                            to="/"
                            className="navbar-item"
                            onClick={() => {
                              dispatch(logout())
                            }}
                          >
                            <span className="icon">
                              <i className="fas fa-user" />
                            </span>
                            <span>Logout</span>
                          </Link>
                        </div>
                      </div>
                    </div>
                  </React.Fragment>
                ) : (
                  <div className="navbar-item">
                    <div className="field is-grouped">
                      <p className="control">
                        <NavLink
                          className="button is-primary"
                          to="/login"
                          onClick={() => this.closeBurger()}
                        >
                          <span className="icon">
                            <i className="fas fa-sign-in-alt" />
                          </span>
                          <span>Login</span>
                        </NavLink>
                      </p>
                      <p className="control">
                        <NavLink
                          className="button"
                          to="/register"
                          onClick={() => this.closeBurger()}
                        >
                          <span className="icon">
                            <i className="fas fa-user-plus" />
                          </span>
                          <span>Register</span>
                        </NavLink>
                      </p>
                    </div>
                  </div>
                )}
              </div>
            </div>
          </div>
        </nav>
        <div style={{ height: '59px' }} />
      </>
    )
  }
}
const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
  currentUser: state.auth.currentUser,
  cart: state.cart.cart
})

export default connect(mapStateToProps)(AppHeaderM)
