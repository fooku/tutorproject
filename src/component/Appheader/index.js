import React, { Component } from 'react'
import { NavLink, Link } from 'react-router-dom'
import { connect } from 'react-redux'
import { logout } from '../../actions/auth'
// import Logo from '../../img/logo.png'

class AppHeader extends Component {
  constructor(props) {
    super(props)
    this.setWrapperRef = this.setWrapperRef.bind(this)
    this.handleClickOutside = this.handleClickOutside.bind(this)

    this.state = {
      isActive: false
    }
  }

  componentDidMount() {
    document.addEventListener('click', this.handleClickOutside)
    document.addEventListener('touchstart', this.handleClickOutside)
    document.getElementById('navbar').style.backgroundColor = 'transparent'
    window.addEventListener('scroll', this.scrollNavbar)

    document
      .querySelector('.navbar-burger')
      .addEventListener('click', toggleNav)

    function toggleNav() {
      var nav = document.querySelector('.navbar-menu')
      if (nav.className === 'navbar-menu') {
        nav.className = 'navbar-menu is-active'
        document.getElementById('navbar').style.backgroundColor = '#fff'
      } else {
        nav.className = 'navbar-menu'
        if (
          document.body.scrollTop > 80 ||
          document.documentElement.scrollTop > 80
        ) {
          document.getElementById('navbar').style.backgroundColor = ''
          document.getElementById('navbar').style.boxShadow =
            '0 2px 6px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)'
        } else {
          document.getElementById('navbar').style.backgroundColor =
            'transparent'
          document.getElementById('navbar').style.boxShadow = ''
        }
      }
    }
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.scrollNavbar)
    document.removeEventListener('click', this.handleClickOutside)
    document.removeEventListener('touchstart', this.handleClickOutside)
  }

  scrollNavbar() {
    var nav = document.querySelector('.navbar-menu')
    if (nav.className === 'navbar-menu') {
      if (
        document.body.scrollTop > 80 ||
        document.documentElement.scrollTop > 80
      ) {
        document.getElementById('navbar').style.backgroundColor = '#fff'
        document.getElementById('navbar').style.boxShadow =
          '0 2px 6px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)'
      } else {
        document.getElementById('navbar').style.backgroundColor = 'transparent'
        document.getElementById('navbar').style.boxShadow = ''
      }
    } else {
    }
  }

  closeBurger() {
    this.setState({
      isActive: false
    })

    var nav = document.querySelector('.navbar-menu')
    nav.className = 'navbar-menu'

    if (document.documentElement.scrollTop > 80) {
      document.getElementById('navbar').style.backgroundColor = '#fff'
      document.getElementById('navbar').style.boxShadow =
        '0 2px 6px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)'
    } else {
      document.getElementById('navbar').style.backgroundColor = 'transparent'
      document.getElementById('navbar').style.boxShadow = ''
    }
  }

  menu() {
    this.setState({
      isActive: !this.state.isActive
    })
  }

  setWrapperRef(node) {
    this.wrapperRef = node
  }

  handleClickOutside(event) {
    if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
      this.setState({
        isActive: false
      })
      this.closeBurger()
    }
  }

  render() {
    const { isAuthenticated, dispatch, currentUser, cart, alert } = this.props
    return (
      <nav
        id="navbar"
        className="navbar is-white is-fixed-top"
        ref={this.setWrapperRef}
      >
        <div className="container">
          <div className="navbar-brand">
            <NavLink className="navbar-item" to="/">
              {/* <img src={Logo} alt="logo" width="45" height="45" /> */}
              <span
                style={{ fontSize: '1.8rem' }}
                className="has-text-weight-bold	"
              >
                Well-Balanced
              </span>
            </NavLink>
            <div className="navbar-burger burger">
              <span />
              <span />
              <span />
            </div>
          </div>
          <div id="navbarBasicExample" className="navbar-menu">
            <div className="navbar-start">
              <NavLink
                exact
                className="navbar-item s"
                to="/"
                onClick={() => this.closeBurger()}
              >
                Home
              </NavLink>

              <NavLink
                className="navbar-item s"
                to="/course"
                onClick={() => this.closeBurger()}
              >
                Online
              </NavLink>
              <NavLink
                className="navbar-item s"
                to="/offlinecourse"
                onClick={() => this.closeBurger()}
              >
                Private
              </NavLink>
              {/* <div className="navbar-item has-dropdown is-hoverable">
                <NavLink to="#" className="navbar-link">
                  Courses
                </NavLink>

                <div className="navbar-dropdown">
                  <Link
                    className="navbar-item"
                    to="/course"
                    onClick={() => this.closeBurger()}
                  >
                    Online Courses
                  </Link>
                  <Link
                    className="navbar-item"
                    to="/offlinecourse"
                    onClick={() => this.closeBurger()}
                  >
                    Private Courses
                  </Link>
                </div>
              </div> */}

              <NavLink
                className="navbar-item s"
                to="/book"
                onClick={() => this.closeBurger()}
              >
                eBooks
              </NavLink>
              <NavLink
                className="navbar-item s"
                to="/howtobuy"
                onClick={() => this.closeBurger()}
              >
                How to order
              </NavLink>
            </div>

            <div className="navbar-end">
              {isAuthenticated ? (
                <React.Fragment>
                  <NavLink
                    className="navbar-item s mr-1"
                    to="/mycourse"
                    onClick={() => this.closeBurger()}
                  >
                    My Corner
                    {alert.mycoursealert !== undefined &&
                    alert.mycoursealert !== 0 ? (
                      <span className="notify-badge">
                        {' '}
                        {/* {alert.mycoursealert} */}
                        <i className="fas fa-bell" />
                      </span>
                    ) : null}
                  </NavLink>
                  <NavLink
                    className="navbar-item s mr-1"
                    to="/order"
                    onClick={() => this.closeBurger()}
                  >
                    Order
                    {alert.orderalert !== undefined &&
                    alert.orderalert !== 0 ? (
                      <span className="notify-badge-order">
                        {' '}
                        {/* {alert.orderalert} */}
                        <i className="fas fa-bell" />
                      </span>
                    ) : null}
                  </NavLink>
                  <NavLink
                    className="navbar-item s"
                    to="/cart"
                    onClick={() => this.closeBurger()}
                  >
                    <i className="fas fa-shopping-cart" />

                    {cart !== undefined && cart.length > 0 ? (
                      <span className="notify-badge-cart"> {cart.length}</span>
                    ) : null}
                  </NavLink>
                  <div className="navbar-item">
                    <div
                      className={
                        this.state.isActive
                          ? 'navbar-menu navbar-item has-dropdown is-active'
                          : 'navbar-menu navbar-item has-dropdown'
                      }
                      onClick={() => this.menu()}
                    >
                      <label className="navbar-link tt">
                        {currentUser && (
                          <p className="is-size-5">{currentUser.email}</p>
                        )}
                      </label>
                      <div className="navbar-dropdown is-boxed">
                        <Link
                          to="/profile"
                          className="navbar-item"
                          onClick={() => this.closeBurger()}
                        >
                          <span>Profile</span>
                        </Link>
                        <Link
                          to="/changepassword"
                          className="navbar-item"
                          onClick={() => this.closeBurger()}
                        >
                          <span>ChangePassword</span>
                        </Link>
                        {currentUser.usertype === 'admin' ? (
                          <React.Fragment>
                            <hr className="navbar-divider" />
                            <Link
                              to="/uploadimage"
                              className="navbar-item"
                              onClick={() => this.closeBurger()}
                            >
                              <span>Upload Image</span>
                            </Link>
                            <Link
                              to="/usersmanage"
                              className="navbar-item"
                              onClick={() => this.closeBurger()}
                            >
                              <span>Manage Users</span>
                            </Link>
                            <Link
                              to="/ordermanage"
                              className="navbar-item"
                              onClick={() => this.closeBurger()}
                            >
                              <span>Manage Order</span>
                            </Link>
                            <Link
                              to="/coursemanage"
                              className="navbar-item"
                              onClick={() => this.closeBurger()}
                            >
                              <span>Manage Course</span>
                            </Link>
                            <Link
                              to="/schedulemagage"
                              className="navbar-item"
                              onClick={() => this.closeBurger()}
                            >
                              <span>Manage Schedule</span>
                            </Link>
                            <Link
                              to="/bookmanage"
                              className="navbar-item"
                              onClick={() => this.closeBurger()}
                            >
                              <span>Manage Book</span>
                            </Link>
                          </React.Fragment>
                        ) : null}
                        <hr className="navbar-divider" />
                        <Link
                          to="/"
                          className="navbar-item"
                          onClick={() => {
                            dispatch(logout())
                          }}
                        >
                          <span className="icon">
                            <i className="fas fa-user" />
                          </span>
                          <span>Logout</span>
                        </Link>
                      </div>
                    </div>
                  </div>
                </React.Fragment>
              ) : (
                <div className="navbar-item">
                  <div className="field is-grouped">
                    <p className="control">
                      <Link
                        className="button is-primary"
                        to="/login"
                        onClick={() => this.closeBurger()}
                      >
                        <span className="icon">
                          <i className="fas fa-sign-in-alt" />
                        </span>
                        <span>Login</span>
                      </Link>
                    </p>
                    <p className="control">
                      <Link
                        className="button"
                        to="/register"
                        onClick={() => this.closeBurger()}
                      >
                        <span className="icon">
                          <i className="fas fa-user-plus" />
                        </span>
                        <span>Register</span>
                      </Link>
                    </p>
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      </nav>
    )
  }
}
const mapStateToProps = state => ({
  isAuthenticated: state.auth.isAuthenticated,
  currentUser: state.auth.currentUser,
  alert: state.alert.alert,
  cart: state.cart.cart
})

export default connect(mapStateToProps)(AppHeader)
