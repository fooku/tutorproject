import React from 'react'

export default ({ message }) => (
  <div className="form-errors">
    <p className="help is-size-6 is-danger">{message}</p>
  </div>
)
