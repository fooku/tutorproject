import React from 'react'
import { Switch, Route } from 'react-router-dom'

import HomePage from '../views/HomePage'
import CoursePage from '../views/CoursePage'
import CourseDetailPage from '../views/CoursePage/ConrseDetailPage'
import HowtoPage from '../views/HowtoPage'
import LoginPage from '../views/auth/LoginPage'
import RegisterPage from '../views/auth/RegisterPage'
import ProfilePage from '../views/ProfilePage'
import CartPage from '../views/cartPage'
import MycoursePage from '../views/mycoursePage'
import MyCourseDetailPage from '../views/mycoursePage/MyCourseDetailPage'
import OrderPage from '../views/OrderPage'
import OrderDetailPage from '../views/OrderPage/OrderDetailPage'
import PaymentPage from '../views/OrderPage/PaymentPage'
import ResetPasswordPage from '../views/ResetPasswordPage'

import UsersManage from '../views/Manage/UsersManage'
import CourseManage from '../views/Manage/CourseManage'
import Course from '../views/Manage/CourseManage/courseManagePage'
import Bundle from '../views/Manage/CourseManage/bundleManagePage'

import UploadImage from '../views/Manage/UploadImagePage'
import OrderManagePage from '../views/Manage/OrderManagePage'
import OrderDetailManagePage from '../views/Manage/OrderManagePage/OrderDetailManagePage'
import BundleDetailPage from '../views/CoursePage/BundleDetailPage'

import OfflineCoursePage from '../views/offlineCoursePage'
import BookPage from '../views/BookPage'
import DetailPage from '../views/BookPage/DetailPage'

import ScheduleMagenePage from '../views/Manage/ScheduleMagenePage'

import BookManagePage from '../views/Manage/BookManage'
import tablePage from '../views/tablePage'
import PolicyPage from '../views/PolicyPage'
import BookDetailManagePage from '../views/Manage/BookManage/bookManagePage'

const Routing = () => (
  <Switch>
    <Route exact path="/" component={HomePage} />
    <Route exact path="/course" component={CoursePage} />
    <Route exact path="/offlinecourse" component={OfflineCoursePage} />
    <Route exact path="/course/:course" component={CourseDetailPage} />
    <Route exact path="/bundle/:bundle" component={BundleDetailPage} />
    <Route exact path="/book" component={BookPage} />
    <Route exact path="/book/:book" component={DetailPage} />
    <Route exact path="/howtobuy" component={HowtoPage} />
    <Route exact path="/login" component={LoginPage} />
    <Route exact path="/register" component={RegisterPage} />
    <Route exact path="/usersmanage" component={UsersManage} />
    <Route exact path="/coursemanage" component={CourseManage} />
    <Route exact path="/coursemanage/:course" component={Course} />
    <Route exact path="/bundlemanage/:bundle" component={Bundle} />
    <Route exact path="/uploadimage" component={UploadImage} />
    <Route exact path="/profile" component={ProfilePage} />
    <Route exact path="/changepassword" component={ResetPasswordPage} />
    <Route exact path="/cart" component={CartPage} />
    <Route exact path="/mycourse" component={MycoursePage} />
    <Route exact path="/mycourse/:id/" component={MyCourseDetailPage} />
    <Route
      exact
      path="/mycourse/:id/:activeVideo"
      component={MyCourseDetailPage}
    />
    <Route exact path="/order" component={OrderPage} />
    <Route exact path="/order/:id" component={OrderDetailPage} />
    <Route exact path="/order/payment/:id" component={PaymentPage} />
    <Route exact path="/ordermanage" component={OrderManagePage} />
    <Route exact path="/ordermanage/:id" component={OrderDetailManagePage} />

    <Route exact path="/schedulemagage" component={ScheduleMagenePage} />

    <Route exact path="/bookmanage" component={BookManagePage} />
    <Route exact path="/bookmanage/:book" component={BookDetailManagePage} />
    <Route exact path="/schedule" component={tablePage} />
    <Route exact path="/policy" component={PolicyPage} />
  </Switch>
)

export default Routing
