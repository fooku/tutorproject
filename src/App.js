import React, { Component } from 'react'
import './App.css'
import './bulma.css'
import AppHeader from './component/Appheader'
import Routing from './routes'
import { verifyauth } from './actions/auth'
import { connect } from 'react-redux'
import { withRouter } from 'react-router'
import Loading from './component/Loading'
import AppheaderM from './component/AppheaderM'

class App extends Component {
  componentDidMount() {
    const { dispatch } = this.props

    dispatch(verifyauth())
    console.log('window.screen.width >> ', window.screen.width)
  }

  render() {
    const { isFetching } = this.props

    return (
      <div className="App">
        {isFetching ? <Loading /> : null}
        {window.screen.width < 1275 ? <AppheaderM /> : <AppHeader />}

        <Routing />
      </div>
    )
  }
}
const mapStateToProps = state => ({
  isFetching: state.auth.isFetchingAuth
})

export default withRouter(connect(mapStateToProps)(App))
